#include "LightOps.h"
#include "myGL.h"
#include <glm/glm.hpp>
#include "Model.h"
#include <algorithm>
#include <glm/gtx/string_cast.hpp>
using namespace glm;

void LightOps::constShading(MaterialCoeffs &matrCoefs, fmat4 &lightTransfMat,
	vector<dvec3>& polNormalsNormalisedTransf, vector<dvec3>& polCentresTransf)
{
	int n = polygons.size();
	fmat4& modelMatrix = myGL::getModelMatrix();
	for(int i = 0; i < n; i++) {
		ivec3& pol = this->polygons[i];
		dvec4& first = this->vertices[pol[0]];
		dvec4& second = this->vertices[pol[1]];
		dvec4& third = this->vertices[pol[2]];

		myGL::myglBegin(myGL::getCurrDrawForm());
		// ##############################################		CULLING		###################################################
		if (myGL::cull) {
			fmat4& totalMat = myGL::getTotalMatrix();

			dvec4 firstTransf = totalMat * first;
			dvec4 secondTransf = totalMat * second;
			dvec4 thirdTransf = totalMat * third;

			firstTransf = { firstTransf[0] / firstTransf[3], firstTransf[1] / firstTransf[3], firstTransf[2] / firstTransf[3], 1 };
			secondTransf = { secondTransf[0] / secondTransf[3], secondTransf[1] / secondTransf[3], secondTransf[2] / secondTransf[3], 1 };
			thirdTransf = { thirdTransf[0] / thirdTransf[3], thirdTransf[1] / thirdTransf[3], thirdTransf[2] / thirdTransf[3], 1 };

			if ((firstTransf.x * secondTransf.y - secondTransf.x * firstTransf.y) 
				+ (secondTransf.x * thirdTransf.y - thirdTransf.x * secondTransf.y) 
				+ (thirdTransf.x * firstTransf.y - firstTransf.x * thirdTransf.y) 
				< 0) {
				myGL::myglEnd();
				continue;
			}
		}
		// ##############################################		CULLING		###################################################

		//dvec4 normal = this->planes[i]; normal[3] = 1;		// Set h = 1. This is done to enable the transformation of the normal
		//dvec4 polCentre = { this->polCentres[i], 1 };
		
		//dvec3 firstTransf = modelMatrix * first;
		//dvec3 secondTransf = modelMatrix * second;
		//dvec3 thirdTransf = modelMatrix * third;

		// Calculate the new normal and new polCentre!
		dvec3& normalTransf = polNormalsNormalisedTransf[i];
		dvec3& polCentreTransf = polCentresTransf[i];

		//dvec3 NHat = modelMatrix * dvec4{ polNormalsNormalised[i] , 1 };
		//NHat = normalize(NHat);
		//dvec3 polCentreTransf = modelMatrix * dvec4{ polCentres[i] , 1};

		// Time to calculate the color for this polygon!
		dvec3 intensity{ 0, 0, 0 };
		for (LightSource ls : this->lightSources) {
			if (ls.type == SourceType::Ambient) {
				intensity.x += ls.colour.x * matrCoefs.ka;		// x -> r
				intensity.y += ls.colour.y * matrCoefs.ka;		// y -> g
				intensity.z += ls.colour.z * matrCoefs.ka;		// z -> b
			}
			else if (ls.type == SourceType::Diffuse) {
				// Let's calculate new light pos! Need to take the model matrix that was used before the current model matrix
				// Because the current model matrix contains object-only transformations
				dvec3 newPos = lightTransfMat * dvec4{ ls.pos, 1 };

				dvec3 LHat = normalize(newPos - polCentreTransf);				// Pretty easy and logical
				// For NHat will have to transform it first
				dvec3 NHat = normalize(normalTransf);
				GLdouble ln = glm::max(0., dot(LHat, NHat));

				if (ln <= 0) {
					continue;		// No reason to calc this, value is gonna be 0 either way
				}

				intensity.x += ls.colour.x * matrCoefs.kd * ln;		// x -> r
				intensity.y += ls.colour.y * matrCoefs.kd * ln;		// y -> g
				intensity.z += ls.colour.z * matrCoefs.kd * ln;		// z -> b
			}
		}

		// Check if any maximums have gone overboard
		if (intensity.x > 255) {
			intensity.x = 255;
		}
		
		if (intensity.y > 255) {
			intensity.y = 255;
		}

		if (intensity.z > 255) {
			intensity.z = 255;
		}

		// cout << "Intensity: " << glm::to_string(intensity) << endl;
		myGL::myglColor3ub(intensity.x, intensity.y, intensity.z);
		myGL::myglVertex3f(first.x, first.y, first.z);
		myGL::myglVertex3f(second.x, second.y, second.z);
		myGL::myglVertex3f(third.x, third.y, third.z);
		myGL::myglEnd();
	}
}

void LightOps::gouraudShading(MaterialCoeffs& matrCoefs, fmat4& lightTransfMat, 
	vector<dvec3>& vexNormalsNormalisedTransf, vector<dvec3>& polCentresTransf)
{
	int n = polygons.size();
	fmat4& modelMatrix = myGL::getModelMatrix();
	for (int i = 0; i < n; i++) {
		ivec3& pol = this->polygons[i];

		GLint firstIndex = pol[0];
		GLint secondIndex = pol[1];
		GLint thirdIndex = pol[2];

		dvec4& first = this->vertices[firstIndex];
		dvec4& second = this->vertices[secondIndex];
		dvec4& third = this->vertices[thirdIndex];

		myGL::myglBegin(myGL::getCurrDrawForm());
		// ##############################################		CULLING		###################################################
		if (myGL::cull) {
			fmat4& totalMat = myGL::getTotalMatrix();

			dvec4 firstTransf = totalMat * first;
			dvec4 secondTransf = totalMat * second;
			dvec4 thirdTransf = totalMat * third;

			firstTransf = { firstTransf[0] / firstTransf[3], firstTransf[1] / firstTransf[3], firstTransf[2] / firstTransf[3], 1 };
			secondTransf = { secondTransf[0] / secondTransf[3], secondTransf[1] / secondTransf[3], secondTransf[2] / secondTransf[3], 1 };
			thirdTransf = { thirdTransf[0] / thirdTransf[3], thirdTransf[1] / thirdTransf[3], thirdTransf[2] / thirdTransf[3], 1 };

			if ((firstTransf.x * secondTransf.y - secondTransf.x * firstTransf.y)
				+ (secondTransf.x * thirdTransf.y - thirdTransf.x * secondTransf.y)
				+ (thirdTransf.x * firstTransf.y - firstTransf.x * thirdTransf.y)
				< 0) {
				myGL::myglEnd();
				continue;
			}
		}
		// ##############################################		CULLING		###################################################

		//dvec4 normal = this->planes[i]; normal[3] = 1;		// Set h = 1. This is done to enable the transformation of the normal
		//dvec4 polCentre = { this->polCentres[i], 1 };

		// dvec3 firstTransf = modelMatrix * first;
		// dvec3 secondTransf = modelMatrix * second;
		// dvec3 thirdTransf = modelMatrix * third;

		// dvec3 normalTransf = cross((secondTransf - firstTransf), (thirdTransf - firstTransf));
		//dvec3 polCentreTransf = (firstTransf + secondTransf + thirdTransf) / 3.;

		// Calculate the new normal and new polCentre!
		//dvec4 normalFirstTransf = modelMatrix * dvec4{ vexNormalsNormalised[firstIndex], 1 };
		//dvec4 normalSecondTransf = modelMatrix * dvec4{ vexNormalsNormalised[secondIndex], 1 };
		//dvec4 normalThirdTransf = modelMatrix * dvec4{ vexNormalsNormalised[thirdIndex], 1 };
		//dvec3 polCentreTransf = modelMatrix * polCentre;

		dvec3& normalFirstTransf = vexNormalsNormalisedTransf[firstIndex];
		dvec3& normalSecondTransf = vexNormalsNormalisedTransf[secondIndex];
		dvec3& normalThirdTransf = vexNormalsNormalisedTransf[thirdIndex];
		dvec3& polCentreTransf = polCentresTransf[i];

		// Time to calculate the color for this polygon!
		dvec3 intensityFirst{ 0, 0, 0 };
		dvec3 intensitySecond{ 0, 0, 0 };
		dvec3 intensityThird{ 0, 0, 0 };
		for (LightSource ls : this->lightSources) {
			if (ls.type == SourceType::Ambient) {
				GLdouble gx = ls.colour.x * matrCoefs.ka;
				GLdouble gy = ls.colour.y * matrCoefs.ka;
				GLdouble gz = ls.colour.z * matrCoefs.ka;

				intensityFirst.x += gx;		// x -> r
				intensityFirst.y += gy;		// y -> g
				intensityFirst.z += gz;		// z -> b

				intensitySecond.x += gx;		// x -> r
				intensitySecond.y += gy;		// y -> g
				intensitySecond.z += gz;		// z -> b

				intensityThird.x += gx;		// x -> r
				intensityThird.y += gy;		// y -> g
				intensityThird.z += gz;		// z -> b
			}
			else if (ls.type == SourceType::Diffuse) {
				dvec3 newPos = lightTransfMat * dvec4{ ls.pos, 1 };
				dvec3 LHat = normalize(newPos - polCentreTransf);				// Pretty easy and logical

				GLdouble ln1 = glm::max(0., dot(LHat, normalFirstTransf));
				GLdouble ln2 = glm::max(0., dot(LHat, normalSecondTransf));
				GLdouble ln3 = glm::max(0., dot(LHat, normalThirdTransf));

				// cout << "LHat: " << glm::to_string(LHat) << endl;
				// cout << "N1Tr" << glm::to_string(normalFirstTransf) << endl;
				// cout << "N2Tr" << glm::to_string(normalSecondTransf) << endl;
				// cout << "N3Tr" << glm::to_string(normalThirdTransf) << endl;

				// cout << "ln1: " << ln1 << endl;
				// cout << "ln2: " << ln2 << endl;
				// cout << "ln3: " << ln3 << endl;

				intensityFirst.x += ls.colour.x * matrCoefs.kd * ln1;		// x -> r
				intensityFirst.y += ls.colour.y * matrCoefs.kd * ln1;		// y -> g
				intensityFirst.z += ls.colour.z * matrCoefs.kd * ln1;		// z -> b


				intensitySecond.x += ls.colour.x * matrCoefs.kd * ln2;		// x -> r
				intensitySecond.y += ls.colour.y * matrCoefs.kd * ln2;		// y -> g
				intensitySecond.z += ls.colour.z * matrCoefs.kd * ln2;		// z -> b


				intensityThird.x += ls.colour.x * matrCoefs.kd * ln3;		// x -> r
				intensityThird.y += ls.colour.y * matrCoefs.kd * ln3;		// y -> g
				intensityThird.z += ls.colour.z * matrCoefs.kd * ln3;		// z -> b
			}
		}

		// Check if any maximums have gone overboard
		if (intensityFirst.x > 255) {
			intensityFirst.x = 255;
		}
		if (intensityFirst.y > 255) {
			intensityFirst.y = 255;
		}
		if (intensityFirst.z > 255) {
			intensityFirst.z = 255;
		}

		if (intensitySecond.x > 255) {
			intensitySecond.x = 255;
		}
		if (intensitySecond.y > 255) {
			intensitySecond.y = 255;
		}
		if (intensitySecond.z > 255) {
			intensitySecond.z = 255;
		}

		if (intensityThird.x > 255) {
			intensityThird.x = 255;
		}
		if (intensityThird.y > 255) {
			intensityThird.y = 255;
		}
		if (intensityThird.z > 255) {
			intensityThird.z = 255;
		}

		// cout << "Intensity1: " << glm::to_string(intensityFirst) << endl;
		// cout << "Intensity2: " << glm::to_string(intensitySecond) << endl;
		// cout << "Intensity3: " << glm::to_string(intensityThird) << endl;

		myGL::myglColor3ub(intensityFirst.x, intensityFirst.y, intensityFirst.z);
		myGL::myglVertex3f(first.x, first.y, first.z);
		
		myGL::myglColor3ub(intensitySecond.x, intensitySecond.y, intensitySecond.z);
		myGL::myglVertex3f(second.x, second.y, second.z);
		
		myGL::myglColor3ub(intensityThird.x, intensityThird.y, intensityThird.z);
		myGL::myglVertex3f(third.x, third.y, third.z);
		
		myGL::myglEnd();
	}
}

LightOps::LightOps(
	vector<dvec4>& vertices,
	vector<dvec3>& vexNormalsNormalised,
	vector<ivec3>& polygons,
	vector<dvec3>& polCentres,
	vector<dvec3>& polNormalsNormalised,
	vector<dvec4>& planes,
	vector<dvec4>& projectionPlanes,
	vector<dvec4>& testingPoints
) :
	vertices{vertices},
	vexNormalsNormalised{ vexNormalsNormalised },
	polygons{ polygons },
	polCentres{ polCentres },
	polNormalsNormalised{ polNormalsNormalised },
	planes{ planes },
	projectionPlanes{ projectionPlanes },
	testingPoints{ testingPoints }
{
}
