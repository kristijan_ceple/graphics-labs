#pragma once
#include <vector>
#include <glm\fwd.hpp>
#include <GL/freeglut.h>
using namespace std; using namespace glm;

inline GLdouble STEP = 0.01;

class BezierHolder
{
private:
	GLint n;
	
	vector<fvec3> controlPolygon;
	vector<fvec3> bezierCurve;
	GLboolean curveReady = false;
	// vector<GLdouble> bernsteinPolynom;
	
	vector<GLdouble> bcoeffs;
	GLboolean coeffsCalculated = false;
public:
	BezierHolder();
	void calculateBinCoeffs();
	// GLdouble bernsteinPolynom(GLdouble t);
	vector<fvec3> calculateBezierCurve();
	vector<fvec3> getBezierCurve();
	GLboolean areCoeffsCalculated();

	void addControlPoint(fvec3 toAdd);
	vector<fvec3> getControlPolygon();
};

