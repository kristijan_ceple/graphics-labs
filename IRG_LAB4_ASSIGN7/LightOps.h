#pragma once
#include <iostream>
#include <glm/glm.hpp>
#include <vector>
#include <GL/freeglut.h>
#include "Model.h"
using namespace std; using namespace glm;

// ########################################################################		ENUMS		####################################################################
enum class Lighting { Constant, Gouraud, Phong };
enum class SourceType { Ambient, Diffuse };
// ########################################################################		ENUMS		####################################################################

struct LightSource {
	dvec3 pos;
	ivec3 colour;
	SourceType type;
	LightSource(dvec3 pos, ivec3 colour, SourceType type) : pos{ pos }, colour{ colour }, type{ type } {}
};

class LightOps
{
public:
	vector<LightSource> lightSources;
	Lighting currLighting = Lighting::Constant;
	vector<dvec4>& vertices;
	vector<dvec3>& vexNormalsNormalised;
	vector<ivec3>& polygons;
	vector<dvec3>& polCentres;
	vector<dvec3>& polNormalsNormalised;
	vector<dvec4>& planes;
	vector<dvec4>& projectionPlanes;
	vector<dvec4>& testingPoints;
	void constShading(MaterialCoeffs& matrCoefs, fmat4& lightTransfMat,
		vector<dvec3>& polNormalsNormalisedTransf, vector<dvec3>& polCentresTransf);
	void gouraudShading(MaterialCoeffs& matrCoefs, fmat4& lightTransfMat,
		vector<dvec3>& vexNormalsNormalisedTransf, vector<dvec3>& polCentresTransf);
	LightOps(
		vector<dvec4>& vertices,
		vector<dvec3>& vexNormalsNormalised,
		vector<ivec3>& polygons,
		vector<dvec3>& polCentres,
		vector<dvec3>& polNormalsNormalised,
		vector<dvec4>& planes,
		vector<dvec4>& projectionPlanes,
		vector<dvec4>& testingPoints
	);
};

