#include <iostream>
#include <vector>
#include <iostream>
using namespace std;

// Render each polygon
vector<ivec3> pols = customObj->getPolygons();
vector<dvec4> vertices = customObj->getVertices();
vector<dvec4> planes = customObj->getPlanes();

switch (lightOps->currLighting) {
case Lighting::Constant:
	customObj->formTransfPolNormals_And_PolCentres();
	lightOps->constShading(customObj->lightCoeffs, beforeObjRotation,
		customObj->polNormalsNormalisedTransf, customObj->polCentresTransf);
	break;
case Lighting::Gouraud:
	customObj->formTransfVexNormals_And_PolCentres();
	lightOps->gouraudShading(customObj->lightCoeffs, beforeObjRotation,
		customObj->vexNormalsNormalisedTransf, customObj->polCentresTransf);
	break;
case Lighting::Phong:
	break;
}