#include "Vector.h"
#include <cstdarg>

Vector::Vector() { this->_dimension = 0; _elements = new double[0]; }

Vector::Vector(size_t n) { this->_dimension = n; _elements = new double[n]; }

Vector::Vector(int num, ...)
{
	va_list valist;
	double* elements = new double[num];

	va_start(valist, num);
	for (int i = 0; i < num; i++) {
		*(elements + i) = va_arg(valist, double);
	}
	va_end(valist);
	
	this->_elements = elements;
	this->_dimension = num;
}

Vector::Vector(double _elements[], size_t dimension)
{
	this->_elements = (double *) malloc(dimension * sizeof(double));
	this->_dimension = dimension;
	memcpy(this->_elements, _elements, dimension * sizeof(double));
}

Vector::Vector(bool _immutable, bool canTakeArgumentArray, double* array, size_t dimension)
{
	this->_dimension = dimension;
	this->_immutable = _immutable;

	if (canTakeArgumentArray) {
		this->_elements = array;
	}
	else {
		_elements = new double[dimension];
		memcpy(this->_elements, array, dimension * sizeof(double));
	}
}

double Vector::get(size_t toGet)
{
	return *(_elements + toGet);
}

IVector* Vector::set(size_t pos, double toSetVal)
{
	*(_elements + pos) = toSetVal;
	return this;
}

size_t Vector::getDimension()
{
	return this->_dimension;
}

IVector* Vector::copy()
{
	/*
	IVector* newVector = new Vector{this->_dimension};
	memcpy(newVector, this, sizeof(*this))
	return newVector;
	*/

	IVector* newVector = new Vector{ this->_elements, _dimension };
	return newVector;
}

IVector* Vector::newInstance(size_t n)
{
	double* buffer = new double[n] {};
	size_t dimension = this->getDimension();
	
	// Only the first n elements are 
	for (size_t i = 0; i < this->getDimension() && i < n; i++) {
		*(buffer + i) = *(this->_elements + i);
	}

	return new Vector{ buffer, dimension};
}

Vector::~Vector()
{
	delete(this->_elements);
}

Vector* Vector::parseSimple(std::string toParse)
{
	// Have to read until newlines
	double buffer[4];
	size_t quantity = scanf_s("[^\n\t\r ]%lf[^\n\t\r ]%lf[^\n\t\r ]%lf[^\n\t\r ]%lf", &buffer[0], &buffer[1], &buffer[2], &buffer[3]);

	if (quantity <= 0 || quantity > 4) {
		throw std::invalid_argument("Invalid format!");
	}

	return new Vector{ buffer, quantity };
}