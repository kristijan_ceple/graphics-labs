#include "AbstractMatrix.h"
#include "MatrixTransposeView.h"
#include "MatrixSubMatrixView.h"
#include "Matrix.h"
#include <iostream>
#include <fmt/core.h>
using namespace std;

IMatrix* AbstractMatrix::nTranspose(bool liveView)
{
	// Make a new Matrix transpose view and return it
	IMatrix* toReturn;
	if (liveView) {
		toReturn = new MatrixTransposeView{this};
	}
	else {
		// Need to make a new matrix first, and then pass it to the func
		IMatrix* newMatrix = this->copy();
		toReturn = new MatrixTransposeView{ newMatrix };
	}

	return toReturn;
}

IMatrix* AbstractMatrix::add(IMatrix& otherMat)
{
	for (size_t i = 0; i < this->getRowsCount(); i++) {
		for (size_t j = 0; j < this->getColsCount(); j++) {
			this->set(i, j, ( this->get(i, j) + otherMat.get(i, j) ) );
		}
	}

	return this;
}	

IMatrix* AbstractMatrix::nAdd(IMatrix& otherMat)
{
	// First create a copy of this
	return this->copy()->add(otherMat);
}

IMatrix* AbstractMatrix::sub(IMatrix& otherMat)
{
	for (size_t i = 0; i < this->getRowsCount(); i++) {
		for (size_t j = 0; j < this->getColsCount(); j++) {
			this->set(i, j, (this->get(i, j) - otherMat.get(i, j)));
		}
	}

	return this;
}

IMatrix* AbstractMatrix::nSub(IMatrix& otherMat)
{
	return this->copy()->sub(otherMat);
}

inline double AbstractMatrix::multiplyRowColumn(IMatrix& otherMat, size_t currRow, size_t currCol, size_t firstCols, size_t secondRows)
{
	double sum = 0;

	for (size_t k = 0; k < firstCols; k++) {
		for (size_t l = 0; l < secondRows; l++) {
			sum += this->get(currRow, k) * otherMat.get(l, currCol);
		}
	}

	return sum;
}

IMatrix* AbstractMatrix::nMultiply(IMatrix& otherMat)
{
	// This will be a fucked up one
	// Let's first check dimension

	size_t firstCols = this->getColsCount();
	size_t firstRows = this->getRowsCount();
	size_t secondRows = otherMat.getRowsCount();
	size_t secondCols = otherMat.getColsCount();

	if (firstCols != secondRows) {
		throw std::invalid_argument("Matrices dimensions mismatch!");
	}

	double rowSum;
	// size_t sumElementsNum = this->getColsCount();
	IMatrix* resMat = new Matrix{firstRows, secondCols};
	for (size_t i = 0; i < firstRows; i++) {
		for (size_t j = 0; j < secondCols; j++) {
			rowSum = multiplyRowColumn(otherMat, i, j, this->getColsCount(), otherMat.getRowsCount());
			// Have got the sum now, place it in the resMat
			resMat->set(i, j, rowSum);
		}
	}

	return resMat;
}

double AbstractMatrix::determinant()
{
	// First check if rows == cols
	size_t rows = this->getRowsCount();
	size_t cols = this->getColsCount();
	if (rows != cols) {
		throw invalid_argument("Can't calculate determinant of non-quadratic matrix!");
	}

	// even more fucked up
	// Have to alternate 1 and -1
	// Have to go recursively down to 2
	if (cols == 2) {
		return (this->get(0, 0)*this->get(1, 1) - this->get(0, 1) * this->get(1, 0));
	}
	else if (cols == 1) {
		return this->get(0, 0);
	}

	double sum = 0;
	for (size_t j = 0; j < this->getColsCount(); j++) {
		IMatrix* subMatr = this->subMatrix(0, j, true);
		// Oke got it.
		sum += (j % 2 == 1) ? (-1) * subMatr->determinant() * this->get(0, j) : subMatr->determinant() * this->get(0, j);
	}

	//That's it!
	return sum;
}

IMatrix* AbstractMatrix::subMatrix(size_t row, size_t col, bool liveView)
{
	// Uhhh
	// What even here?
	// aha

	IMatrix* mat;
	if (liveView) {
		mat = this;
	}
	else {
		mat = this->copy();
	}

	return new MatrixSubMatrixView{this, row, col};
}

IMatrix* AbstractMatrix::nInvert()
{
	// Also extremely fucked up
	// now this will be the shit
	// So, first check the matrix dimensions
	if (this->getColsCount() != this->getRowsCount()) {
		throw invalid_argument("Matrix dimension mismatch!");
	}

	// Now - let's calculate the determinant of M
	double detM = this->determinant();
	if (detM == 0) {
		throw  runtime_error("Determinant is zero -- can't find inverse!");
	}

	size_t Mrows = this->getRowsCount();
	size_t Mcols = this->getColsCount();
	double minorTmp;
	IMatrix* subMat;

	// Excellent - move on -- transpose now
	IMatrix* transMat = this->nTranspose(false);

	// Now, we will create a new matrix, which will be filled with minors
	IMatrix* minorsMat = new Matrix{Mrows, Mcols};

	// Let's go
	for (size_t i = 0; i < Mrows; i++) {
		for (size_t j = 0; j < Mcols; j++) {
			// Calculate each minor, and put it into the matrix
			subMat = transMat->subMatrix(i, j);
			minorTmp = subMat->determinant();
			minorTmp *= (i + j) % 2 == 1 ? -1 : 1;
			minorsMat->set(i, j, minorTmp);
		}
	}

	// Okay, got it! Now multiply each value with 1/detM
	minorsMat->scalarMultiply(1/detM);

	// Wasn't so hard actually
	return minorsMat;
}

 IMatrix* AbstractMatrix::scalarMultiply(double scalar, bool onLive)
{
	IMatrix* mat;
	if (onLive) {
		mat = this;
	}
	else {
		mat = this->copy();
	}

	size_t Mrows = this->getRowsCount();
	size_t Mcols = this->getColsCount();
	for (size_t i = 0; i < Mrows; i++) {
		for (size_t j = 0; j < Mcols; j++) {
			mat->set(i, j, this->get(i, j) * scalar);
		}
	}

	return mat;
}

double** AbstractMatrix::toArray()
{
	size_t rows = this->getRowsCount();
	size_t cols = this->getColsCount();

	// 2D array -> this is where the fun begins
	// Array of arrays(well, pointers to arrays technically speaking)
	double** newArray = new double* [rows];
	for (size_t i = 0; i < rows; i++) {
		newArray[i] = new double[cols];
	}

	// Just iterate inversely
	for (size_t i = 0; i < rows; i++) {
		for (size_t j = 0; j < cols; j++) {
			newArray[i][j] = this->get(i, j);
		}
	}

	return newArray;
}

std::string AbstractMatrix::toString(size_t precision)
{
	size_t colsCount = this->getColsCount();
	size_t rowsCount = this->getRowsCount();
	string toReturn;
	size_t emptyStringLen = precision + 1 + 2;

	toReturn.append("- ");
	for (size_t j = 0; j < colsCount; j++) {
		toReturn.append(fmt::format("{:{}} ", "", emptyStringLen));
	}
	toReturn.append("-\n");

	for (size_t i = 0; i < rowsCount; i++) {
		toReturn.append("| ");
		for (size_t j = 0; j < colsCount; j++) {
			toReturn.append(fmt::format("{:.{}f} ",  this->get(i, j), precision));
		}
		toReturn.append("|\n");
	}

	toReturn.append("- ");
	for (size_t j = 0; j < colsCount; j++) {
		toReturn.append(fmt::format("{:{}} ", "", emptyStringLen));
	}
	toReturn.append("-\n");

	return toReturn;
}

IVector* AbstractMatrix::toVector(bool liveView)
{
	// What even? Whole matrix as vector?
	// Do something here - need to use those supporting classes I guess
	return nullptr;
}
