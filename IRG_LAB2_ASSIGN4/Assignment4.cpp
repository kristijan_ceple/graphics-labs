#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include "Model.h"
#include <fmt\format.h>
#include <boost/algorithm/string.hpp>
using namespace std; using namespace glm;

 // #################################################################################################################
 // ##########################################		CONSTANTS		################################################
 // #################################################################################################################

GLuint window;
GLuint width = 1600, height = 900;
GLdouble aspectRatio = width / height;
GLdouble move_z = 0, move_x = 0, rotate_x_global = 0, rotate_y_global = 0, rotate_x_obj = 0, rotate_y_obj = 0;
GLboolean rotatingGlobal = true;
GLuint mouse_x, mouse_y;
unique_ptr<Model> customObj = nullptr;
const GLdouble EPSILON = 1E-6;
const GLdouble RADIUS = 0.025;
GLint currDrawForm = GL_LINE_LOOP;

enum class Colour { Black, White, Red, Green, Blue, Yellow };
static unordered_map<string, Colour> const colourTbl = {
	{"black", Colour::Black},
	{"white", Colour::White},
	{"red", Colour::Red},
	{"green", Colour::Green},
	{"blue", Colour::Blue},
	{"yellow", Colour::Yellow},
};
Colour drawingColour = Colour::White;

fvec3 eye = { 0, 0, 5 };
fvec3 center = { 0, 0, 0 };

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void setColour(Colour drawingColour);
void specialKeys(int key, int x, int y);
void exampleCube();
void glObj();
int queryPoints(dvec4 toCheck);
dvec4 antiTransformVex(dvec4);
void transformModel();
void keyP();
void glTestingPoints();
void updateEye();
void updateCenter();

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	// First load data
	string toLoad;
	cout << "Filename to load << ";
	cin >> toLoad;
	customObj = make_unique<Model>(toLoad);
	cout << "Loading object " << toLoad << endl;
	customObj->loadData();
	cout << customObj->dataStr() << endl;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("Custom object rendering!");
	glutDisplayFunc(myDisplay);
	glutPassiveMotionFunc(mouseMoved);
	glutSpecialFunc(specialKeys);
	glutReshapeFunc(myReshape);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glEnable(GL_DEPTH_TEST);

	// Culling
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT, GL_LINE);
	glEnable(GL_CULL_FACE);

	cout << "Object rendering program!" << endl;
	cout << "Right click clears the polygon and enables inputting another object to render!" << endl;
	cout << "Middle mouse button clears the rotation!" << endl;

	cout << "Press p to input a point that will be tested whether it is inside, outside, or on the mantles of the convex polygon!" << endl;
	cout << "Press e to update the eye coords, and c to update the center(look-at point) coords!" << endl;
	cout << "Press f to change the current drawing form(wireframe vs. filled polygon)!" << endl;
	cout << "Press W, A, S, D to move around in the scene! By pressing the (gaming?) keys or numpad keys you can rotate the scene or the object!" << endl;
	cout << "Press z to switch between global rotation and object rotation(has effects what do the keys rotate)!" << endl;

	cout << "Press x to change colour!" << endl;
	cout << "Irrelevant -- Press r for specific testing points check!" << endl;
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	// glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
	glLoadIdentity();
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		0, 1, 0
	);
	renderScene();			// First render the scene!
	glutSwapBuffers();		// Render complete -- swap buffers!
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	glMatrixMode(GL_PROJECTION);		// Projection matrix
	glLoadIdentity();					// Reset to identity matrix
	//glFrustum(-10.0, 10, -10.0, 10.0, 50, 150.0);
	gluPerspective(45, aspectRatio, 0.1, 100);
	//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	//gluOrtho2D(-2, 2, -2, 2);
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		0, 1, 0
	);
}

void glObj()
{
	glPushMatrix();
	glRotatef(rotate_x_obj, 1.0, 0.0, 0.0);
	glRotatef(rotate_y_obj, 0.0, 1.0, 0.0);
	transformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> vertices = customObj->getVertices();

	for (ivec3 pol : pols) {
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		// glFrontFace(GL_CCW);
		glBegin(currDrawForm);
		glVertex3d(first.x, first.y, first.z);
		glVertex3d(second.x, second.y, second.z);
		glVertex3d(third.x, third.y, third.z);
		glEnd();
	}
	glPopMatrix();
}

void specialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		//move_x += 0.1;
		if (rotatingGlobal) {
			rotate_y_global += 5;
		}
		else {
			rotate_y_obj -= 5;
		}
	}
	else if (key == GLUT_KEY_LEFT) {
		//move_x -= 0.1;
		if (rotatingGlobal) {
			rotate_y_global -= 5;
		}
		else {
			rotate_y_obj += 5;
		}
	}
	else if (key == GLUT_KEY_UP) {
		//center = center + myGL::zAxis;
		//eye = eye + myGL::zAxis;
		//move_z -= 0.1;

		if (rotatingGlobal) {
			rotate_x_global -= 5;
		}
		else {
			rotate_x_obj += 5;
		}
	}
	else if (key == GLUT_KEY_DOWN) {
		//center = center - myGL::zAxis;
		//eye = eye - myGL::zAxis;
		//move_z += 0.1;
		if (rotatingGlobal) {
			rotate_x_global += 5;
		}
		else {
			rotate_x_obj -= 5;
		}
	}

	glutPostRedisplay();
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void setColour(Colour drawingColour) {
	switch (drawingColour) {
	case Colour::Black:
		glColor3f(0, 0, 0);
		break;
	case Colour::White:
		glColor3f(1, 1, 1);
		break;
	case Colour::Red:
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case Colour::Green:
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case Colour::Blue:
		glColor3f(0, 0, 1);
		break;
	case Colour::Yellow:
		glColor3f(1, 1, 0);
		break;
	}
}

void renderScene()
{
	glRotatef(rotate_x_global, 1.0, 0.0, 0.0);
	glRotatef(rotate_y_global, 0.0, 1.0, 0.0);

	//cout << "CustomObj = " << customObj << endl;
	//cout << "customObj->isDataLoaded() = " << fmt::to_string(customObj->isDataLoaded()) << endl;

	//glPushMatrix();
	//glMatrixMode(GL_MODELVIEW);			// Model view matrix
	//glLoadIdentity();					// -||-
	//gluLookAt(
	//	move_x, 0, 2.5 + move_z,
	//	0, 0, 0,
	//	0, 1, 0
	//);
	//glPopMatrix();

	if (customObj && customObj->isDataLoaded()) {
		glTestingPoints();
		setColour(drawingColour);
		glObj();
	}
	else {
		exampleCube();
		// GLUquadricObj* quadro = gluNewQuadric();
		// gluSphere(quadro, 1, 48, 48);
	}
}

void glTestingPoints()
{
	// Now load the testing poitns
	glColor3f(1, 0, 0);
	vector<dvec4> testingPoints = customObj->getTestingPoints();
	
	//glLoadIdentity();
	for (dvec4 testingPoint : testingPoints) {
		//cout << "Testing point: " << fmt::format("{} {} {}\n", testingPoint[0], testingPoint[1], testingPoint[2]);
		glPushMatrix();
		glTranslatef(testingPoint[0], testingPoint[1], testingPoint[2]);
		//glLoadIdentity();
		glutSolidSphere(RADIUS, 48, 48);
		glPopMatrix();
	}	
}

void updateEye()
{
	cout << "Enter eye coordinates x, y, z << ";
	//scanf_s("%f, %f, %f", &eye[0], &eye[1], &eye[2]);
	cin >> eye[0];
	cin >> eye[1];
	cin >> eye[2];
	cout << endl;
	glutPostRedisplay();
}

void updateCenter()
{
	cout << "Enter center coordinates x, y, z << ";
	//scanf_s("%f, %f, %f", &center[0], &center[1], &center[2]);
	cin >> center[0];
	cin >> center[1];
	cin >> center[2];
	cout << endl;
	glutPostRedisplay();
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clear everything
		customObj = nullptr;
		cout << "Object reset -> enter a new object to render: << ";
		string toLoad;
		cin >> toLoad;
		cout << "Loading object " << toLoad << endl;
		customObj = make_unique<Model>(toLoad);

		// Okay got it now
		customObj->loadData();
		cout << customObj->dataStr();
		glutPostRedisplay();
	}
}

void keyP()
{
	if (!customObj || !customObj->isDataLoaded()) {
		cout << "Object data not loaded!" << endl;
		return;
	}

	string buffer;
	dvec4 pointToCheck;
	cout << "Please enter the point to be queried << ";

	cin >> buffer;
	pointToCheck[0] = stod(buffer);

	cin >> buffer;
	pointToCheck[1] = stod(buffer);

	cin >> buffer;
	pointToCheck[2] = stod(buffer);

	// Set the homo var
	pointToCheck[3] = 1;

	/*vector<string> splitArr(3);
	boost::split(splitArr, buffer, boost::is_any_of(" "));
	dvec4 pointToCheck{ stod(splitArr[0]), stod(splitArr[1]), stod(splitArr[2]), 1 };
	*/

	// Add it to testing points here, request re-drawing
	customObj->addTestingPoint(pointToCheck);
	glutPostRedisplay();

	pointToCheck = antiTransformVex(pointToCheck);
	int statusCode = queryPoints(pointToCheck);
	if (statusCode == -1) {
		cout << "The point is located inside the polygon!" << endl;
	}
	else if (statusCode == 0) {
		cout << "The point is located on the polygon mantle!" << endl;
	}
	else {
		cout << "The point is located outside the polygon!" << endl;
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case 'e':
		updateEye();
		break;
	case 'c':
		updateCenter();
		break;
	case 'p':
		keyP();
		break;
	case 'f':
	{
		// Switch draw form!
		if (currDrawForm == GL_LINE_LOOP) {
			currDrawForm = GL_TRIANGLES;
		}
		else if (currDrawForm == GL_TRIANGLES) {
			currDrawForm = GL_LINE_LOOP;
		}
		glutPostRedisplay();
		break;
	}
	case 'w':
		eye.z -= 0.25;
		center.z -= 0.25;
		glutPostRedisplay();
		break;
	case 's':
		eye.z += 0.25;
		center.z += 0.25;
		glutPostRedisplay();
		break;
	case 'a':
		eye.x += 0.25;
		center.x += 0.25;
		glutPostRedisplay();
		break;
	case 'd':
		eye.x -= 0.25;
		center.x -= 0.25;
		glutPostRedisplay();
		break;
	case 'x':
	{
		cout << "Please input the new colour! Choose among: Black, White, Red, Green, Blue, and Yellow!" << endl
			<< "<< ";
		string colour;
		cin >> colour;

		std::transform(
			colour.begin(),			// What to transform - begin iter
			colour.end(),			// What to transform - end iter
			colour.begin(),			// Where to save - begin iter
			[](unsigned char c) {return std::tolower(c); });		// Lambda that does the transf
		auto it = colourTbl.find(colour);
		if (it == colourTbl.end()) {
			throw invalid_argument("Unknown colour!");
		}

		drawingColour = it->second;
		glutPostRedisplay();
		break;
	}
	case 'z':
		// Switch the global toggle
		rotatingGlobal ^= 1;
		break;
	case 'r':
	{
		string line;
		vector<string> parts;
		while (true) {
			getline(cin, line);
			if (line == "") {
				continue;
			}
			else if (line == "quit") {
				break;
			}

			boost::split(parts, line, boost::is_any_of(" "));
			dvec4 pointToCheck{ stod(parts[0]), stod(parts[1]), stod(parts[2]), 1 };
			customObj->addTestingPoint(pointToCheck);
			glutPostRedisplay();

			pointToCheck = antiTransformVex(pointToCheck);
			int statusCode = queryPoints(pointToCheck);
			if (statusCode == -1) {
				cout << "The point is located inside the polygon!" << endl;
			}
			else if (statusCode == 0) {
				cout << "The point is located on the polygon mantle!" << endl;
			}
			else {
				cout << "The point is located outside the polygon!" << endl;
			}
			break;
		}
	}
	default:
		cout << "Specified key(" << theKey << ") is not supported! :(" << endl;
	}
}

inline void transformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// glTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	glScalef(scaleFactor, scaleFactor, scaleFactor);
	glTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline dvec4 antiTransformVex(dvec4 toTransform)
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	GLdouble scaleFactor = customObj->getScaleFactor();

	dmat4 translateMat = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{bodyCentre[0], bodyCentre[1], bodyCentre[2], 1},
	};

	dmat4 scaleMat = {
		{1. / scaleFactor, 0, 0, 0},
		{0, 1. / scaleFactor, 0, 0},
		{0, 0, 1. / scaleFactor, 0},
		{0, 0, 0, 1},
	};

	// First scale then translate
	// In code: translateMat * scaleMat * vex
	dvec4 resVex = translateMat * scaleMat * toTransform;
	return resVex;
}

int queryPoints(dvec4 toCheck)
{
	// Check if inside
	bool inside = true;
	bool onMantle = false;
	vector<dvec4> planes = customObj->getPlanes();
	for (dvec4 plane : planes) {
		GLdouble res = glm::dot(toCheck, plane);
		if (glm::abs(res) <= EPSILON) {
			onMantle = true;
			break;
		}
		else if (res > 0) {
			/// Means it's not inside
			inside = false;
			break;
		}
	}

	// Print res
	if (onMantle) {
		return 0;
	}
	else if (inside) {
		return -1;
	}
	else {
		return 1;
	}
}

void exampleCube()
{
	glPushMatrix();
	glRotatef(rotate_x_obj, 1.0, 0.0, 0.0);
	glRotatef(rotate_y_obj, 0.0, 1.0, 0.0);

	// FRONT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0); glVertex3f(-0.5, -0.5, -0.5);
	glColor3f(0.0, 1.0, 0.0); glVertex3f(-0.5, 0.5, -0.5);
	glColor3f(0.0, 0.0, 1.0); glVertex3f(0.5, 0.5, -0.5);
	glColor3f(1.0, 0.0, 1.0); glVertex3f(0.5, -0.5, -0.5);
	glEnd();

	// BACK
	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glEnd();

	// Purple side - RIGHT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glEnd();

	// Green side - LEFT
	glBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// Blue side - TOP
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glEnd();

	// Red side - BOTTOM
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();
	glPopMatrix();
}