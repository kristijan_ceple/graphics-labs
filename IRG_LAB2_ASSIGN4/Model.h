#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <GL/freeglut.h>
using namespace std; using namespace glm;

#pragma once
class Model
{
private:
	const string path;
	string name;
	vector<dvec4> vertices;
	vector<ivec3> polygons;
	vector<dvec4> planes;
	vector<dvec4> testingPoints;
	
	GLdouble minX, maxX;
	GLdouble minY, maxY;
	GLdouble minZ, maxZ;

	dvec4 bodyCentre;
	dvec3 scaleFactorVex;
	GLdouble scaleFactor;
	
	bool dataLoaded = false;
public:
	inline static const GLdouble INTERVAL = 2;
	Model(string path);
	bool loadData();
	bool isDataLoaded();
	vector<dvec4> getVertices();
	vector<ivec3> getPolygons();
	vector<dvec4> getPlanes();
	const string getPath();
	const string getName();
	const string dataStr();
	const dvec4 getBodyCentre();
	const double getScaleFactor();
	const dvec3 getScaleFactors();
	void addTestingPoint(dvec4 toTest);
	vector<dvec4> getTestingPoints();
};

