#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include "Model.h"
#include <fmt\format.h>
#include <boost/algorithm/string.hpp>
using namespace std; using namespace glm;

 // #################################################################################################################
 // ##########################################		CONSTANTS		################################################
 // #################################################################################################################

GLuint window;
GLuint width = 640, height = 480;
GLdouble aspectRatio = width / height;
GLdouble move_z = 0, move_x = 0, rotate_x = 0, rotate_y = 0;
GLuint mouse_x, mouse_y;
GLbyte drawingColour = 'w';
unique_ptr<Model> customObj = nullptr;
const GLdouble EPSILON = 1E-6;
const GLdouble RADIUS = 0.025;

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void setColour(char drawingColour);
void specialKeys(int key, int x, int y);
void exampleCube();
void glObj();
int queryPoints(dvec4 toCheck);
dvec4 antiTransformVex(dvec4);
void transformModel();
void keyP();
void glTestingPoints();

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	// First load data
	string toLoad;
	cout << "Filename to load << ";
	cin >> toLoad;
	customObj = make_unique<Model>(toLoad);
	cout << "Loading object " << toLoad << endl;
	customObj->loadData();
	cout << customObj->dataStr() << endl;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("Custom object rendering!");
	glutDisplayFunc(myDisplay);
	glutPassiveMotionFunc(mouseMoved);
	glutSpecialFunc(specialKeys);
	glutReshapeFunc(myReshape);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);

	cout << "Object rendering program!" << endl;
	cout << "Right click clears the polygon and enables inputting another object to render!" << endl;
	cout << "Press p to input a point that will be tested whether it is inside, outside, or on the mantles of the convex polygon!" << endl;
	cout << "Press r, g, b, y, k, or w to set color!" << endl;
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	//cout << "rotate_x >> " << rotate_x << endl;
	//cout << "rotate_y >> " << rotate_y << endl;
	// glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
	glLoadIdentity();
	glEnable(GL_DEPTH_TEST);
	gluLookAt(
		0, 0, 5,
		0, 0, 0,
		0, 1, 0
	);
	renderScene();			// First render the scene!
	glutSwapBuffers();		// Render complete -- swap buffers!
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	glMatrixMode(GL_PROJECTION);		// Projection matrix
	glLoadIdentity();					// Reset to identity matrix
	//glFrustum(-10.0, 10, -10.0, 10.0, 50, 150.0);
	gluPerspective(45, aspectRatio, 0.1, 100);
	//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	//gluOrtho2D(-2, 2, -2, 2);
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();					// -||-
	gluLookAt(
		0, 0, 2.5,
		0, 0, 0,
		0, 1, 0
	);
	glEnable(GL_DEPTH_TEST);
}

void glObj()
{
	glPushMatrix();
	//glRotatef(rotate_x, 1.0, 0.0, 0.0);
	//glRotatef(rotate_y, 0.0, 1.0, 0.0);
	transformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> vertices = customObj->getVertices();

	for (ivec3 pol : pols) {
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		glBegin(GL_LINE_LOOP);
		glVertex3d(first.x, first.y, first.z);
		glVertex3d(second.x, second.y, second.z);
		glVertex3d(third.x, third.y, third.z);
		glEnd();
	}
	glPopMatrix();
}

void specialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		move_x += 0.1;
		rotate_y -= 5;
	}
	else if (key == GLUT_KEY_LEFT) {
		move_x -= 0.1;
		rotate_y += 5;
	}
	else if (key == GLUT_KEY_UP) {
		move_z -= 0.1;
		rotate_x += 5;
	}
	else if (key == GLUT_KEY_DOWN) {
		move_z += 0.1;
		rotate_x -= 5;
	}

	glutPostRedisplay();
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void setColour(char drawingColour) {
	switch (drawingColour) {
	case 'k':
		glColor3f(0, 0, 0);
		break;
	case 'w':
		glColor3f(1, 1, 1);
		break;
	case 'r':
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case 'g':
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 'b':
		glColor3f(0, 0, 1);
		break;
	case 'y':
		glColor3f(1, 1, 0);
		break;
	}
}

void renderScene()
{
	glRotatef(rotate_x, 1.0, 0.0, 0.0);
	glRotatef(rotate_y, 0.0, 1.0, 0.0);

	//cout << "CustomObj = " << customObj << endl;
	//cout << "customObj->isDataLoaded() = " << fmt::to_string(customObj->isDataLoaded()) << endl;

	//glPushMatrix();
	//glMatrixMode(GL_MODELVIEW);			// Model view matrix
	//glLoadIdentity();					// -||-
	//gluLookAt(
	//	move_x, 0, 2.5 + move_z,
	//	0, 0, 0,
	//	0, 1, 0
	//);
	//glPopMatrix();

	if (customObj && customObj->isDataLoaded()) {
		glTestingPoints();
		setColour(drawingColour);
		glObj();
	}
	else {
		exampleCube();
		// GLUquadricObj* quadro = gluNewQuadric();
		// gluSphere(quadro, 1, 48, 48);
	}
}

void glTestingPoints()
{
	// Now load the testing poitns
	glColor3f(1, 0, 0);
	vector<dvec4> testingPoints = customObj->getTestingPoints();
	
	//glLoadIdentity();
	for (dvec4 testingPoint : testingPoints) {
		//cout << "Testing point: " << fmt::format("{} {} {}\n", testingPoint[0], testingPoint[1], testingPoint[2]);
		glPushMatrix();
		glTranslatef(testingPoint[0], testingPoint[1], testingPoint[2]);
		//glLoadIdentity();
		glutSolidSphere(RADIUS, 48, 48);
		glPopMatrix();
	}	
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clear everything
		customObj = nullptr;
		cout << "Object reset -> enter a new object to render: << ";
		string toLoad;
		cin >> toLoad;
		cout << "Loading object " << toLoad << endl;
		customObj = make_unique<Model>(toLoad);

		// Okay got it now
		customObj->loadData();
		cout << customObj->dataStr();
		glutPostRedisplay();
	}
}

void keyP()
{
	if (!customObj || !customObj->isDataLoaded()) {
		cout << "Object data not loaded!" << endl;
		return;
	}

	string buffer;
	dvec4 pointToCheck;
	cout << "Please enter the point to be queried << ";

	cin >> buffer;
	pointToCheck[0] = stod(buffer);

	cin >> buffer;
	pointToCheck[1] = stod(buffer);

	cin >> buffer;
	pointToCheck[2] = stod(buffer);

	// Set the homo var
	pointToCheck[3] = 1;

	/*vector<string> splitArr(3);
	boost::split(splitArr, buffer, boost::is_any_of(" "));
	dvec4 pointToCheck{ stod(splitArr[0]), stod(splitArr[1]), stod(splitArr[2]), 1 };
	*/

	// Add it to testing points here, request re-drawing
	customObj->addTestingPoint(pointToCheck);
	glutPostRedisplay();

	pointToCheck = antiTransformVex(pointToCheck);
	int statusCode = queryPoints(pointToCheck);
	if (statusCode == -1) {
		cout << "The point is located inside the polygon!" << endl;
	}
	else if (statusCode == 0) {
		cout << "The point is located on the polygon mantle!" << endl;
	}
	else {
		cout << "The point is located outside the polygon!" << endl;
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case 'p':
		keyP();
		break;
	case 'r':
	case 'w':
	case 'g':
	case 'b':
	case 'y':
	case 'k':
		drawingColour = theKey;
		glutPostRedisplay();
		break;
	case 'l':
		string line;
		vector<string> parts;
		while(true) {
			getline(cin, line);
			if (line == "") {
				continue;
			}
			else if (line == "quit") {
				break;
			}

			boost::split(parts, line, boost::is_any_of(" "));
			dvec4 pointToCheck{stod(parts[0]), stod(parts[1]), stod(parts[2]), 1};
			customObj->addTestingPoint(pointToCheck);
			glutPostRedisplay();

			pointToCheck = antiTransformVex(pointToCheck);
			int statusCode = queryPoints(pointToCheck);
			if (statusCode == -1) {
				cout << "The point is located inside the polygon!" << endl;
			}
			else if (statusCode == 0) {
				cout << "The point is located on the polygon mantle!" << endl;
			}
			else {
				cout << "The point is located outside the polygon!" << endl;
			}
		}
		break;
	//default:
	//	cout << "Specified colour(" << theKey << ") is not supported! :(" << endl;
	}
}

inline void transformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// glTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	glScalef(scaleFactor, scaleFactor, scaleFactor);
	glTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline dvec4 antiTransformVex(dvec4 toTransform)
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	GLdouble scaleFactor = customObj->getScaleFactor();

	dmat4 translateMat = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{bodyCentre[0], bodyCentre[1], bodyCentre[2], 1},
	};

	dmat4 scaleMat = {
		{1. / scaleFactor, 0, 0, 0},
		{0, 1. / scaleFactor, 0, 0},
		{0, 0, 1. / scaleFactor, 0},
		{0, 0, 0, 1},
	};

	// First scale then translate
	// In code: translateMat * scaleMat * vex
	dvec4 resVex = translateMat * scaleMat * toTransform;
	return resVex;
}

int queryPoints(dvec4 toCheck)
{
	// Check if inside
	bool inside = true;
	bool onMantle = false;
	vector<dvec4> planes = customObj->getPlanes();
	for (dvec4 plane : planes) {
		GLdouble res = glm::dot(toCheck, plane);
		if (glm::abs(res) <= EPSILON) {
			onMantle = true;
			break;
		}
		else if (res > 0) {
			/// Means it's not inside
			inside = false;
			break;
		}
	}

	// Print res
	if (onMantle) {
		return 0;
	}
	else if (inside) {
		return -1;
	}
	else {
		return 1;
	}
}

void exampleCube()
{
	glPushMatrix();
	glRotatef(rotate_x, 1.0, 0.0, 0.0);
	glRotatef(rotate_y, 0.0, 1.0, 0.0);

	// FRONT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0); glVertex3f(-0.5, -0.5, -0.5);
	glColor3f(0.0, 1.0, 0.0); glVertex3f(-0.5, 0.5, -0.5);
	glColor3f(0.0, 0.0, 1.0); glVertex3f(0.5, 0.5, -0.5);
	glColor3f(1.0, 0.0, 1.0); glVertex3f(0.5, -0.5, -0.5);
	glEnd();

	// BACK
	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glEnd();

	// Purple side - RIGHT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glEnd();

	// Green side - LEFT
	glBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// Blue side - TOP
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glEnd();

	// Red side - BOTTOM
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();
	glPopMatrix();
}