﻿#include <iostream>
#include <glm/glm.hpp>
#include <io.h>
#include <fcntl.h>
using namespace std;
using namespace glm;

/*
const string WHITESPACE = " \n\r\f\v\t";

void ltrim(string& s)
{
	// Wanna trim the leading whitespace
	size_t index = s.find_first_not_of(WHITESPACE);
	const char* subString = s.substr(index, s.size()).c_str();
	s.assign(subString);
}

void rtrim(string& s)
{
	// Let's remove the trailing whitespace
	size_t index = s.find_last_not_of(WHITESPACE);
	const char* subString = s.substr(0, index).c_str();
	s.assign(subString);
}

void trim(string& s)
{
	ltrim(s);
	rtrim(s);
}

dvec3 parseString2Vector(string toParse)
{
	// First need to remove all the leading and trailing whitespace
	// The goal is to get a i, b j, c k

	double a, b, c;
	trim(toParse);
}
*/

// Inline cause this is basically one larger macro??? Just experimenting right now actually
inline void printDMat3(dmat3 toPrint)
{
	printf("- %8s %8s %8s -\n", "", "", "");
	printf("| %f %f %f |\n", toPrint[0][0], toPrint[1][0], toPrint[2][0]);
	printf("| %f %f %f |\n", toPrint[0][1], toPrint[1][1], toPrint[2][1]);
	printf("| %f %f %f |\n", toPrint[0][2], toPrint[1][2], toPrint[2][2]);
	printf("- %8s %8s %8s -\n", "", "", "");
}

// This actually works LMAO!
template<typename T, int tcols, int trows> void printGenericMatrix(mat<tcols, trows, f64, defaultp> toPrint, int cols, int rows)
{
	// Just leaving here
}

int main(void)
{
	cout << "Welcome to the 1st lab exercise!" << endl;
	cout << "Demonstrating a three-variable equation solver!" << endl
		<< "Please enter x, y, z components one by one: "
		<< endl;
	
	double input[3][4];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cin >> input[i][j];
		}
	}

	// Have them all in the simplified matrix; Now need to form a matrix from this
	// Do it by column
	dmat3 glmMat;
	for (int j = 0; j < 3; j++) {
		glmMat[j] = { input[0][j], input[1][j], input[2][j] };
	}

	cout << "The equation system in matrix form: " << endl;
	printDMat3(glmMat);

	// Now get the result vector!
	dvec3 resVec = { input[0][3], input[1][3], input[2][3] };

	cout << "The system solutions column-vector:" << endl;
	printf_s("[ %f %f %f ]\n", resVec[0], resVec[1], resVec[2]);

	// Let's solve the equation
	// Solution column-vector = inverse(matrix) * solution-column-vector
	dvec3 solVec = inverse(glmMat) * resVec;
	
	// Let's print it out
	cout << "The system solutions column-vector:" << endl;
	printf_s("[ %f %f %f ]", solVec[0], solVec[1], solVec[2]);

	return 0;
}