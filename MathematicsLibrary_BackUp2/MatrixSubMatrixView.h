#pragma once
#ifndef MATRIXSUBMATRIXVIEW_H
#define MATRIXSUBMATRIXVIEW_H

#include "AbstractMatrix.h"
class MatrixSubMatrixView :
	public AbstractMatrix
{
private:
//	int rowIndexes[];		Some bug in VC++
	size_t* _colIndice;
	size_t* _rowIndice;
	IMatrix* _origMat;
	MatrixSubMatrixView(IMatrix&, size_t*, size_t, size_t*, size_t);
public:
	MatrixSubMatrixView(IMatrix& origMat, size_t, size_t);
	size_t getRowsCount();
	size_t getColsCount();
	double get(size_t, size_t);
	IMatrix* set(size_t, size_t, double);
	IMatrix* copy();
	IMatrix* newInstance(size_t, size_t);
	IMatrix* subMatrix(size_t, size_t, bool);
};
#endif
