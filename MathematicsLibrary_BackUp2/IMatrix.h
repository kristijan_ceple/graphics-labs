#pragma once
#ifndef IMATRIX_H
#define IMATRIX_H

#include "IVector.h"

class IMatrix
{
public:
	virtual int getRowsCount() = 0;
	virtual int getColCount() = 0;
	virtual double get(int, int) = 0;
	virtual IMatrix* set(int, int, double) = 0;
	virtual IMatrix* copy() = 0;
	virtual IMatrix* newInstance() = 0;
	virtual IMatrix* nTranspose() = 0;
	virtual IMatrix* add(IMatrix &other) = 0;
	virtual IMatrix* nAdd(IMatrix &other) = 0;
	virtual IMatrix* sub(IMatrix& other) = 0;
	virtual IMatrix* nSub(IMatrix& other) = 0;
	virtual IMatrix* nMultiply(IMatrix &other) = 0;
	virtual double determinant() = 0;
	virtual IMatrix* subMatrix(int, int, bool) = 0;
	virtual IMatrix* nInvert() = 0;
	virtual double** toArray() = 0;
	virtual IVector* toVector(bool) = 0;
};

#endif
