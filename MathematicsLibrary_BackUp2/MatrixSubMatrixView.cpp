#include "MatrixSubMatrixView.h"

MatrixSubMatrixView::MatrixSubMatrixView(IMatrix& origMat, size_t row, size_t col)
{
	this->_origMat = &origMat;
	this->_rowIndice = new size_t(row);
	this->_colIndice = new size_t(col);
}

MatrixSubMatrixView::MatrixSubMatrixView(IMatrix& origMat, size_t*, size_t numRows, size_t*, size_t numCols)
{
	this->_origMat = &origMat;
	this->_rowIndice = new size_t[numRows];
	this->_colIndice = new size_t[numCols];
}