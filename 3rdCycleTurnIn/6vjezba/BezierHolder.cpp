#include "BezierHolder.h"
#include <glm/exponential.hpp>
#include <glm\geometric.hpp>
#include <GL/glut.h>
#include <boost/math/special_functions/binomial.hpp>

BezierHolder::BezierHolder()
{
	n = -1;		// Dial n back by 1 so instead of 1->n we got 0->(n-1)
}

void BezierHolder::calculateBinCoeffs()
{
	// First, let's preprocess and prepare the bcoeffs vector
// #################################	!!!Optimised algorithm!!!		###########################################
	GLint limit = n/2;
	GLint i;
	for (i = 0; i <= limit; i++) {
		this->bcoeffs.push_back(boost::math::binomial_coefficient<GLdouble>(n, i));
	}

	// Now mirror them!
	if (n % 2 == 0) {
		limit--;		// Moving the limit, for example: 1 2 1, here limit is 1, but we've already covered it in the upper loop
	}

	for (GLint i = limit; i >= 0; i--) {
		this->bcoeffs.push_back(this->bcoeffs[i]);
	}
// #################################	!!!Optimised algorithm!!!		###########################################

	// Non - optimised loop
	/*for (int i = 0; i <= n; i++) {
		this->bcoeffs.push_back(boost::math::binomial_coefficient<GLdouble>(n, i));
	}*/

	this->coeffsCalculated = true;
}

vector<fvec3> BezierHolder::calculateBezierCurve()
{
	if (!this->coeffsCalculated) {
		this->calculateBinCoeffs();		// Now we're ready
	}
	
	// GLint t1 = t <= ceil(n / 2) ? t : 1 - t;
	for (GLdouble t = 0; t <= 1; t += STEP) {
		GLdouble reverseT = 1 - t;
		fvec3 sum{ 0, 0, 0 };

		for (GLint i = 0, j = n; i <= n; i++, j--) {
			GLdouble bernstein = bcoeffs[i] * glm::pow(t, i)* glm::pow(reverseT, j);
			fvec3 toMultWith = this->controlPolygon[i];
			sum += fvec3{toMultWith[0] * bernstein, toMultWith[1] * bernstein, toMultWith[2] * bernstein};
		}

		// For this t we got a point calculated in the sum! Push it back onto the vector of points!
		this->bezierCurve.push_back(sum);
	}

	// Done!
	curveReady = true;
	return this->bezierCurve;
}

vector<fvec3> BezierHolder::getBezierCurve()
{
	return this->bezierCurve;
}

GLboolean BezierHolder::areCoeffsCalculated()
{
	return this->coeffsCalculated;
}

void BezierHolder::addControlPoint(fvec3 toAdd)
{
	this->controlPolygon.push_back(toAdd);
	n++;
	this->coeffsCalculated = false;
}

vector<fvec3> BezierHolder::getControlPolygon()
{
	return this->controlPolygon;
}
