#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <GL/freeglut.h>
using namespace std; using namespace glm;

#pragma once
class Model
{
private:
	const string path;
	string name;
	vector<dvec4> vertices;
	vector<ivec3> polygons;
	vector<dvec4> planes;
	vector<dvec4> projectionPlanes;
	vector<dvec4> testingPoints;
	
	GLdouble minX, maxX;
	GLdouble minY, maxY;
	GLdouble minZ, maxZ;

	dvec4 bodyCentre;
	dvec3 scaleFactorVex;
	GLdouble scaleFactor;
	
	bool dataLoaded = false;
public:
	inline static const GLdouble INTERVAL = 2;
	Model(string path);
	bool loadData();
	bool isDataLoaded();
	const string dataStr();

	void addTestingPoint(dvec4 toTest);
	void postTransformPlanes(dmat4 projectionMat);

	const dvec4 getBodyCentre();
	const double getScaleFactor();
	const dvec3 getScaleFactors();
	vector<dvec4> getVertices();
	vector<ivec3> getPolygons();
	vector<dvec4> getPlanes();
	const string getPath();
	const string getName();
	vector<dvec4> getTestingPoints();
	vector<dvec4> getProjectionPlanes();
};

