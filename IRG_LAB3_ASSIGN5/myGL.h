#pragma once
#include <string>
#include <stack>
#include <glm/glm.hpp>
#include <GL/glut.h>
#include <vector>

using namespace std; using namespace glm;

class myGL
{
private:
	inline static GLint currState = GL_MODELVIEW;
	inline static fmat4 modelviewMatrix;
	inline static fmat4 projectionMatrix;
	inline static fmat4 totalMatrix;
	inline static stack<fmat4> matricesStack;

	// inline static GLbyte drawCounter = 0;
	// static vector<ivec3> vexes;

	inline static GLboolean ECSet = false;
	inline static dvec3 eye;
	inline static dvec3 center;

	myGL();			// Private constructor
public:
	inline static fvec3 zAxis, yAxis, xAxis, vup;

	static void myglPushMatrix();
	static void myglPopMatrix();

	static void myglMatrixMode(int toSet);
	static void mygluLookAt(
		GLfloat eyeX, GLfloat eyeY, GLfloat eyeZ,
		GLfloat centerX, GLfloat centerY, GLfloat centerZ,
		GLfloat upX, GLfloat upY, GLfloat upZ
	);

	static void myglLoadIdentity();
	static void myglLoadIdentityExclusive();
	static void myglTranslatef(GLfloat dx, GLfloat dy, GLfloat dz);
	static void myglRotatef(GLfloat angle, GLfloat rx, GLfloat ry, GLfloat rz);
	static void myglScalef(GLfloat sx, GLfloat sy, GLfloat sz);

	static inline void myglBegin(int code);
	static inline void myglVertex3f(GLfloat x, GLfloat y, GLfloat z);
	static inline void myglEnd();

	static void myglPerspective();
};

inline void myGL::myglBegin(int code)
{
	// Prepare the total matrix!
	// First modelview, then projection matrix!
	myGL::totalMatrix = myGL::projectionMatrix * myGL::modelviewMatrix;

	// myGL::vexes.clear();			// Clear the points!
	glBegin(code);					// Initiate OGL background drawing!
}

inline void myGL::myglEnd()
{
	glEnd();		// Delegator function
}

inline void myGL::myglVertex3f(GLfloat x, GLfloat y, GLfloat z)
{
	// Now draw the polygon! Multiply each point with the matrix before drawing!
	//fvec4 noProj = myGL::modelviewMatrix * fvec4{ x, y, z, 1 };
	fvec4 vexTmp = myGL::totalMatrix * fvec4{ x, y, z, 1 };
	fvec3 vexToDraw = fvec3{ vexTmp[0] / vexTmp[3], vexTmp[1] / vexTmp[3], vexTmp[2] / vexTmp[3] };
	glVertex3f(vexToDraw[0], vexToDraw[1], vexToDraw[2]);
}