#pragma once
#include <glm/glm.hpp>
#include <vector>
using namespace glm;

using TriangleList = std::vector<ivec3>;
using VertexList = std::vector<dvec3>;
class ExampleSphere
{
    static inline const float X = .525731112119133606f;
    static inline const float Z = .850650808352039932f;
    static inline const float N = 0.f;
public:
    VertexList vertices =
    {
      {-X,N,Z}, {X,N,Z}, {-X,N,-Z}, {X,N,-Z},
      {N,Z,X}, {N,Z,-X}, {N,-Z,X}, {N,-Z,-X},
      {Z,X,N}, {-Z,X, N}, {Z,-X,N}, {-Z,-X, N}
    };

    TriangleList triangles =
    {
      ivec3{0,4,1},ivec3{0,9,4},ivec3{9,5,4},ivec3{4,5,8},ivec3{4,8,1},
      ivec3{8,10,1},ivec3{8,3,10},ivec3{5,3,8},ivec3{5,2,3},ivec3{2,7,3},
      ivec3{7,10,3},ivec3{7,6,10},ivec3{7,11,6},ivec3{11,0,6},ivec3{0,1,6},
      ivec3{6,1,10},ivec3{9,0,11},ivec3{9,11,2},ivec3{9,2,5},ivec3{7,2,11}
    };
};

