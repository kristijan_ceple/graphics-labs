#include <iostream>
#include <unordered_map>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include "Model.h"
#include <boost/algorithm/string.hpp>
#include "myGL.h"
#include "LightOps.h"
#include <fmt\format.h>
using namespace std; using namespace glm;

 // #################################################################################################################
 // ##########################################		CONSTANTS		################################################
 // #################################################################################################################

GLuint window;
GLuint width = 640, height = 480;
GLdouble aspectRatio = width / height;
GLdouble move_z = 0, move_x = 0, rotate_x_global = 0, rotate_y_global = 0, rotate_x_obj = 0, rotate_y_obj = 0;
GLboolean rotatingGlobal = true;
GLboolean OGLdraw = false;

GLuint mouse_x, mouse_y;
unique_ptr<Model> customObj = nullptr;
unique_ptr<LightOps> lightOps = nullptr;
const GLdouble EPSILON = 1E-6;
const GLdouble RADIUS = 0.025;
const GLdouble LIGHT_SOURCE_RADIUS = 0.125;

static unordered_map<string, SourceType> const sourceTypeTbl = {
	{"ambient", SourceType::Ambient},
	{"diffuse", SourceType::Diffuse}
};

enum class Colour {Black, White, Red, Green, Blue, Yellow};
static unordered_map<string, Colour> const colourTbl = {
	{"black", Colour::Black},
	{"white", Colour::White},
	{"red", Colour::Red},
	{"green", Colour::Green},
	{"blue", Colour::Blue},
	{"yellow", Colour::Yellow},
};
Colour drawingColour = Colour::White;

fvec3 eye = { 0, 0, 5 };
fvec3 center = { 0, 0, 4 };

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void setColour(Colour drawingColour);
void specialKeys(int key, int x, int y);
void exampleCube();
void myglObj();
void glObj();
int queryPoints(dvec4 toCheck);
dvec4 antiTransformVex(dvec4);
void transformModel();
void glTransformModel();
void keyP();
void glTestingPoints();
void updateEye();
void updateCenter();
void loadObj(string);
void glLightSources();
void hardcodeLightSources1();
void hardcodeLightSources2();
void hardcodeLightSources3();

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	// First load data
	string toLoad;
	cout << "Filename to load << ";
	cin >> toLoad;
	loadObj(toLoad);
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("Lighting!!!");
	glutDisplayFunc(myDisplay);
	glutPassiveMotionFunc(mouseMoved);
	glutSpecialFunc(specialKeys);
	glutReshapeFunc(myReshape);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glEnable(GL_DEPTH_TEST);

	cout << "Object rendering program!" << endl;
	cout << "Right click clears the polygon and enables inputting another object to render!" << endl;
	cout << "Middle mouse button clears the rotation and lighting effects!" << endl;

	cout << "Press p to input a point that will be tested whether it is inside, outside, or on the mantles of the convex polygon!" << endl;
	cout << "Press e to update the eye coords, and c to update the center(look-at point) coords!" << endl;
	cout << "Press l to input a new light source!" << endl;
	cout << "Press 1 = Constanst Shading, and 2 = Gouraud Shading!" << endl;
	cout << "Press m to input new material lighting coeffs(default is 1, 1)!" << endl;
	cout << "Press f to change the current drawing form(wireframe vs. filled polygon)!" << endl;
	cout << "By default, Constant Shading is used!" << endl;
	cout << "Press W, A, S, D to move around in the scene! By pressing the (gaming?) keys or numpad keys you can rotate the scene or the object!" << endl;
	cout << "Press z to switch between global rotation and object rotation(has effects what do the keys rotate)!" << endl;
	cout << "Press 8, 9, or 0 to hardcode specific light sources!" << endl;

	cout << "Press x to change colour!" << endl;
	cout << "Press q to toggle OGL cube wire-frame drawing!" << endl;
	cout << "Irrelevant -- Press r for specific testing points check!" << endl;
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	//cout << "rotate_x >> " << rotate_x << endl;
	//cout << "rotate_y >> " << rotate_y << endl;
	// glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	myGL::myglMatrixMode(GL_PROJECTION);
	myGL::myglLoadIdentityExclusive();
	myGL::myglMatrixMode(GL_MODELVIEW);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
	myGL::myglLoadIdentity();
	myGL::mygluLookAt(
		eye[0], eye[1], eye[2],
		center[0], center[1], center[2],
		0, 1, 0
	);

	myGL::myglPerspective();
	if (OGLdraw) {
		glMatrixMode(GL_MODELVIEW);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
		glLoadIdentity();
		gluLookAt(
			eye.x, eye.y, eye.z,
			0, 0, 0,
			0, 1, 0
		);
	}

	renderScene();			// First render the scene!
	glutSwapBuffers();		// Render complete -- swap buffers!
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	myGL::myglMatrixMode(GL_PROJECTION);		// Projection matrix
	myGL::myglLoadIdentity();					// Reset to identity matrix
	// Later will set the projection matrix!
	myGL::myglMatrixMode(GL_MODELVIEW);			// Model view matrix
	myGL::myglLoadIdentity();					// -||-
	myGL::mygluLookAt(
		eye[0], eye[1], eye[2],
		center[0], center[1], center[2],
		0, 1, 0
	);
	myGL::myglPerspective();

	if (OGLdraw) {
		glMatrixMode(GL_PROJECTION);		// Projection matrix
		glLoadIdentity();
		gluPerspective(45, aspectRatio, 0.1, 100);
		//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
		//gluOrtho2D(-2, 2, -2, 2);
		glMatrixMode(GL_MODELVIEW);			// Model view matrix
		glLoadIdentity();
		gluLookAt(
			eye.x, eye.y, eye.z,
			0, 0, 0,
			0, 1, 0
		);
	}
}

void glObj()
{
	//cout << "Entered glObj!" << endl;

	glPushMatrix();
	glRotatef(rotate_x_obj, 1.0, 0.0, 0.0);
	glRotatef(rotate_y_obj, 0.0, 1.0, 0.0);
	glTransformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> vertices = customObj->getVertices();

	for (ivec3 pol : pols) {
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		//cout << "Vex1: " << fmt::format("{} {} {}", first.x, first.y, first.z) << endl;
		//cout << "Vex2: " << fmt::format("{} {} {}", second.x, second.y, second.z) << endl;
		//cout << "Vex3: " << fmt::format("{} {} {}", third.x, third.y, third.z) << endl;

		//glColor3ub(0, 0, 255);
		glColor3f(0.f, 0.f, 1.f);
		glBegin(GL_LINE_LOOP);
		glVertex3d(first.x, first.y, first.z);
		glVertex3d(second.x, second.y, second.z);
		glVertex3d(third.x, third.y, third.z);
		glEnd();
	}
	glPopMatrix();
}

void myglObj()
{
	myGL::myglPushMatrix();
	fmat4 beforeObjRotation = myGL::getModelMatrix();
	// Object only rotation - need to make sure NOT to apply this to light sources
	myGL::myglRotatef(rotate_x_obj, 1.0, 0.0, 0.0);
	myGL::myglRotatef(rotate_y_obj, 0.0, 1.0, 0.0);
	transformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> vertices = customObj->getVertices();
	vector<dvec4> planes = customObj->getPlanes();

	switch(lightOps->currLighting) {
	case Lighting::Constant:
		customObj->formTransfPolNormals_And_PolCentres();
		lightOps->constShading(customObj->lightCoeffs, beforeObjRotation,
			customObj->polNormalsNormalisedTransf, customObj->polCentresTransf);
		break;
	case Lighting::Gouraud:
		customObj->formTransfVexNormals_And_PolCentres();
		lightOps->gouraudShading(customObj->lightCoeffs, beforeObjRotation,
			customObj->vexNormalsNormalisedTransf, customObj->polCentresTransf);
		break;
	case Lighting::Phong:
		break;
	}
	
	myGL::myglPopMatrix();
}

void specialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		//move_x += 0.1;
		if (rotatingGlobal) {
			rotate_y_global += 5;
		}
		else {
			rotate_y_obj -= 5;
		}
	}
	else if (key == GLUT_KEY_LEFT) {
		//move_x -= 0.1;
		if (rotatingGlobal) {
			rotate_y_global -= 5;
		}
		else {
			rotate_y_obj += 5;
		}
	}
	else if (key == GLUT_KEY_UP) {
		//center = center + myGL::zAxis;
		//eye = eye + myGL::zAxis;
		//move_z -= 0.1;
		
		if (rotatingGlobal) {
			rotate_x_global -= 5;
		}
		else {
			rotate_x_obj += 5;
		}
	}
	else if (key == GLUT_KEY_DOWN) {
		//center = center - myGL::zAxis;
		//eye = eye - myGL::zAxis;
		//move_z += 0.1;
		if (rotatingGlobal) {
			rotate_x_global += 5;
		}
		else {
			rotate_x_obj -= 5;
		}
	}

	glutPostRedisplay();
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void setColour(Colour drawingColour) {
	switch (drawingColour) {
	case Colour::Black:
		glColor3f(0, 0, 0);
		break;
	case Colour::White:
		glColor3f(1, 1, 1);
		break;
	case Colour::Red:
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case Colour::Green:
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case Colour::Blue:
		glColor3f(0, 0, 1);
		break;
	case Colour::Yellow:
		glColor3f(1, 1, 0);
		break;
	}
}

void renderScene()
{
	myGL::myglRotatef(rotate_x_global, 1.0, 0.0, 0.0);
	myGL::myglRotatef(rotate_y_global, 0.0, 1.0, 0.0);

	if (OGLdraw) {
		glRotatef(rotate_x_global, 1.0, 0.0, 0.0);
		glRotatef(rotate_y_global, 0.0, 1.0, 0.0);
	}

	//cout << "CustomObj = " << customObj << endl;
	//cout << "customObj->isDataLoaded() = " << fmt::to_string(customObj->isDataLoaded()) << endl;

	if (customObj && customObj->isDataLoaded()) {
		glTestingPoints();
		setColour(drawingColour);
		myglObj();
	}
	else {
		exampleCube();
	}
	glLightSources();

	if (OGLdraw) {
		glObj();
	}
}

void glTestingPoints()
{
	// Now load the testing points
	glColor3f(1, 0, 0);
	vector<dvec4> testingPoints = customObj->getTestingPoints();
	
	//glLoadIdentity();
	for (dvec4 testingPoint : testingPoints) {
		//cout << "Testing point: " << fmt::format("{} {} {}\n", testingPoint[0], testingPoint[1], testingPoint[2]);
		glPushMatrix();
		glTranslatef(testingPoint[0], testingPoint[1], testingPoint[2]);
		//glLoadIdentity();
		glutSolidSphere(RADIUS, 48, 48);
		glPopMatrix();
	}	
}

void updateEye()
{
	// gluLookAt gonna update eye coords in myGL
	cout << "Enter eye coordinates x, y, z << ";
	//scanf_s("%f, %f, %f", &eye[0], &eye[1], &eye[2]);
	cin >> eye[0];
	cin >> eye[1];
	cin >> eye[2];
	cout << endl;
	glutPostRedisplay();
}

void updateCenter()
{
	// gluLookAt gonna update centre coords in myGL
	cout << "Enter center coordinates x, y, z << ";
	//scanf_s("%f, %f, %f", &center[0], &center[1], &center[2]);
	cin >> center[0];
	cin >> center[1];
	cin >> center[2];
	cout << endl;
	glutPostRedisplay();
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clear everything
		customObj = nullptr;
		cout << "Object reset -> enter a new object to render: << ";
		string toLoad;
		cin >> toLoad;
		loadObj(toLoad);
		glutPostRedisplay();
	}
	else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {
		// Clear the light sources and rotation
		rotate_x_global = rotate_y_global = rotate_x_obj = rotate_y_obj = 0;
		lightOps->lightSources.clear();
		cout << "Rotation and light sources reset!" << endl;
		glutPostRedisplay();
	}
}

void loadObj(string toLoad)
{
	cout << "Loading object " << toLoad << endl;
	customObj = make_unique<Model>(toLoad);

	// Okay got it now
	customObj->loadData();
	cout << customObj->dataStr() << endl;

	// Calculate the vertex normals
	customObj->formVexNormals();

	// Let's update the lighting model while here as well
	lightOps = make_unique<LightOps>(
		customObj->getVertices(),
		customObj->getVexNormalsNormalised(),
		customObj->getPolygons(),
		customObj->getPolCentres(),
		customObj->getPolygonNormalsNormalised(),
		customObj->getPlanes(),
		customObj->getProjectionPlanes(),
		customObj->getTestingPoints()
	);
}

void glLightSources()
{
	if (!lightOps) {
		return;
	}

	for (LightSource ls : lightOps->lightSources) {
		// Wanna display a sphere at each light source
		myGL::myExampleSphere(LIGHT_SOURCE_RADIUS, ls.pos, ls.colour);
		
	}
}

void hardcodeLightSources1()
{
	if (!lightOps) {
		return;
	}

	lightOps->lightSources.push_back(LightSource{
		{2, 2, 2},
		{255, 0, 0},
		SourceType::Ambient
		});
	lightOps->lightSources.push_back(LightSource{
		{-2, -2, -2},
		{255, 255, 0},
		SourceType::Diffuse
		});

}

void hardcodeLightSources2()
{
	if (!lightOps) {
		return;
	}

	lightOps->lightSources.push_back(LightSource{
		{2, 2, 2},
		{255, 255, 0},
		SourceType::Diffuse
		});
}

void hardcodeLightSources3()
{
	if (!lightOps) {
		return;
	}

	lightOps->lightSources.push_back(LightSource{
		{2, 2, 2},
		{255, 255, 0},
		SourceType::Ambient
		});
}

void keyP()
{
	if (!customObj || !customObj->isDataLoaded()) {
		cout << "Object data not loaded!" << endl;
		return;
	}

	string buffer;
	dvec4 pointToCheck;
	cout << "Please enter the point to be queried << ";

	cin >> buffer;
	pointToCheck[0] = stod(buffer);

	cin >> buffer;
	pointToCheck[1] = stod(buffer);

	cin >> buffer;
	pointToCheck[2] = stod(buffer);

	// Set the homo var
	pointToCheck[3] = 1;

	/*vector<string> splitArr(3);
	boost::split(splitArr, buffer, boost::is_any_of(" "));
	dvec4 pointToCheck{ stod(splitArr[0]), stod(splitArr[1]), stod(splitArr[2]), 1 };
	*/

	// Add it to testing points here, request re-drawing
	customObj->addTestingPoint(pointToCheck);
	glutPostRedisplay();

	pointToCheck = antiTransformVex(pointToCheck);
	int statusCode = queryPoints(pointToCheck);
	if (statusCode == -1) {
		cout << "The point is located inside the polygon!" << endl;
	}
	else if (statusCode == 0) {
		cout << "The point is located on the polygon mantle!" << endl;
	}
	else {
		cout << "The point is located outside the polygon!" << endl;
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case 'e':
		updateEye();
		break;
	case 'c':
		updateCenter();
		break;
	case 'p':
		keyP();
		break;
	case 'm':
	{
		cout << "Please enter the material lighting coefficients ka, kd, and kr << ";
		cin >> customObj->lightCoeffs.ka;
		cin >> customObj->lightCoeffs.kd;
		cin >> customObj->lightCoeffs.kr;
		break;
	}
	case 'l':
	{
		// Add light Source
		fvec3 pos;
		fvec3 colour;

		cout << "Please enter the light source 3D coordinates << ";
		cin >> pos.x;
		cin >> pos.y;
		cin >> pos.z;

		cout << "Please enter the light source colour intensities in the range [0-255] << ";
		cin >> colour.x;
		cin >> colour.y;
		cin >> colour.z;

		cout << "Please enter whether the light source is ambient or diffuse type << ";
		string lightType;
		cin >> lightType;

		std::transform(
			lightType.begin(),			// What to transform - begin iter
			lightType.end(),			// What to transform - end iter
			lightType.begin(),			// Where to save - begin iter
			[](unsigned char c) {return std::tolower(c); });		// Lambda that does the transf
		auto it = sourceTypeTbl.find(lightType);
		if (it == sourceTypeTbl.end()) {
			throw invalid_argument("Unknown light source type!");
		}

		lightOps->lightSources.push_back(LightSource{ pos, colour, it->second });
		glutPostRedisplay();
		break;
	}
	case '1':
		// Const shading
		lightOps->currLighting = Lighting::Constant;
		glutPostRedisplay();
		break;
	case '2':
		// Gouraud shading
		lightOps->currLighting = Lighting::Gouraud;
		glutPostRedisplay();
		break;
	case '3':
		// Phong shading
		break;
	case '8':
		hardcodeLightSources3();
		glutPostRedisplay();
		break;
	case '9':
		hardcodeLightSources2();
		glutPostRedisplay();
		break;
	case '0':
		hardcodeLightSources1();
		glutPostRedisplay();
		break;
	case 'f':
	{
		// Switch draw form!
		GLint currDrawForm = myGL::getCurrDrawForm();
		if (currDrawForm == GL_LINE_LOOP) {
			myGL::setCurrDrawForm(GL_TRIANGLES);
		}
		else if (currDrawForm == GL_TRIANGLES) {
			myGL::setCurrDrawForm(GL_LINE_LOOP);
		}
		glutPostRedisplay();
		break;
	}
	case 'w':
		eye.z -= 0.25;
		center.z -= 0.25;
		glutPostRedisplay();
		break;
	case 's':
		eye.z += 0.25;
		center.z += 0.25;
		glutPostRedisplay();
		break;
	case 'a':
		eye.x += 0.25;
		center.x += 0.25;
		glutPostRedisplay();
		break;
	case 'd':
		eye.x -= 0.25;
		center.x -= 0.25;
		glutPostRedisplay();
		break;
	case 'x':
	{
		cout << "Please input the new colour! Choose among: Black, White, Red, Green, Blue, and Yellow!" << endl
			<< "<< ";
		string colour;
		cin >> colour;
		
		std::transform(
			colour.begin(),			// What to transform - begin iter
			colour.end(),			// What to transform - end iter
			colour.begin(),			// Where to save - begin iter
			[](unsigned char c) {return std::tolower(c); });		// Lambda that does the transf
		auto it = colourTbl.find(colour);
		if (it == colourTbl.end()) {
			throw invalid_argument("Unknown colour!");
		}

		drawingColour = it->second;
		glutPostRedisplay();
		break;
	}
	case 'z':
		// Switch the global toggle
		rotatingGlobal ^= 1;
		break;
	case 'r':
	{
		string line;
		vector<string> parts;
		while (true) {
			getline(cin, line);
			if (line == "") {
				continue;
			}
			else if (line == "quit") {
				break;
			}

			boost::split(parts, line, boost::is_any_of(" "));
			dvec4 pointToCheck{ stod(parts[0]), stod(parts[1]), stod(parts[2]), 1 };
			customObj->addTestingPoint(pointToCheck);
			glutPostRedisplay();

			pointToCheck = antiTransformVex(pointToCheck);
			int statusCode = queryPoints(pointToCheck);
			if (statusCode == -1) {
				cout << "The point is located inside the polygon!" << endl;
			}
			else if (statusCode == 0) {
				cout << "The point is located on the polygon mantle!" << endl;
			}
			else {
				cout << "The point is located outside the polygon!" << endl;
			}
		break;
		}
	}
	case 'q':
		OGLdraw ^= 1;
		cout << "OGL drawing toggled: " << to_string(OGLdraw) << endl;
		myReshape(width, height);
		glutPostRedisplay();
		break;
	default:
		cout << "Specified key(" << theKey << ") is not supported! :(" << endl;
	}
}

inline void transformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// myGL::myglTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	myGL::myglScalef(scaleFactor, scaleFactor, scaleFactor);
	myGL::myglTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline void glTransformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// glTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	glScalef(scaleFactor, scaleFactor, scaleFactor);
	glTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline dvec4 antiTransformVex(dvec4 toTransform)
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	GLdouble scaleFactor = customObj->getScaleFactor();

	dmat4 translateMat = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{bodyCentre[0], bodyCentre[1], bodyCentre[2], 1},
	};

	dmat4 scaleMat = {
		{1. / scaleFactor, 0, 0, 0},
		{0, 1. / scaleFactor, 0, 0},
		{0, 0, 1. / scaleFactor, 0},
		{0, 0, 0, 1},
	};

	// First scale then translate
	// In code: translateMat * scaleMat * vex
	dvec4 resVex = translateMat * scaleMat * toTransform;
	return resVex;
}

int queryPoints(dvec4 toCheck)
{
	// Check if inside
	bool inside = true;
	bool onMantle = false;
	vector<dvec4> planes = customObj->getPlanes();
	for (dvec4 plane : planes) {
		GLdouble res = glm::dot(toCheck, plane);
		if (glm::abs(res) <= EPSILON) {
			onMantle = true;
			break;
		}
		else if (res > 0) {
			/// Means it's not inside
			inside = false;
			break;
		}
	}

	// Print res
	if (onMantle) {
		return 0;
	}
	else if (inside) {
		return -1;
	}
	else {
		return 1;
	}
}

void exampleCube()
{
	myGL::myglPushMatrix();
	//myGL::myglRotatef(rotate_x, 1.0, 0.0, 0.0);
	//myGL::myglRotatef(rotate_y, 0.0, 1.0, 0.0);

	// FRONT
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0); myGL::myglVertex3f(-0.5, -0.5, -0.5);
	glColor3f(0.0, 1.0, 0.0); myGL::myglVertex3f(-0.5, 0.5, -0.5);
	glColor3f(0.0, 0.0, 1.0); myGL::myglVertex3f(0.5, 0.5, -0.5);
	glColor3f(1.0, 0.0, 1.0); myGL::myglVertex3f(0.5, -0.5, -0.5);
	myGL::myglEnd();

	// BACK
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	myGL::myglVertex3f(0.5, -0.5, 0.5);
	myGL::myglVertex3f(0.5, 0.5, 0.5);
	myGL::myglVertex3f(-0.5, 0.5, 0.5);
	myGL::myglVertex3f(-0.5, -0.5, 0.5);
	myGL::myglEnd();

	// Purple side - RIGHT
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	myGL::myglVertex3f(0.5, -0.5, -0.5);
	myGL::myglVertex3f(0.5, 0.5, -0.5);
	myGL::myglVertex3f(0.5, 0.5, 0.5);
	myGL::myglVertex3f(0.5, -0.5, 0.5);
	myGL::myglEnd();

	// Green side - LEFT
	myGL::myglBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	myGL::myglVertex3f(-0.5, -0.5, 0.5);
	myGL::myglVertex3f(-0.5, 0.5, 0.5);
	myGL::myglVertex3f(-0.5, 0.5, -0.5);
	myGL::myglVertex3f(-0.5, -0.5, -0.5);
	myGL::myglEnd();

	// Blue side - TOP
	myGL::myglBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	myGL::myglVertex3f(0.5, 0.5, 0.5);
	myGL::myglVertex3f(0.5, 0.5, -0.5);
	myGL::myglVertex3f(-0.5, 0.5, -0.5);
	myGL::myglVertex3f(-0.5, 0.5, 0.5);
	myGL::myglEnd();

	// Red side - BOTTOM
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	myGL::myglVertex3f(0.5, -0.5, -0.5);
	myGL::myglVertex3f(0.5, -0.5, 0.5);
	myGL::myglVertex3f(-0.5, -0.5, 0.5);
	myGL::myglVertex3f(-0.5, -0.5, -0.5);
	myGL::myglEnd();
	myGL::myglPopMatrix();
}