#pragma once
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <GL/freeglut.h>
using namespace std; using namespace glm;

struct MaterialCoeffs {
	GLdouble ka, kd, kr;
	MaterialCoeffs(GLdouble ka, GLdouble kd) : MaterialCoeffs(ka, kd, 1) {};
	MaterialCoeffs(GLdouble ka, GLdouble kd, GLdouble kr) : ka{ ka }, kd{ kd }, kr{ kr } {};
};

class Model
{
private:
	const string path;
	string name;
	vector<dvec4> vertices;
	vector<dvec3> vexNormalsNormalised;
	vector<ivec3> polygons;
	vector<dvec3> polCentres;
	vector<dvec3> polNormalsNormalised;
	vector<dvec4> planes;
	vector<dvec4> projectionPlanes;
	vector<dvec4> testingPoints;
	
	GLdouble minX, maxX;
	GLdouble minY, maxY;
	GLdouble minZ, maxZ;

	dvec4 bodyCentre;
	dvec3 scaleFactorVex;
	GLdouble scaleFactor;

	bool dataLoaded = false;
public:
	MaterialCoeffs lightCoeffs{1, 1, 1};
	vector<dvec3> vexNormalsNormalisedTransf;
	vector<dvec3> polCentresTransf;
	vector<dvec3> polNormalsNormalisedTransf;

	inline static const GLdouble INTERVAL = 2;
	Model(string path);
	bool loadData();
	bool isDataLoaded();
	const string dataStr();

	void addTestingPoint(dvec4 toTest);
	void postTransformPlanes(dmat4 projectionMat);
	void formVexNormals();

	void formTransfVexNormals_And_PolCentres();
	void formTransfPolNormals_And_PolCentres();

	const dvec4& getBodyCentre();
	const double& getScaleFactor();
	const dvec3& getScaleFactors();
	vector<dvec4>& getVertices();
	vector<dvec3>& getVexNormalsNormalised();
	vector<ivec3>& getPolygons();
	vector<dvec3>& getPolygonNormalsNormalised();
	vector<dvec4>& getPlanes();
	const string& getPath();
	const string& getName();
	vector<dvec4>& getTestingPoints();
	vector<dvec4>& getProjectionPlanes();
	vector<dvec3>& getPolCentres();
};

