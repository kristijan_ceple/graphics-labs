#pragma once
#include <string>
#include <stack>
#include <glm/glm.hpp>
#include <GL/glut.h>
#include <vector>
#include <stdexcept>

using namespace std; using namespace glm;

class myGL
{
private:
	inline static GLint currState = GL_MODELVIEW;
	inline static fmat4 modelMatrix;
	inline static fmat4 viewMatrix;
	inline static fmat4 projectionMatrix;
	inline static fmat4 totalMatrix;
	inline static stack<fmat4> matricesStack;

	inline static GLint currDrawForm = GL_LINE_LOOP;

	inline static GLboolean ECSet = false;
	inline static dvec3 eye;
	inline static dvec3 center;

	myGL();			// Private constructor
public:
	inline static fvec3 zAxis, yAxis, xAxis, vup;
	inline static GLboolean cull = true;

	static void myglPushMatrix();
	static void myglPopMatrix();

	static void myglMatrixMode(int toSet);
	static void mygluLookAt(
		GLfloat eyeX, GLfloat eyeY, GLfloat eyeZ,
		GLfloat centerX, GLfloat centerY, GLfloat centerZ,
		GLfloat upX, GLfloat upY, GLfloat upZ
	);

	static void myglLoadIdentity();
	static void myglLoadIdentityExclusive();
	static void myglTranslatef(GLfloat dx, GLfloat dy, GLfloat dz);
	static void myglRotatef(GLfloat angle, GLfloat rx, GLfloat ry, GLfloat rz);
	static void myglScalef(GLfloat sx, GLfloat sy, GLfloat sz);

	static inline void myglBegin(int code);
	static inline void myglVertex3f(GLfloat x, GLfloat y, GLfloat z);
	static inline void myglColor3ub(GLubyte red, GLubyte green, GLubyte blue);
	static inline void myglEnd();
	static inline fmat4& getTotalMatrix();
	static inline fmat4& getModelMatrix();
	static inline fmat4 getModelMatrixCopy();
	static inline fmat4 getModelViewMatrix();
	static inline fmat4& getViewMatrix();
	static inline fmat4& getProjectionMatrix();
	static inline void setCurrDrawForm(const int toSet);
	static inline const GLint getCurrDrawForm();
	static void myglPerspective();

	static void myExampleSphere(GLdouble scaleFactor, dvec3& pos, ivec3& colour);
};

//////////////////////////////////////////////		INLINE METHODS IMPLEMENTATION		////////////////////////////////////

inline void myGL::myglBegin(int code)
{
	// Prepare the total matrix!
	// First modelview, then projection matrix!
	myGL::totalMatrix = myGL::projectionMatrix * myGL::viewMatrix * myGL::modelMatrix;;

	// myGL::vexes.clear();			// Clear the points!
	glBegin(code);					// Initiate OGL background drawing!
}

inline void myGL::myglEnd()
{
	glEnd();		// Delegator function
}

inline void myGL::myglVertex3f(GLfloat x, GLfloat y, GLfloat z)
{
	// Now draw the polygon! Multiply each point with the matrix before drawing!
	//fvec4 noProj = myGL::modelviewMatrix * fvec4{ x, y, z, 1 };
	fvec4 vexTmp = myGL::totalMatrix * fvec4{ x, y, z, 1 };
	fvec3 vexToDraw = fvec3{ vexTmp[0] / vexTmp[3], vexTmp[1] / vexTmp[3], vexTmp[2] / vexTmp[3] };
	glVertex3f(vexToDraw[0], vexToDraw[1], vexToDraw[2]);
}

inline void myGL::myglColor3ub(GLubyte red, GLubyte green, GLubyte blue)
{
	glColor3ub(red, green, blue);
}

inline fmat4& myGL::getTotalMatrix()
{
	return myGL::totalMatrix;
}

inline fmat4& myGL::getModelMatrix()
{
	return myGL::modelMatrix;
}

inline fmat4 myGL::getModelMatrixCopy()
{
	return myGL::modelMatrix;
}

inline fmat4& myGL::getViewMatrix()
{
	return myGL::viewMatrix;
}

inline fmat4 myGL::getModelViewMatrix()
{
	return myGL::viewMatrix * myGL::modelMatrix;
}

inline fmat4& myGL::getProjectionMatrix()
{
	return myGL::projectionMatrix;
}

inline void myGL::setCurrDrawForm(const int toSet)
{
	// First check if toSet is valid
	if (toSet != GL_LINE_LOOP && toSet != GL_TRIANGLES) {
		throw invalid_argument("Invalid argument passed: " + toSet);
	}

	myGL::currDrawForm = toSet;
}

inline const GLint myGL::getCurrDrawForm()
{
	return myGL::currDrawForm;
}
