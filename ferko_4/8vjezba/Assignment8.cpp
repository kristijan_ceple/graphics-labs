//*************************************************************************************************************
// Fractals Program!!!
//*************************************************************************************************************

#include <iostream>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <stdexcept>
#include <complex>
using namespace std; using namespace glm;

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

GLuint window; 
GLuint width = 300, height = 300;

//*********************************************************************************
//	Fractals global variables
//*********************************************************************************
enum class FractalType { Mandelbrot, Julia };

GLdouble epsilon = 100;
GLint  maxIter = 16;
GLdouble uMin = -1.5;
GLdouble uMax = 0.5;
GLdouble vMin = -1;
GLdouble vMax = 1;
FractalType currType = FractalType::Mandelbrot;

GLdouble uInterval = 2;
GLdouble vInterval = 2;

GLdouble uFracCoeff = 2. / width;
GLdouble vFracCoeff = 2. / height;

GLdouble depthCoeff = 255. / 16;
complex<GLdouble> c = {0.32, 0.043};

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void mySpecialKeys(int key, int x, int y);
void myObject();

void userInput();
void changeResolution();
void updateC();
void updateEpsilon();
void updateMaxIter();

void renderScene();
void fractalsMandelbrot	();
void fractalsJulia();

void hardcodeMandelbrot();
void hardcodeJulia();
void sitrep();


//*********************************************************************************
//	Glavni program.
//*********************************************************************************

int main(int argc, char** argv)
{
	// postavljanje dvostrukog spremnika za prikaz (zbog titranja)
	glutInitDisplayMode (GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize (width, height); 
	glutInitWindowPosition (100, 100);
	glutInit(&argc, argv);
	
	cout << "Welcome to the fractals program! At start default values are set for the Mandelbrot set in the lab description!" << endl;
	cout << "Press m for mandelbrot, and j for julia sets!" << endl;
	cout << "Press 1 for hardcoded mandelbrot set(from lab assignment desc), and 2 for hardcoded julia(-||-) set!" << endl;
	cout << "Up/down gaming keys regulate maxIter, and left/right gkeys regulate epsilon!" << endl;
	cout << "Press e to update epsilon to a specified value!" << endl;
	cout << "Press c to update constant c(used in Julia set)!" << endl;
	cout << "Press d to update maxIter value(Depth)!" << endl;
	cout << "Press s for sitrep!" << endl;
	cout << "Right mouse button allows to enter all the data for a set(including u, v, ...)!" << endl;
	cout << "Middle mouse button allows to enter a new resolution!" << endl;

	window = glutCreateWindow ("Fractals");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glutSpecialFunc(mySpecialKeys);

	// userInput();

	glutMainLoop();
	return 0;
}

//*********************************************************************************
//	Osvjezavanje prikaza. 
//*********************************************************************************

void myDisplay(void)
{
	// printf("Pozvan myDisplay()\n");
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f);		         // boja pozadine - bijela
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	renderScene();
	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

//*********************************************************************************
//	Promjena velicine prozora.
//	Funkcija gluPerspective i gluLookAt se obavlja 
//		transformacija pogleda i projekcija
//*********************************************************************************

void myReshape (int w, int h)
{
	// printf("MR: width=%d, height=%d\n",w,h);
	width=w; height=h;
	glViewport (0, 0, width, height); 
	glMatrixMode (GL_PROJECTION);        // aktivirana matrica projekcije
	glLoadIdentity ();
	gluOrtho2D(0, width, 0, height);
	glMatrixMode (GL_MODELVIEW);         // aktivirana matrica modela
}

void renderScene()
{
	if (currType == FractalType::Mandelbrot) {
		fractalsMandelbrot();
	}
	else if (currType == FractalType::Julia) {
		fractalsJulia();
	}
}

void fractalsMandelbrot()
{
	glBegin(GL_POINTS);
	glPointSize(1.);
	for (GLuint x0 = 0; x0 < width; x0++) {
		for (GLuint y0 = 0; y0 < height; y0++) {
			GLdouble u0 = uFracCoeff * x0 + uMin;
			GLdouble v0 = vFracCoeff * y0 + vMin;

			complex<GLdouble> cLocal = {u0, v0};
			complex<GLdouble> z = 0;
			GLdouble r = 0;

			GLint k = -1;
			for (; k < maxIter && r < epsilon; k++) {
				z = z * z + cLocal;
				r = abs(z);
			}
			glColor3ub(0, 0, depthCoeff * k);
			glVertex2d(x0, y0);
		}
	}
	glEnd();
}

void fractalsJulia()
{
	glBegin(GL_POINTS);
	glPointSize(1.);
	for (GLint x0 = 0; x0 < width; x0++) {
		for (GLint y0 = 0; y0 < height; y0++) {
			GLdouble u0 = uFracCoeff * x0 + uMin;
			GLdouble v0 = vFracCoeff * y0 + vMin;

			complex<GLdouble> z = {u0, v0};
			GLdouble r = 0;

			GLint k = -1;
			for (; k < maxIter && r < epsilon; k++) {
				z = z * z + c;
				r = abs(z);
			}
			glColor3ub(0, depthCoeff * k, 0);
			glVertex2d(x0, y0);
		}
	}
	glEnd();
}

inline void hardcodeMandelbrot()
{
	epsilon = 100;
	maxIter = 16;
	uMin = -1.5;
	uMax = 0.5;
	vMin = -1;
	vMax = 1;
	currType = FractalType::Mandelbrot;

	uInterval = 2;
	vInterval = 2;

	uFracCoeff = 2. / width;
	vFracCoeff = 2. / height;

	depthCoeff = 16;
}

inline void hardcodeJulia()
{
	epsilon = 100;
	maxIter = 16;
	uMin = -1;
	uMax = 1;
	vMin = -1.2;
	vMax = 1.2;
	currType = FractalType::Julia;

	uInterval = 2;
	vInterval = 2.4;

	uFracCoeff = 2. / width;
	vFracCoeff = 2.4 / height;

	depthCoeff = 16;
	c = {0.32, 0.043};
}

inline void sitrep()
{
	cout << "Window Width << " << width
		<< ", Window Height << " << height << endl;
	cout << "Current epsilon << " << epsilon << endl;
	cout << "Current maxIter << " << maxIter << endl;
}

//*********************************************************************************
//	Crta moj objekt. Ovdje treba naciniti prikaz ucitanog objekta.
//*********************************************************************************
void myObject ()
{
//	glutWireCube (1.0);
//	glutSolidCube (1.0);
//	glutWireTeapot (1.0);
//	glutSolidTeapot (1.0);
	
	glBegin (GL_TRIANGLES); // ili glBegin (GL_LINE_LOOP); za zicnu formu
		glColor3ub(255, 0, 0);	glVertex3f(-1.0, 0.0, 0.0);
		glColor3ub(0, 0, 0);	glVertex3f(0.0, 1.0, 0.0);
		glColor3ub(100, 0, 0);	glVertex3f(0.0, 0.0, 1.0);
	glEnd();
}

void userInput(void)
{
	cout << "Please enter Epsilon and max iterations num << ";

	cin >> maxIter;
	if (maxIter <= 0) {
		throw invalid_argument("maxIter must be greater than 0!");
	}

	cin >> epsilon;
	if (epsilon <= 0) {
		throw invalid_argument("epsilon must be great than 0!");
	}

	cout << "Please enter now uMin, uMax, vMin, vMax << ";
	cin >> uMin;
	cin >> uMax;

	// Check if uMin < uMax
	if (uMin >= uMax) {
		throw invalid_argument("uMin MUST be less than uMax!");
	}

	cin >> vMin;
	cin >> vMax;

	// Check if uMin < uMax
	if (vMin >= vMax) {
		throw invalid_argument("vMin MUST be less than vMax!");
	}

	if (currType == FractalType::Julia) {
		GLdouble real, imag;
		cout << "Please enter cReal and cImag << ";
		cin >> real;
		cin >> imag;
		c = { real, imag };
	}

	// Time to update the cached values
	uInterval = uMax - uMin;
	vInterval = vMax - vMin;

	uFracCoeff = uInterval / width;
	vFracCoeff = vInterval / height;

	depthCoeff = 255. / maxIter;
}

void changeResolution()
{
	cout << "Enter width and height!" << endl;
	cin >> width;
	cin >> height;

	// Update the correlated vars
	uFracCoeff = uInterval / width;
	vFracCoeff = vInterval / height;
}

inline void updateC()
{
	GLdouble real, imag;
	cout << "Please enter cReal and cImag << ";
	cin >> real;
	cin >> imag;
	c = { real, imag };
}

inline void updateEpsilon()
{
	cout << "Please enter Epsilon << ";
	cin >> epsilon;
	if (epsilon <= 0) {
		throw invalid_argument("Epsilon must be greater than 0!");
	}
}

inline void updateMaxIter()
{
	cout << "Please enter Max Iter << ";
	cin >> maxIter;
	if (maxIter <= 0) {
		throw invalid_argument("maxIter must be greater than 0!");
	}
}

//*********************************************************************************
//	Mis.
//*********************************************************************************

void myMouse(int button, int state, int x, int y)
{
	//	Desna tipka - brise canvas. 
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		userInput();
		glutPostRedisplay();
	}
	else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) {
		changeResolution();
		myReshape(width, height);
		glutPostRedisplay();
	}
}

//*********************************************************************************
//	Tastatura tipke - esc - izlazi iz programa.
//*********************************************************************************

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	switch (theKey) 
	{
	case 'e':
		updateEpsilon();
		glutPostRedisplay();
		break;
	case 'c':
		updateC();
		glutPostRedisplay();
		break;
	case 'd':
		updateMaxIter();
		glutPostRedisplay();
		break;
	case 's':
		sitrep();
		break;
	case 'm':
		currType = FractalType::Mandelbrot;
		cout << "Drawing Mandelbrot!" << endl;
		glutPostRedisplay();
		break;
	case 'j':
		currType = FractalType::Julia;
		cout << "Drawing Julia!" << endl;
		glutPostRedisplay();
		break;
	case '1':
		cout << "Hardcoding Mandelbrot data(from lab assignment desc)!" << endl;
		hardcodeMandelbrot();
		glutPostRedisplay();
		break;
	case '2':
		cout << "Hardcoding Julia data(from lab assignment desc)!" << endl;
		hardcodeJulia();
		glutPostRedisplay();
		break;
	}
}

void mySpecialKeys(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_LEFT:
		if (epsilon <= 1) {
			return;		// Don't decrease to 0
		}

		epsilon -= 1;
		cout << "Epsilon = " << epsilon << endl;
		glutPostRedisplay();
		break;
	case GLUT_KEY_RIGHT:
		epsilon += 1;
		cout << "Epsilon = " << epsilon << endl;
		glutPostRedisplay();
		break;
	case GLUT_KEY_UP:
		maxIter += 1;
		depthCoeff = 255. / maxIter;

		cout << "maxIter = " << maxIter << endl;
		glutPostRedisplay();
		break;
	case GLUT_KEY_DOWN:
		if (maxIter <= 1) {
			return;		// Don't decrease to 0
		}

		maxIter -= 1;
		depthCoeff = 255. / maxIter;
		cout << "maxIter = " << maxIter << endl;
		glutPostRedisplay();
		break;
	}
}