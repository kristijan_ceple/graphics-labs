#include <iostream>
#include <glm/glm.hpp>
using namespace std;
using namespace glm;

/*
const string WHITESPACE = " \n\r\f\v\t";

void ltrim(string& s)
{
	// Wanna trim the leading whitespace
	size_t index = s.find_first_not_of(WHITESPACE);
	const char* subString = s.substr(index, s.size()).c_str();
	s.assign(subString);
}

void rtrim(string& s)
{
	// Let's remove the trailing whitespace
	size_t index = s.find_last_not_of(WHITESPACE);
	const char* subString = s.substr(0, index).c_str();
	s.assign(subString);
}

void trim(string& s)
{
	ltrim(s);
	rtrim(s);
}

dvec3 parseString2Vector(string toParse)
{
	// First need to remove all the leading and trailing whitespace
	// The goal is to get a i, b j, c k

	double a, b, c;
	trim(toParse);
}
*/

int main(void)
{
	cout << "Welcome to the 1st lab exercise!" << endl;
	cout << "Demonstrating vector normalisation!" << endl
		<< "Please enter the desired vector in format: xi + yj + zk: "
		<< endl;

	double a1, b1, c1;
	while (true) {
		unsigned char errorCode = scanf_s("%lfi + %lfj + %lfk", &a1, &b1, &c1);

		if (errorCode == 3) {
			break;
		}
		else {
			cout << "Unallowed format!" << endl;
			char buffer[100];
			fgets(buffer, 100, stdin);		// This is used for the left-over space
		}
	}
	printf_s("You entered vector: %fi + %fj + %fk\n", a1, b1, c1);

	// Now make glm vector
	dvec3 vec1{ a1, b1, c1 };

	// Let's normalise it
	dvec3 resVec = normalize(vec1);

	cout << "The result of vector normalisation:" << endl;
	printf_s("%fi + %fj + %fk\n", resVec.x, resVec.y, resVec.z);

	return 0;
}

