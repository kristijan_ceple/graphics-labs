﻿#include <iostream>
#include <glm/glm.hpp>
using namespace std;
using namespace glm;

// Inline cause this is basically one larger macro??? Just experimenting right now actually
inline void printDMat3(dmat3 toPrint)
{
	printf("- %8s %8s %8s -\n", "", "", "");
	printf("| %f %f %f |\n", toPrint[0][0], toPrint[1][0], toPrint[2][0]);
	printf("| %f %f %f |\n", toPrint[0][1], toPrint[1][1], toPrint[2][1]);
	printf("| %f %f %f |\n", toPrint[0][2], toPrint[1][2], toPrint[2][2]);
	printf("- %8s %8s %8s -\n", "", "", "");
}

inline void printDMat4(dmat4 toPrint)
{
	printf("- %8s %8s %8s %8s -\n", "", "", "", "");
	printf("| %f %f %f %f |\n", toPrint[0][0], toPrint[1][0], toPrint[2][0], toPrint[3][0]);
	printf("| %f %f %f %f |\n", toPrint[0][1], toPrint[1][1], toPrint[2][1], toPrint[3][1]);
	printf("| %f %f %f %f |\n", toPrint[0][2], toPrint[1][2], toPrint[2][2], toPrint[3][2]);
	printf("| %f %f %f %f |\n", toPrint[0][3], toPrint[1][3], toPrint[2][3], toPrint[3][3]);
	printf("- %8s %8s %8s %8s -\n", "", "", "", "");
}

// This actually works LMAO!
template<typename T, int tcols, int trows> void printGenericMatrix(mat<tcols, trows, f64, defaultp> toPrint, int cols, int rows)
{
	// Just leaving here
}

int main(void)
{
	cout << "Welcome to the 1st lab exercise!" << endl;
	cout << "Demonstrating vector-matrix multiplication!" << endl
		<< "Please enter the vector-row first: "
		<< endl;
	
	dvec4 glmVec;
	for (int i = 0; i < 4; i++) {
		cin >> glmVec[i];
	}

	cout << "The row-vector:" << endl;
	printf_s("[ %f %f %f %f ]\n", glmVec[0], glmVec[1], glmVec[2], glmVec[3]);


	cout << "Please enter the matrix now:" << endl;
	double input[4][4];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cin >> input[i][j];
		}
	}

	// Have them all in the simplified matrix; Now need to form a matrix from this
	// Do it by column
	dmat4 glmMat;
	for (int j = 0; j < 4; j++) {
		glmMat[j] = { input[0][j], input[1][j], input[2][j], input[3][j] };
	}

	cout << "The matrix: " << endl;
	printDMat4(glmMat);

	// Let's multiply them
	dvec4 resVec = glmVec * glmMat;
	
	// Let's print it out
	cout << "The system solutions column-vector:" << endl;
	printf_s("[ %f %f %f %f ]", resVec[0], resVec[1], resVec[2], resVec[3]);

	return 0;
}