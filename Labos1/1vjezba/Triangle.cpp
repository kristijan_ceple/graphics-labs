﻿#include <iostream>
#include <glm/glm.hpp>
using namespace std;
using namespace glm;

const double EPSILON = 1E-2;

// Inline cause this is basically one larger macro??? Just experimenting right now actually
inline void printDMat3(dmat3 toPrint)
{
	printf("- %8s %8s %8s -\n", "", "", "");
	printf("| %f %f %f |\n", toPrint[0][0], toPrint[1][0], toPrint[2][0]);
	printf("| %f %f %f |\n", toPrint[0][1], toPrint[1][1], toPrint[2][1]);
	printf("| %f %f %f |\n", toPrint[0][2], toPrint[1][2], toPrint[2][2]);
	printf("- %8s %8s %8s -\n", "", "", "");
}

inline void printDMat4(dmat4 toPrint)
{
	printf("- %8s %8s %8s %8s -\n", "", "", "", "");
	printf("| %f %f %f %f |\n", toPrint[0][0], toPrint[1][0], toPrint[2][0], toPrint[3][0]);
	printf("| %f %f %f %f |\n", toPrint[0][1], toPrint[1][1], toPrint[2][1], toPrint[3][1]);
	printf("| %f %f %f %f |\n", toPrint[0][2], toPrint[1][2], toPrint[2][2], toPrint[3][2]);
	printf("| %f %f %f %f |\n", toPrint[0][3], toPrint[1][3], toPrint[2][3], toPrint[3][3]);
	printf("- %8s %8s %8s %8s -\n", "", "", "", "");
}

// This actually works LMAO!
template<typename T, int tcols, int trows> void printGenericMatrix(mat<tcols, trows, f64, defaultp> toPrint, int cols, int rows)
{
	// Just leaving here
}

int main(void)
{
	cout << "Welcome to the 1st lab exercise!" << endl;
	cout << "Demonstrating a three-variable equation solver -- Barycentric Coordinates Edition!" << endl
		<< "Please enter the triangle vertices in matrix form: "
		<< endl;

	double input[3][3];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cin >> input[i][j];
		}
	}

	// Have them all in the simplified matrix; Now need to form a matrix from this
	// Do it by column
	dmat3 triangleVertices;
	for (int j = 0; j < 3; j++) {
		triangleVertices[j] = { input[0][j], input[1][j], input[2][j] };
	}

	cout << "The triangle vertices in matrix form: " << endl;
	printDMat3(triangleVertices);

	// Now get the result vector!
	cout << "Enter the point of which the barycentric coordinates are requested for:" << endl;
	dvec3 pointCoords;
	for (int i = 0; i < 3; i++) {
		cin >> pointCoords[i];
	}

	// Got the triangle vertices in a matrix, and the point coordinates
	// Now just need to solve the old equation system
	// Baricentric coordinates = triangleVertices^(-1) * pointCoords

	cout << "The point coordinates:" << endl;
	printf_s("[ %f %f %f ]\n", pointCoords[0], pointCoords[1], pointCoords[2]);

	// Let's solve the equation
	// Solution column-vector = inverse(matrix) * solution-column-vector

	double determinant = glm::determinant(triangleVertices);
	dvec3 barycCoords;
	if (determinant == 0) {
		// Have to do something different 'ere
		// Find the row that is problematic and substitute it
		// Use condition t1 + t2 + t3 = 1
		// Fiirst find the problematic row
		
		for (int i = 0; i < 3; i++) {
			if (triangleVertices[0][i] == 0 && triangleVertices[1][i] == 0 && triangleVertices[2][i] == 0) {
				// Let's change the culprit
				triangleVertices[0][i] = 1;
				triangleVertices[1][i] = 1;
				triangleVertices[2][i] = 1;

				pointCoords[i] = 1;

				// Good, move on
			}
		}
	}
	
	barycCoords = inverse(triangleVertices) * pointCoords;

	// Let's print it out
	cout << "The desired point barycentric coordinates:" << endl;
	printf_s("[ %f %f %f ]\n", barycCoords[0], barycCoords[1], barycCoords[2]);

	double barycSum = barycCoords[0] + barycCoords[1] + barycCoords[2];
	cout << "Check the sum of the coordinates(needs to be equal to 1!):" << endl
		<< barycSum << endl;
	if (glm::abs(barycSum - 1) < EPSILON ) {
		cout << "The point should be in the triangle!" << endl;
	}
	else {
		cout << "The point should not be in the triangle!" << endl;
	}

	return 0;
}