#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <fmt\format.h>
#include <boost/algorithm/string.hpp>
#include "Model.h"
#include "BezierHolder.h"
using namespace std; using namespace glm;

 // #################################################################################################################
 // ##########################################		CONSTANTS		################################################
 // #################################################################################################################

GLuint window;
GLuint width = 640, height = 480;
GLdouble aspectRatio = width / height;
GLdouble move_z = 0, move_x = 0, rotate_x_global = 0, rotate_y_global = 0, rotate_x_obj = 0, rotate_y_obj = 0;
GLboolean rotatingGlobal = true;
GLint currDrawForm = GL_LINE_LOOP;
GLuint mouse_x, mouse_y;
unique_ptr<Model> customObj = nullptr;
const GLdouble EPSILON = 1E-6;
const GLdouble RADIUS = 0.025;
const GLdouble RADIUS_BEZIER = 0.075;
const GLdouble MOVE_CAMERA_INTERVAL = 1000. / 15.;			// Default - 1 second	
// const GLdouble MOVE_CAMERA_INTERVAL = 0.01;
unique_ptr<BezierHolder> bezierCurve = make_unique<BezierHolder>();
bool dummyCube = true;

enum class Colour { Black, White, Red, Green, Blue, Yellow };
static unordered_map<string, Colour> const colourTbl = {
	{"black", Colour::Black},
	{"white", Colour::White},
	{"red", Colour::Red},
	{"green", Colour::Green},
	{"blue", Colour::Blue},
	{"yellow", Colour::Yellow},
};
Colour drawingColour = Colour::White;

fvec3 eye{5, 5, 5};
fvec3 center{0, 0, 0};
fvec3 viewUp{ 0, 1, 0 };
fvec3 xAxis, yAxis, zAxis = normalize(eye - center);

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void setColour(Colour drawingColour);
void specialKeys(int key, int x, int y);
void exampleCube();
void glObj();
int queryPoints(dvec4 toCheck);
dvec4 antiTransformVex(dvec4);
void transformModel();
void keyP();
void glTestingPoints();
void bezierInput();
void glBezierControlPolygon();
void drawBezier();
void animateCamera();
void moveCamera(int);
void hardcodeBezier();
bool cull = true;
bool animating = false;
inline void updateTransfPlanes();
void updateAxes();
void updateEye();
void updateCenter();
dvec3 polygonCentre(dvec3 first, dvec3 second, dvec3 third);

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	// First load data
	string toLoad;
	cout << "Filename to load << ";
	cin >> toLoad;
	customObj = make_unique<Model>(toLoad);
	cout << "Loading object " << toLoad << endl;
	customObj->loadData();
	cout << customObj->dataStr() << endl;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("Bezier curves!");
	glutDisplayFunc(myDisplay);
	glutPassiveMotionFunc(mouseMoved);
	glutSpecialFunc(specialKeys);
	glutReshapeFunc(myReshape);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glEnable(GL_DEPTH_TEST);


	cout << "Object rendering program!" << endl;
	cout << "Right click clears the polygon and enables inputting another object to render!" << endl;
	cout << "Middle mouse button clears the rotation!" << endl;
	cout << "Press p to input a point that will be tested whether it is inside, outside, or on the mantles of the convex polygon!" << endl;
	cout << "Press e to update the eye coords, and c to update the center(look-at point) coords!" << endl;
	/////////////////////////////////////////////		5th Lab info		///////////////////////////////////////////////////
	cout << "Press f to change the current drawing form(wireframe vs. filled polygon)!" << endl;
	cout << "Press W, A, S, D to move around in the scene! By pressing the (gaming?) keys or numpad keys you can rotate the scene or the object!" << endl;
	cout << "Press z to switch between global rotation and object rotation(has effects what do the keys rotate)!" << endl;
	/////////////////////////////////////////////		5th Lab info		///////////////////////////////////////////////////

	cout << "Press x to change colour!" << endl;
	cout << "Irrelevant -- Press r for specific testing points check!" << endl;
	
	/////////////////////////////////////////////		6th Lab info		///////////////////////////////////////////////////
	cout << "Press 1 to input a point that will be used for Bezier curve definition!" << endl;
	cout << "Press 0 for a hardcoded bezier example!" << endl;
	cout << "Press 2 to draw the bezier curve!" << endl;
	cout << "Press 3 to animate the camera by following the bezier curve!" << endl;
	/////////////////////////////////////////////		6th Lab info		///////////////////////////////////////////////////

	cout << "Press o to render the dummy cube!" << endl;
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	//cout << "rotate_x >> " << rotate_x << endl;
	//cout << "rotate_y >> " << rotate_y << endl;
	// glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
	glLoadIdentity();
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		viewUp.x, viewUp.y, viewUp.z
	);
	updateTransfPlanes();
	//updateTransfPlanes();
	updateAxes();
	renderScene();			// First render the scene!
	glutSwapBuffers();		// Render complete -- swap buffers!
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	glMatrixMode(GL_PROJECTION);		// Projection matrix
	glLoadIdentity();					// Reset to identity matrix
	//glFrustum(-10.0, 10, -10.0, 10.0, 50, 150.0);
	gluPerspective(60, aspectRatio, 0.1, 100);
	//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	//gluOrtho2D(-2, 2, -2, 2);
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();					// -||-
	updateTransfPlanes();
	//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	//gluOrtho2D(-2, 2, -2, 2);
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();					// -||-
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		viewUp.x, viewUp.y, viewUp.z
	);
	//updateTransfPlanes();
	updateAxes();
}

inline void updateTransfPlanes() {
	// Calculate object post-transform planes
	if (customObj->isDataLoaded()) {
		double transMat[16];
		glGetDoublev(GL_MODELVIEW_MATRIX, transMat);
		dmat4 projectionMat = {
			{transMat[0], transMat[1], transMat[2], transMat[3]},
			{transMat[4], transMat[5], transMat[6], transMat[7]},
			{transMat[8], transMat[9], transMat[10], transMat[11]},
			{transMat[12], transMat[13], transMat[14], transMat[15]}
		};
		customObj->postTransformPlanes(projectionMat);
	}
}

void updateAxes()
{
	// Using method1 calc from the lab instructions
	zAxis = normalize(center - eye);
	viewUp = normalize(viewUp);

	xAxis = cross(zAxis, viewUp);
	yAxis = cross(xAxis, zAxis);
}

void glObj()
{
	glPushMatrix();
	glRotatef(rotate_x_obj, 1.0, 0.0, 0.0);
	glRotatef(rotate_y_obj, 0.0, 1.0, 0.0);
	transformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> transformPlanes = customObj->getProjectionPlanes();
	vector<dvec4> nonTransformPlanes = customObj->getPlanes();
	vector<dvec4> vertices = customObj->getVertices();

	for (int i = 0; i < pols.size(); i++) {
		ivec3 pol = pols[i];
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		if (cull) {
			// Get this polygon's plane and check the mul value!
			/*
			Something's wrong with my former culling. Need to implement
			post-transform culling.
			*/
			//dvec4 plane = transformPlanes[i];
			//GLdouble res = dot(plane, dvec4{ eye, 1 });
			//if (res <= 1) {
			//	// not visible
			//	continue;
			//}

			// Another try
			double modelViewArray[16];
			glGetDoublev(GL_MODELVIEW_MATRIX, modelViewArray);
			dmat4 modelViewMat = {
				{modelViewArray[0], modelViewArray[1], modelViewArray[2], modelViewArray[3]},
				{modelViewArray[4], modelViewArray[5], modelViewArray[6], modelViewArray[7]},
				{modelViewArray[8], modelViewArray[9], modelViewArray[10], modelViewArray[11]},
				{modelViewArray[12], modelViewArray[13], modelViewArray[14], modelViewArray[15]}
			};

			double projectionArray[16];
			glGetDoublev(GL_PROJECTION_MATRIX, projectionArray);
			dmat4 projectionMat = {
				{projectionArray[0], projectionArray[1], projectionArray[2], projectionArray[3]},
				{projectionArray[4], projectionArray[5], projectionArray[6], projectionArray[7]},
				{projectionArray[8], projectionArray[9], projectionArray[10], projectionArray[11]},
				{projectionArray[12], projectionArray[13], projectionArray[14], projectionArray[15]}
			};

			dmat4 totalMat = projectionMat * modelViewMat;
			dvec4 firstTransf = totalMat * first;
			dvec4 secondTransf = totalMat * second;
			dvec4 thirdTransf = totalMat * third;

			dvec3 firstTransFinal = dvec3{ firstTransf[0] / firstTransf[3], firstTransf[1] / firstTransf[3], firstTransf[2] / firstTransf[3] };
			dvec3 secondTransFinal = dvec3{ secondTransf[0] / secondTransf[3], secondTransf[1] / secondTransf[3], secondTransf[2] / secondTransf[3] };
			dvec3 thirdTransFinal = dvec3{ thirdTransf[0] / thirdTransf[3], thirdTransf[1] / thirdTransf[3], thirdTransf[2] / thirdTransf[3] };

			GLdouble x0 = firstTransFinal.x;
			GLdouble y0 = firstTransFinal.y;
			GLdouble z0 = firstTransFinal.z;

			GLdouble x1 = secondTransFinal.x;
			GLdouble y1 = secondTransFinal.y;
			GLdouble z1 = secondTransFinal.z;

			GLdouble x2 = thirdTransFinal.x;
			GLdouble y2 = thirdTransFinal.y;
			GLdouble z2 = thirdTransFinal.z;

			GLdouble area = (x0*y1 - x1*y0) + (x1*y2 - x2*y1) + (x2*y0 - x0*y2);

			if (area < 0) {
				continue;
			}

			// Yet another try
			//dvec4 plane = transformPlanes[i];
			//dvec3 planeNormal = { plane };
			//GLdouble res = dot(planeNormal, dvec3{ eye } - polygonCentre(first, second, third));
			//if (res <= 0) {
			//	// not visible
			//	continue;
			//}
		}

		glBegin(currDrawForm);
		glVertex3d(first.x, first.y, first.z);
		glVertex3d(second.x, second.y, second.z);
		glVertex3d(third.x, third.y, third.z);
		glEnd();
	}
	glPopMatrix();
}

dvec3 polygonCentre(dvec3 first, dvec3 second, dvec3 third) {
	return (first + second + third) / 3.;
}

void specialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		//move_x += 0.1;
		if (rotatingGlobal) {
			rotate_y_global += 5;
		}
		else {
			rotate_y_obj -= 5;
		}
	}
	else if (key == GLUT_KEY_LEFT) {
		//move_x -= 0.1;
		if (rotatingGlobal) {
			rotate_y_global -= 5;
		}
		else {
			rotate_y_obj += 5;
		}
	}
	else if (key == GLUT_KEY_UP) {
		//center = center + myGL::zAxis;
		//eye = eye + myGL::zAxis;
		//move_z -= 0.1;

		if (rotatingGlobal) {
			rotate_x_global -= 5;
		}
		else {
			rotate_x_obj += 5;
		}
	}
	else if (key == GLUT_KEY_DOWN) {
		//center = center - myGL::zAxis;
		//eye = eye - myGL::zAxis;
		//move_z += 0.1;
		if (rotatingGlobal) {
			rotate_x_global += 5;
		}
		else {
			rotate_x_obj -= 5;
		}
	}

	glutPostRedisplay();
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void setColour(Colour drawingColour) {
	switch (drawingColour) {
	case Colour::Black:
		glColor3f(0, 0, 0);
		break;
	case Colour::White:
		glColor3f(1, 1, 1);
		break;
	case Colour::Red:
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case Colour::Green:
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case Colour::Blue:
		glColor3f(0, 0, 1);
		break;
	case Colour::Yellow:
		glColor3f(1, 1, 0);
		break;
	}
}

void renderScene()
{
	glRotatef(rotate_x_global, 1.0, 0.0, 0.0);
	glRotatef(rotate_y_global, 0.0, 1.0, 0.0);

	//cout << "CustomObj = " << customObj << endl;
	//cout << "customObj->isDataLoaded() = " << fmt::to_string(customObj->isDataLoaded()) << endl;

	//glPushMatrix();
	//glMatrixMode(GL_MODELVIEW);			// Model view matrix
	//glLoadIdentity();					// -||-
	//gluLookAt(
	//	move_x, 0, 2.5 + move_z,
	//	0, 0, 0,
	//	0, 1, 0
	//);
	//glPopMatrix();

	if (customObj && customObj->isDataLoaded()) {
		glTestingPoints();
		setColour(drawingColour);
		glObj();
	}
	else {
		if (dummyCube) {
			exampleCube();
		}
		// GLUquadricObj* quadro = gluNewQuadric();
		// gluSphere(quadro, 1, 48, 48);
	}

	glBezierControlPolygon();
	drawBezier();
}

void glTestingPoints()
{
	// Now load the testing poitns
	glColor3f(1, 0, 0);
	vector<dvec4> testingPoints = customObj->getTestingPoints();
	
	//glLoadIdentity();
	for (dvec4 testingPoint : testingPoints) {
		//cout << "Testing point: " << fmt::format("{} {} {}\n", testingPoint[0], testingPoint[1], testingPoint[2]);
		glPushMatrix();
		glTranslatef(testingPoint[0], testingPoint[1], testingPoint[2]);
		//glLoadIdentity();
		glutSolidSphere(RADIUS, 48, 48);
		glPopMatrix();
	}	
}

void bezierInput()
{
	cout << "Please input a bezier control polygon point!" << endl;
	cout << "Format: x y z" << endl;
	fvec3 point;
	
	int toCheck = scanf_s("%f %f %f", &point[0], &point[1], &point[2]);
	if (toCheck != 3) {
		throw invalid_argument("Wrong format!");
	}

	bezierCurve->addControlPoint(point);
}

void glBezierControlPolygon()
{
	glColor3f(0, 0, 1);
	for (fvec3 currPoint : bezierCurve->getControlPolygon()) {
		glPushMatrix();
		// Don't forget to translate the sphere!
		glTranslatef(currPoint[0], currPoint[1], currPoint[2]);
		glutSolidSphere(RADIUS_BEZIER, 48, 48);
		glPopMatrix();
	}
}

void drawBezier()
{
	glBegin(GL_LINE_STRIP);
	for (fvec3 point : bezierCurve->getBezierCurve()) {
		glVertex3f(point[0], point[1], point[2]);
	}
	glEnd();
}

void animateCamera()
{
	// Calculate the interval
	/*
	We want to move the camera over n points, and we wanna do that in an interval
	of hmm gonna see about this later.
	*/
	// MOVE_CAMERA_INTERVAL = bezierCurve->getBezierCurve().size() / 
	if (animating) {
		return;
	}

	// Clear global rotations!
	rotate_x_global = rotate_y_global = 0;

	// Start moving the camera!
	animating = true;
	moveCamera(0);
}

void moveCamera(int index)
{
	// Update the camera, but first check if index is out of bounds
	vector<fvec3> bezierCurvePoints = bezierCurve->getBezierCurve();

	if (index >= bezierCurvePoints.size()) {
		// We've reached the end, that's all folks!
		animating = false;
		return;
	}

	// Else go on!
	eye = bezierCurve->getBezierCurve()[index];
	
	// Moves the camera
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		0, 1, 0
	);
	glutPostRedisplay();

	// Set up the timer here which will move the gluLookAt()
	glutTimerFunc(MOVE_CAMERA_INTERVAL, moveCamera, index + 1);
}

void hardcodeBezier2()
{
	bezierCurve->addControlPoint(fvec3{ 4, 0, 0 });
	bezierCurve->addControlPoint(fvec3{ 0, -5.1, 0 });
	bezierCurve->addControlPoint(fvec3{ -3.02, -0.01, 0 });
	bezierCurve->addControlPoint(fvec3{ -6.55, -5.29, 0 });
	//bezierCurve->addControlPoint(fvec3{ 4, 0, 0 });
}

void hardcodeBezier()
{
	bezierCurve->addControlPoint(fvec3{ 10, 10, 0 });
	bezierCurve->addControlPoint(fvec3{ -10, 10, 0 });
	bezierCurve->addControlPoint(fvec3{ -10, -10, 0 });
	bezierCurve->addControlPoint(fvec3{ 10, -10, 0 });
	bezierCurve->addControlPoint(fvec3{ 10, 10, 0 });
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP) {
		// Clear everything
		customObj.reset();
		cout << "Object reset -> enter a new object to render: << ";
		string toLoad;
		cin >> toLoad;
		cout << "Loading object " << toLoad << endl;
		customObj = make_unique<Model>(toLoad);

		// Okay got it now
		customObj->loadData();
		updateTransfPlanes();
		cout << customObj->dataStr();
		glutPostRedisplay();
	}
	else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_UP) {
		// Clear the bezier lines
		cout << "Bezier curve and rotation reset!" << endl;
		
		// Clear rotation
		rotate_x_obj = rotate_y_obj = rotate_x_global = rotate_y_global = 0;
		
		// Reset eye and center
		eye = { 0, 0, 5 };
		center = { 0, 0, 0 };
		viewUp = { 0, 1, 0 };

		bezierCurve.reset();
		bezierCurve = make_unique<BezierHolder>();
		glutPostRedisplay();
	}
	else if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		// Update center!
		//GLdouble delta = fvec2{ mouse_x - x, mouse_y - y };
		//center += delta;
	}
}

void keyP()
{
	if (!customObj || !customObj->isDataLoaded()) {
		cout << "Object data not loaded!" << endl;
		return;
	}

	string buffer;
	dvec4 pointToCheck;
	cout << "Please enter the point to be queried << ";

	cin >> buffer;
	pointToCheck[0] = stod(buffer);

	cin >> buffer;
	pointToCheck[1] = stod(buffer);

	cin >> buffer;
	pointToCheck[2] = stod(buffer);

	// Set the homo var
	pointToCheck[3] = 1;

	/*vector<string> splitArr(3);
	boost::split(splitArr, buffer, boost::is_any_of(" "));
	dvec4 pointToCheck{ stod(splitArr[0]), stod(splitArr[1]), stod(splitArr[2]), 1 };
	*/

	// Add it to testing points here, request re-drawing
	customObj->addTestingPoint(pointToCheck);
	glutPostRedisplay();

	pointToCheck = antiTransformVex(pointToCheck);
	int statusCode = queryPoints(pointToCheck);
	if (statusCode == -1) {
		cout << "The point is located inside the polygon!" << endl;
	}
	else if (statusCode == 0) {
		cout << "The point is located on the polygon mantle!" << endl;
	}
	else {
		cout << "The point is located outside the polygon!" << endl;
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case '0':
		hardcodeBezier();
		glutPostRedisplay();
		break;
	case 'p':
		keyP();
		break;
	case '1':
		// Bezier Input
		bezierInput();
		glutPostRedisplay();
		break;
	case '2':
		// Bezier Calc
		bezierCurve->calculateBezierCurve();
		glutPostRedisplay();
		cout << "Bezier curve calculated!" << endl;;
		break;
	case '3':
		// Bezier camera animation
		animateCamera();
		break;
	case 'e':
		updateEye();
		break;
	case 'c':
		updateCenter();
		break;
	case 'o':
		dummyCube ^= 0b1;
		glutPostRedisplay();
		break;
	
	case 'f':
	{
		// Switch draw form!
		if (currDrawForm == GL_LINE_LOOP) {
			currDrawForm = GL_TRIANGLES;
		}
		else if (currDrawForm == GL_TRIANGLES) {
			currDrawForm = GL_LINE_LOOP;
		}
		glutPostRedisplay();
		break;
	}
	case 'w':
		eye.z -= 0.25;
		center.z -= 0.25;
		glutPostRedisplay();
		break;
	case 's':
		eye.z += 0.25;
		center.z += 0.25;
		glutPostRedisplay();
		break;
	case 'a':
		eye.x += 0.25;
		center.x += 0.25;
		glutPostRedisplay();
		break;
	case 'd':
		eye.x -= 0.25;
		center.x -= 0.25;
		glutPostRedisplay();
		break;
	case 'x':
	{
		cout << "Please input the new colour! Choose among: Black, White, Red, Green, Blue, and Yellow!" << endl
			<< "<< ";
		string colour;
		cin >> colour;

		std::transform(
			colour.begin(),			// What to transform - begin iter
			colour.end(),			// What to transform - end iter
			colour.begin(),			// Where to save - begin iter
			[](unsigned char c) {return std::tolower(c); });		// Lambda that does the transf
		auto it = colourTbl.find(colour);
		if (it == colourTbl.end()) {
			throw invalid_argument("Unknown colour!");
		}

		drawingColour = it->second;
		glutPostRedisplay();
		break;
	}
	case 'z':
		// Switch the global toggle
		rotatingGlobal ^= 1;
		break;
	case 'r':
	{
		string line;
		vector<string> parts;
		while (true) {
			getline(cin, line);
			if (line == "") {
				continue;
			}
			else if (line == "quit") {
				break;
			}

			boost::split(parts, line, boost::is_any_of(" "));
			dvec4 pointToCheck{ stod(parts[0]), stod(parts[1]), stod(parts[2]), 1 };
			customObj->addTestingPoint(pointToCheck);
			glutPostRedisplay();

			pointToCheck = antiTransformVex(pointToCheck);
			int statusCode = queryPoints(pointToCheck);
			if (statusCode == -1) {
				cout << "The point is located inside the polygon!" << endl;
			}
			else if (statusCode == 0) {
				cout << "The point is located on the polygon mantle!" << endl;
			}
			else {
				cout << "The point is located outside the polygon!" << endl;
			}
			break;
		}
	}
	default:
		cout << "Specified key(" << theKey << ") is not supported! :(" << endl;
	}
}

void updateEye()
{
	cout << "Enter eye coordinates x, y, z << ";
	//scanf_s("%f, %f, %f", &eye[0], &eye[1], &eye[2]);
	cin >> eye[0];
	cin >> eye[1];
	cin >> eye[2];
	cout << endl;
	glutPostRedisplay();
}

void updateCenter()
{
	cout << "Enter center coordinates x, y, z << ";
	//scanf_s("%f, %f, %f", &center[0], &center[1], &center[2]);
	cin >> center[0];
	cin >> center[1];
	cin >> center[2];
	cout << endl;
	glutPostRedisplay();
}

inline void transformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// glTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	glScalef(scaleFactor, scaleFactor, scaleFactor);
	glTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline dvec4 antiTransformVex(dvec4 toTransform)
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	GLdouble scaleFactor = customObj->getScaleFactor();

	dmat4 translateMat = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{bodyCentre[0], bodyCentre[1], bodyCentre[2], 1},
	};

	dmat4 scaleMat = {
		{1. / scaleFactor, 0, 0, 0},
		{0, 1. / scaleFactor, 0, 0},
		{0, 0, 1. / scaleFactor, 0},
		{0, 0, 0, 1},
	};

	// First scale then translate
	// In code: translateMat * scaleMat * vex
	dvec4 resVex = translateMat * scaleMat * toTransform;
	return resVex;
}

int queryPoints(dvec4 toCheck)
{
	// Check if inside
	bool inside = true;
	bool onMantle = false;
	vector<dvec4> planes = customObj->getPlanes();
	for (dvec4 plane : planes) {
		GLdouble res = glm::dot(toCheck, plane);
		if (glm::abs(res) <= EPSILON) {
			onMantle = true;
			break;
		}
		else if (res > 0) {
			/// Means it's not inside
			inside = false;
			break;
		}
	}

	// Print res
	if (onMantle) {
		return 0;
	}
	else if (inside) {
		return -1;
	}
	else {
		return 1;
	}
}

void exampleCube()
{
	// FRONT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0); glVertex3f(-0.5, -0.5, -0.5);
	glColor3f(0.0, 1.0, 0.0); glVertex3f(-0.5, 0.5, -0.5);
	glColor3f(0.0, 0.0, 1.0); glVertex3f(0.5, 0.5, -0.5);
	glColor3f(1.0, 0.0, 1.0); glVertex3f(0.5, -0.5, -0.5);
	glEnd();

	// BACK
	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glEnd();

	// Purple side - RIGHT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glEnd();

	// Green side - LEFT
	glBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// Blue side - TOP
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glEnd();

	// Red side - BOTTOM
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();
}