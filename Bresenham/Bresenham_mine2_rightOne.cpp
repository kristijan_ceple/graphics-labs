//*********************************************************************************
//	Iscrtavanje linije 0-45, usporedba s glLine naredbom.
//	Pocetna i krajnja koordinata se zadaju lijevom tiplom misa, desnom se brise.
//	r, g, b, k s tastature mijenja boju.
//
//	Zadatak: Treba preraditi void myLine(GLint xa, GLint ya, GLint xb, GLint yb)
//	tako da radi ispravno za sve kutove
//
//  Potreban je glut - http://freeglut.sourceforge.net/
//  Za MSVC skinuti: 
//  http://files.transmissionzero.co.uk/software/development/GLUT/freeglut-MSVC.zip
//  postaviti - Properties - Configuration properties VC++Directories - Include Dir
//                                                                    - Library Dir
//*********************************************************************************

#include <stdio.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <iostream>
using namespace std;
using namespace glm;

//*********************************************************************************
//	Pocetna tocka Lx[1], Ly[1] Krajnja tocke Lx[2], Ly[2] linije.
//	Ix - stanje (zadana 1 ili 2 tocka)
//*********************************************************************************

GLdouble Lx[2], Ly[2];
GLint Ix;
const GLdouble EPSILON_COMPARISON_CONST = 1E-6;

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

GLuint window;
GLuint width = 300, height = 300;

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void myLine(GLint xa, GLint ya, GLint xb, GLint yb);


//*********************************************************************************
//	Glavni program.
//*********************************************************************************

int main(int argc, char** argv)
{
	std::cout << "Check!" << std::endl;

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Glut OpenGL Linija");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	printf("Lijevom tipkom misa zadaj tocke - algoritam Bresenham-a\n");
	printf("Tipke r, g, b, k mijenjaju boju.\n");

	glutMainLoop();
	return 0;
}


//*********************************************************************************
//	Osvjezavanje prikaza. (nakon preklapanja prozora) 
//*********************************************************************************

void myDisplay()
{
	//printf("Pozvan myDisplay()\n");
	//glClearColor(1.0f, 1.0f, 1.0f, 1.0f); //  boja pozadine
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //brisanje nakon svake iscrtane linije
	glFlush();
}

//*********************************************************************************
//	Promjena velicine prozora.
//*********************************************************************************

void myReshape(int w, int h)
{
	//printf("Pozvan myReshape()\n");
	width = w; height = h;               //promjena sirine i visine prozora
	Ix = 0;								//	indeks tocke 0-prva 1-druga tocka
	glViewport(0, 0, width, height);	//  otvor u prozoru

	glMatrixMode(GL_PROJECTION);		//	matrica projekcije
	glLoadIdentity();					//	jedinicna matrica
	gluOrtho2D(0, width, 0, height); 	//	okomita projekcija
	glMatrixMode(GL_MODELVIEW);			//	matrica pogleda
	glLoadIdentity();					//	jedinicna matrica

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f); // boja pozadine
	glClear(GL_COLOR_BUFFER_BIT);		//	brisanje pozadine
	glPointSize(1.0);					//	postavi velicinu tocke za liniju
	glColor3f(0.0f, 0.0f, 0.0f);		//	postavi boju linije
}

//*********************************************************************************
//	Crtaj moju liniju.
//*********************************************************************************

void myLine_try1(GLint xa, GLint ya, GLint xb, GLint yb)
{
	glBegin(GL_LINES);
	{
		glVertex2i(xa, ya + 5);			//	crtanje gotove linije
		glVertex2i(xb, yb + 5);
	}
	glEnd();

	GLint	x = xa;                        		//	Bresenhamov algoritam do 45  
	GLint	y = ya;

	GLint	dx = xb - xa;
	GLint	dy = yb - ya;

	// GLdouble	d = dy / (double)dx - 0.5;
	GLdouble	d = dy / (double)dx;

	glBegin(GL_POINTS);

	GLdouble dist1;
	GLdouble dist2;

	GLdouble aLine;
	GLdouble bLine;
	GLdouble cLine;

	GLdouble t_tmp;
	GLdouble yNextLine;

	for (int i = 0; i <= dx; i++)
	{
		glVertex2i(x, y);

		t_tmp = (x + 1 - xa) / dx;
		// use t_tmp to calc the y coordinate for the next x
		yNextLine = dy * t_tmp + xa;

		// Time to calculate the 2 distances, depending on where we're going!

		if (d > 0) {
			// Need to go up! Maybe???
			// First check the distance mayhaps?
			/*
			Thinking time. Need to calculate for i+1(or x+1 - whatever) at which y will the line
			intersect the grid. Then get the 2 distances and compare them. Depending on them decide which
			pixel to light up.

			I have got 2 points required to form a line. Maybe I can form a vector that would align with
			the line. Then form one j vector, find the intersecting coordinates?

			Because I have got 2 points, I can get the line equation

			First try:
			glm::dvec3 line = glm::cross(glm::dvec3{xa, ya, 1}, glm::dvec3{xb, ya, 1});

			// Now have got a, b, and c of our line
			aLine = line[0];
			bLine = line[1];
			cLine = line[2];

			// Let's get the a, b, and c of the forward vertical line

			// Time to form a vector
			*/

			// We're going up, let's calc those distances
			dist1 = glm::abs(yNextLine - y);
			dist2 = glm::abs(y + 1 - yNextLine);

			if (dist2 < dist1) {
				// Increase y
				y++;
			}
			else {
				// y stays the same, move on
			}

			// y++;
			// d = d - 1.0;
		}
		else {
			// Need to go down! Maybe???
			dist1 = glm::abs(yNextLine - y - 1);
			dist2 = glm::abs(y - yNextLine);

			if (dist1 < dist2) {
				// Going down
				y--;
			}
			else {
				// y stays the same
			}
		}

		x++;
	}
	glEnd();
}

void myLine_try2(GLint xa, GLint ya, GLint xb, GLint yb)
{
	glBegin(GL_LINES);
	{
		glVertex2i(xa, ya + 5);			//	crtanje gotove linije
		glVertex2i(xb, yb + 5);
	}
	glEnd();

	GLint	x = xa;                        		//	Bresenhamov algoritam do 45  
	GLint	y = ya;

	GLint	dx = xb - xa;
	GLint	dy = yb - ya;

	// Now first need to check -- is dx positive, or negative?
	bool dxPositive = dx >= 0;
	GLdouble absolut_dx = glm::abs(dx);
	GLdouble d;			// Practically the inclination/slope of the line
	
	/*
	if (dy > 0) {
		d = dy / (double)dx - 0.5;		// Why -0.5 oh sweet God - should probably try printing the values
	}
	else if(dy < 0) {
		d = dy / (double)dx + 0.5;
	}
	else if (dy == 0) {
		d = 0;		// No slope change lol
	}
	*/
	
	d = dy / (double)dx - 0.5;		// Why -0.5 oh sweet God - should probably try printing the values
	glBegin(GL_POINTS);
	for (int i = 0; i <= absolut_dx; i++)
	{
		glVertex2i(x, y);
		if (d > 0)
		{
			if(dxPositive){
				// Going up. Increase y.
				y++;
				d = d - 1.0;		// Why the hell would we fiddle with this???
				d += dy / (double)dx;	// Counteract the fiddling
			}
			
			/*
			else {
				// dx is negative. After drawing we see that we're actually going DOWN!
				// Have to fiddle a bit more here - something ain't right
				y--;
				d = d - 1.0;
				d += dy / (double)dx;	// Counteract the fiddling
			}
			*/
		}
		else if(d < 0) {
			/*
			if (dxPositive) {
				// Going down. Just take a look at the drawings and it's all clear
				y--;
				d = d + 1.0;		// Okay I guess lol
				d -= dy / (double)dx;	// Counteract the fiddling
			}
			
			else {
				// Going up. Increase y.
				y++;
				d = d + 1.0;		// Why the hell would we fiddle with this???
				d -= dy / (double)dx;	// Counteract the fiddling
			}
			*/
		}
		else if(glm::abs(d - 0) < EPSILON_COMPARISON_CONST) {
			// keep y the same? Lmao
		}

		// Increase - or decrease x? OFCOURSE DEPENDS IF WE'RE GOING LEFT OR RIGHT!!!
		x += dxPositive ? 1 : -1;
	}
	glEnd();
}

void myLine_backup(GLint xa, GLint ya, GLint xb, GLint yb)
{
	printf("Point A: x: %d, y: %d\n", xa, ya);
	printf("Point B: x: %d, y: %d\n", xb, yb);

	/*
	At some point(when drawing a vertical line) --> we may get an inf or -inf appear
	Need to fix that
	*/

	/*
	glBegin(GL_LINES);
	{
		glVertex2i(xa, ya + 5);			//	crtanje gotove linije
		glVertex2i(xb, yb + 5);
	}
	glEnd();
	*/

	GLint	x = xa;                        		//	Bresenhamov algoritam do 45  
	GLint	y = ya;

	GLint	dx = xb - xa;
	GLint	dy = yb - ya;

	/*
	GLdouble d = dy / (double)dx;
	cout << "d = " << d << endl;
	if (d > 0) {
		d -= 0.5;
		cout << "d -= 0.5 = " << d << endl;
	}
	else if (d < 0) {
		d += 0.5;
		cout << "d += 0.5 = " << d << endl;
	}
	*/

	GLdouble dInit = dy / (double)dx;
	GLdouble dxAbsolut = glm::abs(dx);
	GLboolean dxPositive = dx > 0;

	cout << "dx = " << dx << endl;
	cout << "dxPositive = " << (dxPositive ? "true" : "false") << endl;
	cout << "dInit = " << dInit << endl;

	char quadrant;
	if (dxPositive) {
		if (dInit > 0) {
			quadrant = 1;
		}
		else {
			quadrant = 4;
		}
	}
	else {
		if (dInit > 0) {
			quadrant = 3;
		}
		else {
			quadrant = 2;
		}
	}

	GLdouble d = dInit;
	if (!dxPositive) {
		d = -1 * d;		// Let's turn the sign around
	}

	glBegin(GL_POINTS);

	if (glm::isinf(dInit)) {
		// Just need to draw a vertical line and then return
		// Draw a line for the current x
		if (ya < yb) {
			// Climb up
			for (int j = ya; j <= yb; j++) {
				glVertex2i(xa, j);
			}
		}
		else {
			// Parkour down
			for (int j = yb; j <= ya; j++) {
				glVertex2i(xa, j);
			}
		}

		glEnd();
		return;
	}

	for (int i = 0; i <= dxAbsolut; i++)
	{
		glVertex2i(x, y);

		if (glm::abs(dInit - 0) < EPSILON_COMPARISON_CONST) {
			// do nothing lol - just touch x
			// Straight(just like me hehe ... ooorr??)
		} else if (dInit > 0) {
			// First case - we're going right
			// I. Quadrant
			if (dxPositive) {
				cout << "Entered dx positive if branch!!" << endl;
				// Have an if that checks whether d has gone overboard - in which case increase y
				if (d > 1) {
					int counter = 0;

					while (d > 1) {
						y++; counter++;
						if (counter >= 2) {
							if (y >= yb) {
								// no more
								for (int k = x + 1; k <= xb; k++) {
									glVertex2i(k, yb);
								}
								glEnd();
								return;
							}

							glVertex2i(x, y);
						}

						d -= 1.0;			// Hmm, have to do something here
						cout << "d -= 1.0" << d << endl;
					}
				}

				// Slowly increase the gradient -- until it goes overboard
				d += dy / (double)dx;
				cout << "d += dy/dx = " << d << endl;
			} else {
				// DX NEGATIVE -> DRAWING LEFT, D IS NEGATIVE
				// Since dInit is positive --> gradient is positive as well
				// III. Quadrant
				// Means we're drawing to the bottom-left
				cout << "Entered dx negative if branch!!" << endl;
				if (d < -1) {
					int counter = 0;

					while (d < -1) {
						y--; counter++;
						if (counter >= 2) {
							if (y <= yb) {
								for (int k = x - 1; k >= xb; k--) {
									glVertex2i(k, yb);
								}
								glEnd();
								return;
							}

							glVertex2i(x, y);
						}

						d += 1.0;			// Hmm, have to do something here
						cout << "d += 1.0 = " << d << endl;
					}
				}

				// Slowly decrease the gradient -- until it goes overboard
				d -= dy / (double)dx;
				cout << "d -= dy/dx = " << d << endl;
			}
		}
		else {
			// dInit < 0 --> negative gradient. 2nd and 4th Quadrants
			/*
			This is some left-over code. No more applies.

			else if(d < 0) {
				y--;
				d = d + 1.0;
				cout << "d + 1.0 = " << d << endl;
				d -= dy / (double)dx;
				cout << "d -= dy/dx = " << d << endl;
			}*/

			// First case - we're going right
			if (dxPositive) {
				cout << "Entered dx positive if branch!!" << endl;
				// We're going down-right
				// IV. Quadrant
				// Have an if that checks whether d has gone overboard - in which case increase y
				if (d < -1) {
					int counter = 0;
					while (d < -1) {
						y--; counter++;
						if (counter >= 2) {
							if (y <= yb) {
								for (int k = x + 1; k <= xb; k++) {
									glVertex2i(k, yb);
								}
								glEnd();
								return;
							}

							glVertex2i(x, y);
						}

						d += 1.0;			// Hmm, have to do something here
						cout << "d += 1.0" << d << endl;
					}
				}

				// Slowly decrease the gradient -- until it goes overboard
				// Notice -- the GRADIENT ITSELF IS NEGATIVE
				d += dy / (double)dx;
				cout << "d += dy/dx = " << d << endl;
			}
			else {
				// II. Quadrant
				// DX NEGATIVE -> DRAWING LEFT, D IS NEGATIVE
				// Since dInit is negative --> gradient is negative as well
				// Means we're drawing to the bottom-left
				cout << "Entered dx negative if branch!!" << endl;
				if (d < -1) {
					int counter = 0;
					while (d < -1) {
						y++; counter++;
						if (counter >= 2) {
							if (y >= yb) {
								for (int k = x - 1; k >= xb; k--) {
									glVertex2i(k, yb);
								}
								glEnd();
								return;
							}

							glVertex2i(x, y);
						}

						d += 1.0;			// Hmm, have to do something here
						cout << "d += 1.0 = " << d << endl;
					}
				}

				// Slowly decrease the gradient(remember -- THE GRADEINT ITSELF IS NEGATIVE)
				// Do this until it goes overboard
				d += dy / (double)dx;
				cout << "d -= dy/dx = " << d << endl;
			}
		}

		dxPositive ? x++ : x--;
	}

	cout << "End of drawing. y = " << y << " , yb = " << yb << endl;
	// Before end, let's check if yb was reached
	switch (quadrant) {
	case 1:
	case 2:
		for (int j = y; y <= yb; y++) {
			glVertex2i(xb, j);
		}
		break;
	case 3:
	case 4:
		for (int j = y; y >= yb; y--) {
			glVertex2i(xb, j);
		}
		break;
	default:
		// Shouldn't happen -- a mistake
		throw invalid_argument("Quadrant not 1, 2, 3, or 4!");
	}

	glEnd();
}

void myLine_try3(GLint xa, GLint ya, GLint xb, GLint yb)
{
	printf("Point A: x: %d, y: %d\n", xa, ya);
	printf("Point B: x: %d, y: %d\n", xb, yb);

	/*
	At some point(when drawing a vertical line) --> we may get an inf or -inf appear
	Need to fix that
	*/

	glBegin(GL_LINES);
	{
		glVertex2i(xa, ya + 5);			//	crtanje gotove linije
		glVertex2i(xb, yb + 5);
	}
	glEnd();

	GLint	x = xa;                        		//	Bresenhamov algoritam do 360 now 
	GLint	y = ya;

	GLint	dx = xb - xa;
	GLint	dy = yb - ya;

	GLdouble dInit = dy / (double)dx;
	GLdouble dxAbsolut = glm::abs(dx);
	GLboolean dxPositive = dx > 0;

	cout << "dx = " << dx << endl;
	cout << "dxPositive = " << (dxPositive ? "true" : "false") << endl;
	cout << "dInit = " << dInit << endl;

	char quadrant;
	if (dxPositive) {
		if (dInit > 0) {
			quadrant = 1;
		}
		else {
			quadrant = 4;
		}
	}
	else {
		if (dInit > 0) {
			quadrant = 3;
		}
		else {
			quadrant = 2;
		}
	}

	GLdouble d = dInit;
	if (!dxPositive) {
		d = -1 * d;		// Let's turn the sign around
	}

	glBegin(GL_POINTS);

	//*********************************************************************************
	//	Vertical line
	//*********************************************************************************
	if (glm::isinf(dInit)) {
		// Just need to draw a vertical line and then return
		// Draw a line for the current x
		if (ya < yb) {
			// Climb up
			for (int j = ya; j <= yb; j++) {
				glVertex2i(xa, j);
			}
		}
		else {
			// Parkour down
			for (int j = yb; j <= ya; j++) {
				glVertex2i(xa, j);
			}
		}

		glEnd();
		return;
	}

	//*********************************************************************************
	//	Horizontal line
	//*********************************************************************************
	if (glm::abs(dInit - 0) < EPSILON_COMPARISON_CONST) {
		for (int i = 0; i <= dxAbsolut; i++) {
			glVertex2i(x, y);

			dxPositive ? x++ : x--;
		}

		glEnd();
		return;
	}


	//*********************************************************************************
	//	All other lines
	//*********************************************************************************
	for (int i = 0; i <= dxAbsolut; i++)
	{
		glVertex2i(x, y);

		if (quadrant == 1) {
			cout << "Entered dx positive if branch!!" << endl;
			// Have an if that checks whether d has gone overboard - in which case increase y
			if (d > 1) {
				int counter = 0;

				while (d > 1) {
					y++; counter++;
					if (counter >= 2) {
						if (y >= yb) {
							// no more
							for (int k = x + 1; k <= xb; k++) {
								glVertex2i(k, yb);
							}
							glEnd();
							return;
						}

						glVertex2i(x, y);
					}

					d -= 1.0;			// Hmm, have to do something here
					cout << "d -= 1.0" << d << endl;
				}
			}

			// Slowly increase the gradient -- until it goes overboard
			d += dy / (double)dx;
			cout << "d += dy/dx = " << d << endl;
		}
		else if (quadrant == 2) {
			// II. Quadrant
			// DX NEGATIVE -> DRAWING LEFT, D IS NEGATIVE
			// Since dInit is negative --> gradient is negative as well
			// Means we're drawing to the bottom-left
			cout << "Entered dx negative if branch!!" << endl;
			if (d < -1) {
				int counter = 0;
				while (d < -1) {
					y++; counter++;
					if (counter >= 2) {
						if (y >= yb) {
							for (int k = x - 1; k >= xb; k--) {
								glVertex2i(k, yb);
							}
							glEnd();
							return;
						}

						glVertex2i(x, y);
					}

					d += 1.0;			// Hmm, have to do something here
					cout << "d += 1.0 = " << d << endl;
				}
			}

			// Slowly decrease the gradient(remember -- THE GRADEINT ITSELF IS NEGATIVE)
			// Do this until it goes overboard
			d += dy / (double)dx;
			cout << "d -= dy/dx = " << d << endl;
		}
		else if (quadrant == 3) {
			// DX NEGATIVE -> DRAWING LEFT, D IS NEGATIVE
			// Since dInit is positive --> gradient is positive as well
			// III. Quadrant
			// Means we're drawing to the bottom-left
			cout << "Entered dx negative if branch!!" << endl;
			if (d < -1) {
				int counter = 0;

				while (d < -1) {
					y--; counter++;
					if (counter >= 2) {
						if (y <= yb) {
							for (int k = x - 1; k >= xb; k--) {
								glVertex2i(k, yb);
							}
							glEnd();
							return;
						}

						glVertex2i(x, y);
					}

					d += 1.0;			// Hmm, have to do something here
					cout << "d += 1.0 = " << d << endl;
				}
			}

			// Slowly decrease the gradient -- until it goes overboard
			d -= dy / (double)dx;
			cout << "d -= dy/dx = " << d << endl;
		}
		else {
			// IV. QUADRANT
			cout << "Entered dx positive if branch!!" << endl;
			// We're going down-right
			// IV. Quadrant
			// Have an if that checks whether d has gone overboard - in which case increase y
			if (d < -1) {
				int counter = 0;
				while (d < -1) {
					y--; counter++;
					if (counter >= 2) {
						if (y <= yb) {
							for (int k = x + 1; k <= xb; k++) {
								glVertex2i(k, yb);
							}
							glEnd();
							return;
						}

						glVertex2i(x, y);
					}

					d += 1.0;			// Hmm, have to do something here
					cout << "d += 1.0" << d << endl;
				}
			}

			// Slowly decrease the gradient -- until it goes overboard
			// Notice -- the GRADIENT ITSELF IS NEGATIVE
			d += dy / (double)dx;
			cout << "d += dy/dx = " << d << endl;
		}
		
		dxPositive ? x++ : x--;
	}

	cout << "End of drawing. y = " << y << " , yb = " << yb << endl;
	// Before end, let's check if yb was reached
	switch (quadrant) {
	case 1:
	case 2:
		for (int j = y; y <= yb; y++) {
			glVertex2i(xb, j);
		}
		break;
	case 3:
	case 4:
		for (int j = y; y >= yb; y--) {
			glVertex2i(xb, j);
		}
		break;
	default:
		// Shouldn't happen -- a mistake
		throw invalid_argument("Quadrant not 1, 2, 3, or 4!");
	}

	glEnd();
}

void myLine(GLint xa, GLint ya, GLint xb, GLint yb)
{
	glColor3f(0, 0, 1); 
	printf("Point A: x: %d, y: %d\n", xa, ya);
	printf("Point B: x: %d, y: %d\n", xb, yb);

	// Let's add some AntiAliasing
	glLineWidth(1.5);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/*
	At some point(when drawing a vertical line) --> we may get an inf or -inf appear
	Need to fix that
	*/

	GLint	x = xa;   
	GLint	y = ya;

	GLint	dx = xb - xa;
	GLint	dy = yb - ya;

	GLbyte quadrant = NULL;
	if (dx > 0 && dy > 0) {
		quadrant = 1;
	}
	else if (dx < 0 && dy > 0) {
		quadrant = 2;
	}
	else if (dx < 0 && dy < 0) {
		quadrant = 3;
	}
	else if (dx > 0 && dy < 0) {
		quadrant = 4;
	}

	// Let's implement negative way!
	// Let's implement integers now
	// First see if above or under 45
	GLint dyAbsolut = glm::abs(dy);
	GLint dxAbsolut = glm::abs(dx);
	GLboolean flip = dyAbsolut > dxAbsolut;
	if(flip){
		// cout << "Entered flip!" << endl;

		GLint twoFragment = dxAbsolut<<1;
		GLint dyAbsolutTwoTimes = dyAbsolut << 1;
		// cout << "fragment = " << fragment << endl;
		GLint d = twoFragment - dyAbsolut;		// usually dxAbs/dyAbs - 0.5 --> now * 2 * dyAbs = 2dxAbs - dyAbs = twoFragment - dyAbs
		GLboolean dyPositive = dy > 0;

		glBegin(GL_POINTS);
		for (int j = 0; j <= dyAbsolut; j++) {
			glVertex2i(x, y);
			
			if (d > 0) {
				switch (quadrant) {
				case 1:
				case 4:
					x++;
					break;
				case 2:
				case 3:
					//cout << "Third quadrant!" << endl;
					x--;
					break;
				}

				d -= dyAbsolutTwoTimes;		// Usually -1 but now -1 * 2 * dyAbs = -2dyAbs
			}

			d += twoFragment;		// Usually += dx/dy but now * 2 * dy = 2dxAbs
			dyPositive? y++ : y--;
		}
		glEnd();
	}
	else {
		GLint twoFragment = dyAbsolut<<1;
		// cout << "fragment = " << fragment << endl;
		GLint d = twoFragment - dxAbsolut;
		GLint dxAbsolutTimesTwo = dxAbsolut<<1;
		GLboolean dxPositive = dx > 0;

		glBegin(GL_POINTS);
		for (int i = 0; i <= dxAbsolut; i++) {
			glVertex2i(x, y);

			if (d > 0) {
				switch (quadrant) {
				case 1:
					//cout << "First quadrant!" << endl;;
				case 2:
					/*if (quadrant == 2) {
						cout << "Second quadrant!" << endl;
					}*/

					y++;
					break;
				case 3:
					//cout << "Third quadrant!" << endl;
				case 4:
					/*if (quadrant == 4) {
						cout << "Fourth quadrant!" << endl;
					}*/

					y--;
					break;
				}

				d -= dxAbsolutTimesTwo;
			}

			d += twoFragment;
			dxPositive ? x++ : x--;
		}
		glEnd();
	}

	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
	{
		glVertex2i(xa, ya + 5);			//	crtanje gotove linije
		glVertex2i(xb, yb + 5);
	}
	glEnd();
}

//*********************************************************************************
//	Mis.
//*********************************************************************************

void myMouse(int button, int state, int x, int y)
{
	//	Lijeva tipka - crta pocetnu tocku ili liniju.
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)

	{
		//	Pamti krajnju tocke linije.
		Lx[Ix] = x;							//	upisi tocku
		Ly[Ix] = height - y;
		Ix = Ix ^ 1;						//	flip - druga tocka

											//	Crta prvu tocku ili liniju do druge tocke.
		if (Ix == 0) {
			myLine((int)Lx[0], (int)Ly[0], (int)Lx[1], (int)Ly[1]);
		}
		else {
			glBegin(GL_POINTS);
			glVertex2i(x, height - y);
			glEnd();
		}

		printf("Koordinate tocke %d: %d %d \n", Ix ^ 1, x, y);

		glFlush();
	}

	//	Desna tipka - brise canvas. 
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		myReshape(width, height);
	}
}


//*********************************************************************************
//	Tastatura tipke - r, g, b, k - mijenjaju boju.
//*********************************************************************************

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	switch (theKey)
	{
	case 'r':
		glColor3f(1, 0, 0);
		break;

	case 'g':
		glColor3f(0, 1, 0);
		break;

	case 'b':
		glColor3f(0, 0, 1);
		break;

	case 'k':
		glColor3f(0, 0, 0);

	}
	glRecti(width - 15, height - 15, width, height); // crta mali kvadrat u boji
	glFlush();
}