#include <stdio.h>
#include <GL/freeglut.h>

GLdouble Lx[2], Ly[2];
GLint Ix;

GLuint window;
GLuint width = 300, height = 300;

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void myLine(GLint xa, GLint ya, GLint xb, GLint yb);

int main(int argc, char** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("GLUT OpenGL line");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	printf("Left mouse click to input points - Bresenham algorithm!\n");
	printf("Keys r, g, b, and k change the colour!\n");

	glutMainLoop();
	return 0;
}

void myDisplay()
{
	glFlush();
}

void myReshape(int w, int h)
{
	width = w, height = h;
	Ix = 0;		// Point index; 0 = First point, 1 = Second point

	glViewport(0, 0, width, height);	// Open in window
	glMatrixMode(GL_PROJECTION);		// Projection matrix
	glLoadIdentity();					// Reset to identity matrix
	gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();					// -||-

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);		// Set background colour
	glClear(GL_COLOR_BUFFER_BIT);			// Clear the background
	glPointSize(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);			// Set color!(to black I guess?)
}

void myLine(GLint xa, GLint ya, GLint xb, GLint yb)
{
	glBegin(GL_LINES);
	{
		glVertex2i(xa, ya + 5);
		glVertex2i(xb, yb + 5);
	}
	glEnd();

	GLint x = xa;
	GLint y = ya;

	GLint dx = xb - xa;
	GLint dy = yb - ya;

	GLdouble d = dy / (double)dx - 0.5;

	glBegin(GL_POINTS);
	for (int i = 0; i <= dx; i++) {
		glVertex2i(x, y);

		if (d > 0) {
			y++;
			d = d - 1.0;
		}
		x++;
		d += dy / (double)dx;
	}
	glEnd();
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		Lx[Ix] = x;
		Ly[Ix] = height - y;	
		Ix = Ix ^ 1;		// Flip the dot

		if (Ix == 0) {
			// Draw the line!
			myLine((int)Lx[0], (int)Ly[0], (int)Lx[1], (int)Ly[1]);
		}
		else {
			// The first point has been passed
			// glVertex2i(x, height - y);
		}

		printf("Point coordinates %d: %d %d\n", Ix, x, y);
		glFlush();
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clear the canvas!
		myReshape(width, height);
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	// Let us set the colour!
	switch (theKey) {
	case 'r':
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case 'g':
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 'b':
		glColor3f(0, 0, 1);
		break;
	case 'k':
		glColor3f(0, 0, 0);
		break;
	}

	glRecti(width-15, height-15, width, height);
	glFlush();
}