#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>

void reshape(int width, int height);
void display();
void renderScene();
void animate(int value);
void drawSquare();
int angle = 0;

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(600, 300);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Animation Exmaple!");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(0.2, animate, 0);
	glutMainLoop();
}

void animate(int numba)
{
	// Let's redraw the scene using an increase(or eventually a reset) angle
	angle++;
	if (angle >= 360) {
		angle = 0;
	}
	glutPostRedisplay();

	// Now set ANOTHER timer!
	glutTimerFunc(0.2, animate, 0);		// 20 ms
}

void display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	renderScene();
	// We've rendered the scene into a buffer, so now swap the buffers
	// So that the buffer with the rendered scene is used for display, while we will
	// render new scenes into a new buffer
	glutSwapBuffers();
}

void reshape(int width, int height)
{
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width-1, 0, height-1, 0, 1);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_MODELVIEW);
}

void drawSquare()
{
	glBegin(GL_QUADS);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(100.0f, 0.0f);
	glVertex2f(100.0f, 100.0f);
	glVertex2f(0.0f, 100.0f);
	glEnd();
}

void renderScene()
{
	glPointSize(1);
	glColor3f(1.0f, 0.0f, 0.3f);

	// First square
	glPushMatrix();		// Save current instance of matrix(identity matrix) to stack -- performance optimisation
	glTranslatef(150.0f, 160.0f, 0.0f);	// Fourth and lastly -> Translate somewhere else
	glScalef(1.5f, 1.5f, 1.0f);		// Third -> scale 1.5 times -- enlarge!
	glRotatef((float)angle, 0.0f, 0.0f, 1.0f);		// Second -> rotate by angle around z axis
	glTranslatef(-50.0f, -50.0f, 0.0f);		// First center it
	drawSquare();
	glPopMatrix();		// Restore the original IdentMatr

	// Second square -- rotating in opposite direction!
	glPushMatrix();
	glTranslatef(400.0f, 160.0f, 0.0f);
	glTranslatef(400.0f, 160.0f, 0.0f);
	glScalef(1.5f, 1.5f, 1.0f);
	glRotatef(-(float)angle, 0.0f, 0.0f, 1.0f);
	glTranslatef(-50.0f, -50.0f, 0.0f);
	drawSquare();
	glPopMatrix();
}