#include "Matrix.h"
#include <iostream>
#include <regex>
#include <string>
#include <algorithm>
#define INITIALISE_ZEROS 1;
using namespace std;

Matrix::Matrix(size_t rows, size_t cols)
{
	// Initialise array of pointers
	this->_elements = new double*[rows];
	
	// Now initialise each array
	for (size_t i = 0; i < rows; i++) {
		this->_elements[i] = new double[cols];
#if INITIALISE_ZEROS == 1
		for (size_t j = 0; j < cols; j++) {
			this->_elements[i][j] = 0;		// Initialise to 0
		}
#endif
	}
}

Matrix::Matrix(size_t rows, size_t cols, double** arr, bool takeArray)
{
	this->_rows = rows;
	this->_cols = cols;

	if (takeArray) {
		this->_elements = arr;
	}
	else {
		// Have to allocate!
		this->_elements = new double* [rows];
		for (size_t i = 0; i < rows; i++) {
			this->_elements[i] = new double[cols];
		}
	}
}

size_t Matrix::getRowsCount()
{
	return this->_rows;
}

size_t Matrix::getColsCount()
{
	return this->_cols;
}

double Matrix::get(size_t row, size_t col)
{
	return this->_elements[row][col];
}

IMatrix* Matrix::set(size_t row, size_t col, double toSetVal)
{
	this->_elements[row][col] = toSetVal;
	return this;
}

IMatrix* Matrix::copy()
{
	// Let's make a copy
	Matrix* newMat = new Matrix{ this->_rows, this->_cols, this->_elements, false };
	return newMat;
}

IMatrix* Matrix::newInstance(size_t rows, size_t cols)
{

	Matrix* toReturn = new Matrix{rows, cols};

	// Only take the first rows and cols, fill others with zero if so defined!
	for (size_t i = 0; i < rows && i < this->_rows; i++) {
		for (size_t j = 0; j < cols && j < this->_cols; j++) {
			toReturn->_elements[i][j] = this->_elements[i][j];
		}
	}

#if INITIALISE_ZEROS
	for (size_t i = this->_rows; i < rows; i++) {
		for (size_t j = this->_cols; j < cols; j++) {
			toReturn->_elements[i][j] = 0;
		}
	}
#endif

	return toReturn;
}

IMatrix* Matrix::parseSimple(std::string toParse)
{
	// First split by |, then do the lower levels
	std::regex separator("|");
	std::sregex_token_iterator end;
	std::sregex_token_iterator token(toParse.begin(), toParse.end(), separator, -1);
	
	vector<vector<double>> elements;
	vector<string> rowsVect;

	while (token != end) {
		rowsVect.push_back(*token++);
	}

	// Now we have to rows -- need to split them by whitespace now
	std::regex seperator("\\s+");

	// Lemme get size of one row just for comparison
	size_t cols;
	string firstRow = rowsVect.at(0);
	firstRow.erase(remove(firstRow.begin(), firstRow.end(), " \t\n"));
	cols = firstRow.size();

	size_t rowsVectSize = rowsVect.size();
	for (size_t i = 0; i < rowsVectSize; i++) {
		vector<double> rowElems;

		string row = rowsVect.at(i);
		sregex_token_iterator token(row.begin(), row.end(), separator, -1);
		while (token != end) {
			string elemStr = *token++;
			
			// Convert to double now
			rowElems.push_back(stod(elemStr));
		}
		
		// Check the amount of elements
		if (cols != rowElems.size()) {
			throw length_error("Unequal row elements number!");
		}

		// Add this to the elems vector(of vectors)
		elements.push_back(rowElems);
	}
	
	size_t rows = elements.size();
	double** elementsArray = new double*[rowsVectSize];
	for (size_t i = 0; i < rows; i++) {
		// First allocate the cols array
		elementsArray[i] = new  double[cols];
		for (size_t j = 0; j < cols; j++) {
			elementsArray[i][j] = elements[i][j];
		}
	}

	return new Matrix{rows, cols, elementsArray, true};
}

Matrix::~Matrix()
{
	// Have an array of arrays(pointers). Have to free them all up
	size_t rows = this->getRowsCount();
	size_t cols = this->getColsCount();

	for (size_t i = 0; i < rows; i++) {
		// Free them!
		delete this->_elements[i];
	}

	// At last, delete elements themselves!
	delete this->_elements;
}

