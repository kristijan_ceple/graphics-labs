#include "AbstractVector.h"

IVector* AbstractVector::add(IVector& toAdd)
{
	// First check if their dimensions match
	if (this->getDimension() != toAdd.getDimension()) {
		throw std::invalid_argument("Vector addition dimension mismatch!");
	}

	for (int i = 0; i < this->getDimension(); i++) {
		this->set(i, this->get(i) + toAdd.get(i));
	}

	return this;
}

IVector* AbstractVector::nAdd(IVector& toAdd)
{
	return this->copy()->add(toAdd);
}

IVector* AbstractVector::sub(IVector& toSub)
{
	// First check if their dimensions match
	if (this->getDimension() != toSub.getDimension()) {
		throw std::invalid_argument("Vector addition dimension mismatch!");
	}

	for (int i = 0; i < this->getDimension(); i++) {
		this->set(i, this->get(i) - toSub.get(i));
	}

	return this;
}

IVector* AbstractVector::nSub(IVector& toSub)
{
	return this->copy()->sub(toSub);
}

IVector* AbstractVector::scalarMultiply(double scalar)
{
	for (int i = 0; i < this->getDimension(); i++) {
		this->set(i, this->get(i) * scalar);
	}

	return this;
}

IVector* AbstractVector::nScalarMultiply(double scalar)
{
	return this->copy()->scalarMultiply(scalar);
}

double AbstractVector::norm()
{
	// Need to calc the absolute value
	double quadratsSum = 0;
	double element;
	for (int i = 0; i < this->getDimension(); i++) {
		element = this->get(i);
		quadratsSum += element * element;
	}

	return sqrt(quadratsSum);
}

IVector* AbstractVector::normalize()
{
	// Just scalar inversely
	return this->scalarMultiply(1. / this->norm());
}

IVector* AbstractVector::nNormalize()
{
	return this->copy()->normalize();
}

double AbstractVector::cosine(IVector& otherVector)
{
	return this->norm() * otherVector.norm() / this->nVectorProduct(otherVector)->norm();
}

double AbstractVector::scalarProduct(IVector& otherVector)
{
	// First check if their dimensions match
	if (this->getDimension() != otherVector.getDimension()) {
		throw std::invalid_argument("Vector addition dimension mismatch!");
	}

	double sum = 0;
	for (int i = 0; i < this->getDimension(); i++) {
		sum += this->get(i) * otherVector.get(i);
	}

	return sum;
}

IVector* AbstractVector::nVectorProduct(IVector& otherVector)
{
	double x1 = this->get(0);
	double y1 = this->get(1);
	double z1 = this->get(2);

	double x2 = this->get(0);
	double y2 = this->get(1);
	double z2 = this->get(2);

	double a = y1 * z2 - y2 * z1;
	double b = x1 * z2 + x2 * z1;
	double c = x1 * y2 - x2 * y1;

	double elements[]{ a, b, c };
	Vector* toReturn = new Vector{ elements, 3 };
	return toReturn;
}

double* AbstractVector::toArray()
{
	size_t dimension = this->getDimension();
	double* toReturnArray = new double[dimension];
	for (int i = 0; i < dimension; i++) {
		*(toReturnArray + i) = this->get(i);
	}

	return toReturnArray;
}

IVector* AbstractVector::copyPart(size_t toCopy)
{
	// Make a new vector first
	double* elements = new double[toCopy] {};
	size_t dimension = this->getDimension();

	for (int i = 0; i < toCopy && i < dimension; i++) {
		elements[i] = this->get(i);
	}

	// This may be unneeded with new C++ features
	//// Have to set the remainder to 0
	//for (int i = dimension; i < toCopy; i++) {
	//	elements[i] = 0;
	//}

	IVector* toReturn = new Vector{ elements, dimension };
	return toReturn;

}

// virtual IMatrix* toRowMatrix(bool) = 0;
// virtual IMatrix* toColumnMatrix(bool) = 0;

std::string AbstractVector::toString(char precision)
{
	std::string toReturn;

	// Just print elements
	toReturn.append("[ ");

	char buffer[20 + 1];
	for (int i = 0; i < this->getDimension(); i++) {
		snprintf(buffer, 21, "%.*f ", precision, this->get(i));
		toReturn.append(buffer);
	}
	toReturn.append("]");

	return toReturn;
}

AbstractVector::~AbstractVector() {}