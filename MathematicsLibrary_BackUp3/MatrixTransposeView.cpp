#include "MatrixTransposeView.h"

MatrixTransposeView::MatrixTransposeView(IMatrix* toTranspose)
{
	this->_matrix = toTranspose;
}

size_t MatrixTransposeView::getRowsCount()
{
	return this->_matrix->getColsCount();
}

size_t MatrixTransposeView::getColsCount()
{
	return this->_matrix->getRowsCount();
}

double MatrixTransposeView::get(size_t row, size_t col)
{
	return this->_matrix->get(col, row);
}

IMatrix* MatrixTransposeView::set(size_t row, size_t col, double toSet)
{
	return this->_matrix->set(col, row, toSet);
}

IMatrix* MatrixTransposeView::copy()
{
	return new MatrixTransposeView{this->_matrix};
}

IMatrix* MatrixTransposeView::newInstance(size_t rows, size_t columns)
{
	// Selecting only the first specified rows and columns
	// Basically in the original matrix select only some columns and rows
	// And create a new view with this modified instance

	IMatrix* newMatrix = this->_matrix->newInstance(columns, rows);
	IMatrix* toReturn = new MatrixTransposeView{newMatrix};
	return toReturn;
}

double** MatrixTransposeView::toArray()
{
	// 2D array -> this is where the fun begins
	double** origArray = this->_matrix->toArray();

	size_t origRows = _matrix->getRowsCount();
	size_t origCols = _matrix->getColsCount();

	// Array of arrays
	double** newArray = new double* [origCols];
	for (int i = 0; i < origCols; i++) {
		newArray[i] = new double[origRows];
	}

	// Just iterate inversely
	for (int j = 0; j < origCols; j++) {
		for (int i = 0; i < origRows; i++) {
			newArray[j][i] = origArray[i][j];
		}
	}

	return newArray;
}