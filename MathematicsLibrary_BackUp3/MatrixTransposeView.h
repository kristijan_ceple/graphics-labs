#include "AbstractMatrix.h"

#pragma once
#ifndef MATRIXTRANSPOSEVIEW_H
#define MATRIXTRANSPOSEVIEW_H

class MatrixTransposeView : public AbstractMatrix
{
private:
	IMatrix* _matrix;

public:
	MatrixTransposeView(IMatrix* toTranspose);
	size_t getRowsCount();
	size_t getColsCount();
	double get(size_t, size_t);
	IMatrix* set(size_t, size_t, double);
	IMatrix* copy();
	IMatrix* newInstance(size_t, size_t);
	double** toArray();
};

#endif
