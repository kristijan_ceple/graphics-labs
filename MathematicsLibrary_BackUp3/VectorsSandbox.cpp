#include <iostream>
#include "AbstractVector.h"
#include "Vector.h"
#include "IVector.h"
#include "IMatrix.h"
#include "Matrix.h"
using namespace std;

void vectorsTest1()
{
	double values[]{3, 4, 5};
	Vector vect1{ values, 3 };
	cout << vect1.toString() << endl;

	Vector vect2{3, 5., 4., 3.};
	cout << vect2.toString() << endl;

	// Let's add them together!
	cout << vect1.add(vect2)->toString() << endl;
	cout << vect1.toString() << endl;

	cout << vect2.toString() << endl;
	cout << vect1.nSub(vect2)->toString() << endl;
}

void matricesTest2()
{
	IMatrix* mat1 = Matrix::parseSimple("1 2 4 | 5 6 7 | 9 13 15");
	IMatrix* mat2 = Matrix::parseSimple("9 8 6 | 5 4 3 | 1 -3 -5");

	mat1->add(*mat2);
	cout << mat1->toString() << endl;
}