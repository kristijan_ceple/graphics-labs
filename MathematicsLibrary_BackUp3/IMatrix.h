#pragma once
#ifndef IMATRIX_H
#define IMATRIX_H

#include "IVector.h"

class IMatrix
{
public:
	virtual size_t getRowsCount() = 0;
	virtual size_t getColsCount() = 0;
	virtual double get(size_t, size_t) = 0;
	virtual IMatrix* set(size_t, size_t, double) = 0;
	virtual IMatrix* copy() = 0;
	virtual IMatrix* newInstance(size_t, size_t) = 0;
	virtual IMatrix* nTranspose(bool) = 0;
	virtual IMatrix* add(IMatrix &other) = 0;
	virtual IMatrix* nAdd(IMatrix &other) = 0;
	virtual IMatrix* sub(IMatrix& other) = 0;
	virtual IMatrix* nSub(IMatrix& other) = 0;
	virtual IMatrix* nMultiply(IMatrix &other) = 0;
	virtual IMatrix* scalarMultiply(double, bool onLive = true) = 0;
	virtual double determinant() = 0;
	virtual IMatrix* subMatrix(size_t, size_t, bool liveView = true) = 0;
	virtual IMatrix* nInvert() = 0;
	virtual double** toArray() = 0;
	virtual IVector* toVector(bool) = 0;
	virtual std::string toString(size_t precision = 3) = 0;
	virtual ~IMatrix() = 0;
};

#endif
