#include <iostream>
#include <vector>
#include <algorithm>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <unordered_set>
#include<boost/functional/hash.hpp>
#include <glm\gtx\string_cast.hpp>
using namespace std; using namespace glm;

/*
This helper hash struct was adapted from user 'jogojapan' from website(https://stackoverflow.com/questions/17016175/c-unordered-map-using-a-custom-class-type-as-the-key)
Boost library was used as well
*/
struct dvec3Hasher
{
	std::size_t operator()(const dvec3& vec) const
	{
		using boost::hash_value;
		using boost::hash_combine;

		size_t seed = 0;
		hash_combine(seed, hash_value(vec[0]));
		hash_combine(seed, hash_value(vec[1]));
		hash_combine(seed, hash_value(vec[2]));

		return seed;
	}
};

const GLdouble COMPARISON_CONST = 1E-6;
auto leftPred = [](const pair<dvec3, dvec3>& firstLine, const pair<dvec3, dvec3>& secondLine)
{
	dvec3 firstPoint = firstLine.first;
	dvec3 secondPoint = secondLine.first;

	GLdouble first = firstPoint[1];
	GLdouble second = secondPoint[1];

	if (glm::abs(first - second) >= COMPARISON_CONST) {
		return first > second;
	}
	else {
		// Sort by x I guess
		return secondPoint[0] < firstPoint[0];
	}
};

auto rightPred = [](const pair<dvec3, dvec3>& firstLine, const pair<dvec3, dvec3>& secondLine)
{
	dvec3 firstPoint = firstLine.first;
	dvec3 secondPoint = secondLine.first;

	GLdouble first = firstPoint[1];
	GLdouble second = secondPoint[1];

	if (glm::abs(first - second) >= COMPARISON_CONST) {
		return first > second;
	}
	else {
		// Sort by x I guess
		return secondPoint[0] > firstPoint[0];
	}
};

/*
	This is a polygon drawing program.
	1. Click on the screen where should the points of our polygon be
	2. On last click, call my polygon filling function
	3. Need to implement the left/right
 */

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

GLuint window;
GLuint width = 300, height = 300;
GLuint mouse_x, mouse_y;

enum class Line { left, right };
vector<dvec3> vertices;		// A list of Points(int-int pairs)
vector<dvec3> testVertices; // A ist of Points that will be tested whether they are inside, outside, or on the polygon lines themselves
unordered_set<dvec3, dvec3Hasher> linesEqs;		// Contains mathematical line equations
const GLint DETECTION_RADIUS = 15;
const GLuint CLEAR_VERTICES_SECS = 2000;
GLboolean convexRender = false;
GLbyte drawingColour = 'k';
GLboolean firstIterCompleted = false;
const GLdouble EPSILON = 400;

void convexAlgo();
void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void clearAllVertices(int key);
void setColour(char drawingColour);
void pointInPolygon(int mouseX, int mouseY);

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Convex Objects");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutPassiveMotionFunc(mouseMoved);
	glutKeyboardFunc(myKeyboard);
	printf("Left mouse click to input points - Convex object filling algorithm!\n");
	printf("After the convex polygon has been drawn it is possible to input testing points(multiple times)!\n");
	printf("Keys r, g, b, and k change the colour!\n");
	glPointSize(5.0);
	glColor3f(0.0f, 0.0f, 0.0f);			// Set color!(to black I guess?)
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	renderScene();	// First render the scene!
	glutSwapBuffers();		// When/Where should I swap buffers? Prolly not here, buffers shoud be swapped AFTER rendering is completed
}

void setColour(char drawingColour) {
	switch (drawingColour) {
	case 'r':
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case 'g':
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 'b':
		glColor3f(0, 0, 1);
		break;
	case 'y':
		glColor3f(1, 1, 0);
		break;
	case 'k':
		glColor3f(0, 0, 0);
		break;
	}
}

void pointInPolygon(int mouseX, int mouseY)
{
	cout << "Testing whether point (" << mouseX << ", " << mouseY
		<< ") is located inside the convex polygon!" << endl;

	/*
	Get all the lines and test whether the result of dot product is < 0 for each and every one of them
	*/
	dvec3 toTest{mouseX, mouseY, 1};
	bool inside = true;
	bool onPol = false;
	GLdouble res;
	for (dvec3 lineEq : linesEqs) {
		res = dot(toTest, lineEq);
		if (res > 0) {
			inside = false;
			break;
		}
		else if (glm::abs(res) <= EPSILON) {
			onPol = true;
			break;
		}
	}

	if (onPol) {
		cout << "The point is on the polygon lines themselves!" << endl;
	}
	else if (inside) {
		cout << "The point is inside the polygon!" << endl;
	}
	else {
		cout << "The point is outside the polygon!" << endl;
	}
}

void renderScene()
{
	// First render the points and mouse pos text
	glColor3f(0, 0, 0);
	string mouseXStr = "X: " + to_string(mouse_x);
	string mouseYStr = "Y: " + to_string(height - mouse_y);

	//cout << "mouse_x: " << mouse_x << endl;
	//cout << "mouse_y: " << mouse_y << endl;
	//cout << "mouseXStr: " << mouseXStr << endl;
	//cout << "mouseYStr: " << mouseYStr << endl;
	
	const char *mouseXC, *mouseYC;
	//mouseXC = new unsigned char[mouseXStr.length() + 1];
	//mouseYC = new unsigned char[mouseYStr.length() + 1];
	// mouseXC = reinterpret_cast<const unsigned char*>(mouseXStr.c_str());
	// mouseYC = reinterpret_cast<const unsigned char*>(mouseYStr.c_str());
	//strcpy(mouseXC, static_cast<const unsigned char*>(mouseXStr.c_str()));

	mouseXC = mouseXStr.c_str();
	mouseYC = mouseYStr.c_str();

	// int xw = glutBitmapLength(GLUT_BITMAP_8_BY_13, mouseXC);
	// int yw = glutBitmapLength(GLUT_BITMAP_8_BY_13, mouseYC);
	glRasterPos2f(0, glutBitmapHeight(GLUT_BITMAP_8_BY_13));
	
	//Print Y Pos
	int len = strlen(mouseYC);
	for (int i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, mouseYC[i]);
	}

	// Print X Pos
	glRasterPos2f(0, 0);
	len = strlen(mouseXC);
	for (int i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, mouseXC[i]);
	}

	glBegin(GL_POINTS);
	for (dvec3 currVex : vertices) {
		glVertex2d(currVex[0], currVex[1]);
	}
	glEnd();

	setColour(drawingColour);
	glRecti(width - 15, height - 15, width, height);

	// Render the convex object if the time has come
	if (convexRender) {
		convexAlgo();
		setColour('k');
		glBegin(GL_POINTS);
		for (dvec3 currVex : testVertices) {
			glVertex2d(currVex[0], currVex[1]);
		}
		glEnd();
	}
}

void clearAllVertices(int key)
{
	vertices.clear();
	testVertices.clear();
	glutPostRedisplay();
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	glMatrixMode(GL_PROJECTION);		// Projection matrix
	glLoadIdentity();					// Reset to identity matrix
	gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();					// -||-
}

void convexAlgo()
{
	if (!firstIterCompleted) {
		cout << "The polygon consists of " << vertices.size() << " vertices." << endl;
	}

	// Let's form left and right lines
	vector<pair<dvec3, dvec3>> leftLines;
	vector<pair<dvec3, dvec3>> rightLines;
	// vector<pair<dvec3, dvec3>> horizontalLines;

	// First let us discover yMin and yMax
	GLdouble yMin;
	GLdouble yMax;
	GLbyte yMinIndex;
	GLbyte yMaxIndex;

	dvec3 firstVex = vertices[0];
	yMin = yMax = firstVex[1];
	yMinIndex = yMaxIndex = 0;

	GLdouble currY;
	for (int i = 0; i < vertices.size(); i++) {
		dvec3 currVex = vertices[i];
		currY = currVex[1];

		// Update yMin and yMax
		if (currY < yMin) {
			yMin = currY;
			yMinIndex = i;
			continue;
		}
		else if (currY > yMax) {
			yMax = currY;
			yMaxIndex = i;
		}
	}

	if (!firstIterCompleted) {
		cout << "yMin: " << yMin
			<< "\tyMax: " << yMax << endl;
	}

	// Clockwise!
	/*
	Okay, so now we must find the vertex which contains the yMin, and then go Clockwise
	*/
	size_t n = vertices.size();
	Line currState = Line::left;
	for (size_t i = yMinIndex, counter = 0; counter < n; counter++) {
		// Construct a line
		dvec3 currVex = vertices[i];
		i = (++i) % n;
		dvec3 nextVex = vertices[i];
		pair<dvec3, dvec3> line = { currVex, nextVex };

		if (currState == Line::left) {
			leftLines.push_back(line);
		}
		else {
			rightLines.push_back(line);
		}

		if (i == yMaxIndex) {
			currState = Line::right;
		}
	}

	// Have now got a list of left and rights. Let's first sort the 2 vectors
	// std::sort(leftLines.begin(), leftLines.end(), leftPred);
	// std::sort(rightLines.begin(), rightLines.end(), rightPred);
	std::reverse(leftLines.begin(), leftLines.end());

	// Okay - sorting done
	// Let's do the main algo part
	/*
	Sooo, we have got the leftLines and the rightLines 
	*/
	size_t albi = 0, adbi = 0;
	glBegin(GL_LINES);
	for (GLint y = yMax; y >= yMin; y--) {
		pair<dvec3, dvec3> rightLine = rightLines[adbi];
		pair<dvec3, dvec3> leftLine = leftLines[albi];

		// Define lines mathematically
		dvec3 leftLineEq = cross(leftLine.first, leftLine.second);
		dvec3 rightLineEq = cross(rightLine.first, rightLine.second);
		dvec3 yLine{0, 1, -y};

		linesEqs.insert(leftLineEq);
		linesEqs.insert(rightLineEq);

		// Find the intersections
		dvec3 leftPoint = cross(leftLineEq, yLine);
		dvec3 rightPoint = cross(rightLineEq, yLine);

		// Now draw the line between the x-es!
		glVertex2d(leftPoint[0]/leftPoint[2], y);
		glVertex2d(rightPoint[0]/rightPoint[2], y);

		// Increase adbi and albi
		while (y <= rightLine.second[1] && adbi < rightLines.size() - 1) {
			rightLine = rightLines[++adbi];
		}

		while (y <= leftLine.first[1] && albi < leftLines.size() - 1) {
			leftLine = leftLines[++albi];
		}
	}
	glEnd();
	glutPostRedisplay();
	if (!firstIterCompleted) {
		cout << "Drawing should have completed by now" << endl;
		cout << "Click anywhere to test whether the point is inside or outside the polygon!" << endl;
		firstIterCompleted = true;
	}
	
	//glutTimerFunc(CLEAR_VERTICES_SECS, clearVertices, 0);
}

void myMouse(int button, int state, int x, int y)
{
	// Okay, let's keep track of vertices here
	// Need to draw a point, and store the 
	// Need at least 3 points!
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clean Up - remove vertices, and remove all the lines
// #############################		CLEANUP		###########################
		vertices.clear();
		testVertices.clear();
		linesEqs.clear();
		convexRender = false;
		firstIterCompleted = false;
// #############################		CLEANUP		###########################
		glutPostRedisplay();
		cout << endl << endl;		// Freshen up the display a bit
		return;
	}
	else if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && convexRender) {
		// The user has specified a point! - first draw it on screen
		setColour('k');
		int trueY = height - y;
		testVertices.push_back(dvec3{x, trueY , 1});
		glutPostRedisplay();
		pointInPolygon(x, trueY);
		return;
	}
	else if (button != GLUT_LEFT_BUTTON || state != GLUT_UP || convexRender) {
		return;
	}

	int trueY = height - y;
	dvec3 toAdd{ x, trueY, 1 };

	size_t n = vertices.size();
	if (n == 0) {
		// Add a point without distance check and continue
		vertices.push_back(toAdd);
		glutPostRedisplay();
		cout << "Point coordinates: " << x << ", " << trueY << endl;
		return;
	}
	
	// First add, and then check distance
	double zerothDist = glm::distance({x, trueY, 1}, vertices[0]);
	if (zerothDist <= DETECTION_RADIUS) {
		if (n < 3) {
			cout << "Need at least 3 points in order to construct a convex geomteric shape!" << endl;
			return;
		}
		else {
			convexRender = true;
			return;		// Nothing new to render
		}
	}
	else {
		vertices.push_back(toAdd);
	}

	glutPostRedisplay();	// Need to re-render
	cout << "Point coordinates: " << x << ", " << trueY << endl;
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case 'r':
	case 'g':
	case 'b':
	case 'y':
	case 'k':
		drawingColour = theKey;
		glutPostRedisplay();
		break;
	default:
		cout << "Specified colour(" << theKey << ") is not supported! :(" << endl;
	}
}