﻿#include <iostream>
#include <glm/glm.hpp>
#include <io.h>
#include <fcntl.h>
using namespace std;
using namespace glm;

/*
const string WHITESPACE = " \n\r\f\v\t";

void ltrim(string& s)
{
	// Wanna trim the leading whitespace
	size_t index = s.find_first_not_of(WHITESPACE);
	const char* subString = s.substr(index, s.size()).c_str();
	s.assign(subString);
}

void rtrim(string& s)
{
	// Let's remove the trailing whitespace
	size_t index = s.find_last_not_of(WHITESPACE);
	const char* subString = s.substr(0, index).c_str();
	s.assign(subString);
}

void trim(string& s)
{
	ltrim(s);
	rtrim(s);
}

dvec3 parseString2Vector(string toParse)
{
	// First need to remove all the leading and trailing whitespace
	// The goal is to get a i, b j, c k

	double a, b, c;
	trim(toParse);
}
*/

// Inline cause this is basically one larger macro??? Just experimenting right now actually
inline void printDMat3(dmat3 toPrint)
{
	printf("- %8s %8s %8s -\n", "", "", "");
	printf("| %f %f %f |\n", toPrint[0][0], toPrint[1][0], toPrint[2][0]);
	printf("| %f %f %f |\n", toPrint[0][1], toPrint[1][1], toPrint[2][1]);
	printf("| %f %f %f |\n", toPrint[0][2], toPrint[1][2], toPrint[2][2]);
	printf("- %8s %8s %8s -\n", "", "", "");
}

// This actually works LMAO!
template<typename T, int tcols, int trows> void printGenericMatrix(mat<tcols, trows, f64, defaultp> toPrint, int cols, int rows)
{
	// Just leaving here
}

int main(void)
{
	cout << "Welcome to the 1st lab exercise!" << endl;
	cout << "Demonstrating 3x3 matrix multiplication with second matrix inversed!" << endl
		<< "Please enter matrix components one by one: "
		<< endl;
	
	// _setmode(_fileno(stdout), _O_U16TEXT);		// Necessary for the matrix unicode characters to print out in Win10

	// Time for 2nd matrix
	// First fill our matrix
	double mat1[3][3];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cin >> mat1[i][j];
		}
	}

	// Now let's make a glm matrix by columns!
	dmat3 glmMat1;
	for (int j = 0; j < 3; j++) {
		// Need to form a vector, and add it to the matrix
		glmMat1[j] = vec3{ mat1[0][j], mat1[1][j] , mat1[2][j] };
	}

	cout << "You have entered the matrix: " << endl;
	printDMat3(glmMat1);

	// Time for 2nd matrix
	cout << "Es ist zeit fur die Zweite matrix!" << endl;

	// First fill our 
	double mat2[3][3];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cin >> mat2[i][j];
		}
	}

	// Now let's make a glm matrix by columns!
	dmat3 glmMat2;
	for (int j = 0; j < 3; j++) {
		// Need to form a vector, and add it to the matrix
		glmMat2[j] = vec3{ mat2[0][j], mat2[1][j] , mat2[2][j] };
	}

	cout << "You have entered the matrix: " << endl;
	printDMat3(glmMat2);

	// Now do matrix addition
	// dmat3 resMat = dot(glmMat1, transpose(glmMat2));
	double secondMatDet = glm::determinant(glmMat2);
	if (secondMatDet == 0) {
		cout << "This won't end well -> the determinant is 0, and inverse cannot be found." << endl
			<< "Will calculate it anyway..." << endl;
	}

	dmat3 inversedMat = inverse(glmMat2);
	cout << "Inverse of the matrix last entered: " << endl;
	printDMat3(inversedMat);

	dmat3 resMat = glmMat1 * inversedMat;
	cout << "The resulting matrix:" << endl;
	printDMat3(resMat);

	return 0;
}