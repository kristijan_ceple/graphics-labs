#include "Model.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <GL/freeglut.h>
#include <fmt/format.h>
#include <fmt/core.h>
using namespace std;
using namespace boost;

Model::Model(string path) : path{ path } {}

bool Model::isDataLoaded()
{
	return this->dataLoaded;
}

vector<dvec4> Model::getVertices()
{
	return this->vertices;
}

vector<ivec3> Model::getPolygons()
{
	return this->polygons;
}

vector<dvec4> Model::getPlanes()
{
	return this->planes;
}


const string Model::getPath()
{
	return this->path;
}

const string Model::getName()
{
	return this->name;
}

bool Model::loadData()
{
	// Let's open the file first
	ifstream objectFile;
	objectFile.open(path);
	if (!objectFile.is_open()) {
		return false;		// Opening failed!
	}

	string line;
	vector<string> res(3);
	bool notFirstVex = false;
	while (getline(objectFile, line)) {
		if (line.empty()) {
			continue;
		}

		char firstChar = line.front();
		if (firstChar == 'v') {
			// Store the vertex!
			// Now split by " "
			boost::split(res, line, boost::is_any_of(" "));

			// Time to update min-max vals
			GLdouble x = stod(res[1]);
			GLdouble y = stod(res[2]);
			GLdouble z = stod(res[3]);

			if (!notFirstVex) {
				// Set up min and max vals
				minX = maxX = x;
				minY = maxY = y;
				minZ = maxZ = z;
				notFirstVex = true;
			}

			if (x < minX) {
				minX = x;
			}
			else if (x > maxX) {
				maxX = x;
			}

			if (y < minY) {
				minY = y;
			}
			else if (y > maxY) {
				maxY = y;
			}

			if (z < minZ) {
				minZ = z;
			}
			else if (z > maxZ) {
				maxZ = z;
			}

			vertices.push_back({ x, y, z, 1 });
		}
		else if (firstChar == 'f') {
			boost::split(res, line, boost::is_any_of(" "));
			GLint firstVexIndex = stoi(res[1]) - 1;
			GLint secondVexIndex = stod(res[2]) - 1;
			GLint thirdVexIndex = stod(res[3]) - 1;
			polygons.push_back({ firstVexIndex, secondVexIndex, thirdVexIndex });		// Normalise by decreasing by 1

			dvec3 firstVex = { vertices[firstVexIndex].x, vertices[firstVexIndex].y, vertices[firstVexIndex].z };
			dvec3 secondVex = { vertices[secondVexIndex].x, vertices[secondVexIndex].y, vertices[secondVexIndex].z };
			dvec3 thirdVex = { vertices[thirdVexIndex].x, vertices[thirdVexIndex].y, vertices[thirdVexIndex].z };

			// While we're already here -> let's form planes
			dvec3 planeNormal = glm::cross(secondVex - firstVex, thirdVex - firstVex);
			dvec4 plane = { planeNormal, -planeNormal[0] * firstVex.x - planeNormal[1] * firstVex.y - planeNormal[2] * firstVex.z };

			/*dvec3 altPlaneTmp = {
				(secondVex[1] - firstVex[1]) * (thirdVex[2] - firstVex[0]) - (secondVex[2] - firstVex[2]) * (thirdVex[1] - firstVex[1]),
				-(secondVex[0] - firstVex[0]) * (thirdVex[2] - firstVex[2]) + (secondVex[2] - firstVex[2]) * (thirdVex[0] - firstVex[0]),
				(secondVex[0] - firstVex[0]) * (thirdVex[1] - firstVex[1]) - (secondVex[1] - firstVex[1]) * (thirdVex[0] - firstVex[0])
			};
			dvec4 altPlane = {
				altPlaneTmp,
				-altPlaneTmp[0] * firstVex.x - altPlaneTmp[1] * firstVex.y - altPlaneTmp[2] * firstVex.z
			};*/
			planes.push_back(plane);
		}
		else if (firstChar == 'g') {
			this->name = line.substr(2);
		}
	}

	// Calculate body centre
	bodyCentre[0] = (maxX + minX) / 2;
	bodyCentre[1] = (maxY + minY) / 2;
	bodyCentre[2] = (maxZ + minZ) / 2;

	GLdouble width = glm::abs(maxX - minX);
	GLdouble height = glm::abs(maxY - minY);
	GLdouble depth = glm::abs(maxZ - minZ);

	// Calc scale factor
	scaleFactorVex[0] = INTERVAL / width;
	scaleFactorVex[1] = INTERVAL / height;
	scaleFactorVex[2] = INTERVAL / depth;

	// Calculate the greatest of width, height, and depth
	GLdouble max = width;
	if (max < height) {
		max = height;
	}
	if (max < depth) {
		max = depth;
	}
	scaleFactor = INTERVAL / max;

	objectFile.close();
	this->dataLoaded = true;
	return true;
}

const dvec4 Model::getBodyCentre()
{
	return this->bodyCentre;
}

const double Model::getScaleFactor()
{
	return this->scaleFactor;
}

const dvec3 Model::getScaleFactors()
{
	return this->scaleFactorVex;
}

const string Model::dataStr()
{
	fmt::memory_buffer buffer;
	if (!this->name.empty()) {
		format_to(buffer, "g {}\n", this->name);
	}

	for (dvec4 vex : vertices) {
		fmt::format_to(buffer, "v {} {} {}\n", vex[0], vex[1], vex[2]);
	}

	fmt::format_to(buffer, "\n");

	for (dvec3 pol : polygons) {
		fmt::format_to(buffer, "f {} {} {}\n", pol[0], pol[1], pol[2]);
	}

	for (dvec4 plane: planes) {
		fmt::format_to(buffer, "Plane A{}, B{}, C{}, D{}\n", plane[0], plane[1], plane[2], plane[3]);
	}

	fmt::format_to(buffer, "Max X: {}\n", maxX);
	fmt::format_to(buffer, "Min X: {}\n", minX);
	fmt::format_to(buffer, "Max Y: {}\n", maxY);
	fmt::format_to(buffer, "Min Y: {}\n", minY);
	fmt::format_to(buffer, "Max Z: {}\n", maxZ);
	fmt::format_to(buffer, "Min Z: {}\n", minZ);

	return fmt::to_string(buffer);
}

vector<dvec4> Model::getTestingPoints()
{
	return this->testingPoints;
}

vector<dvec4> Model::getProjectionPlanes()
{
	return this->projectionPlanes;
}

void Model::postTransformPlanes(dmat4 projectionMat)
{
	for (dvec3 pol : polygons) {
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		dvec3 firstTransf = projectionMat * first;
		dvec3 secondTransf = projectionMat * second;
		dvec3 thirdTransf = projectionMat * third;

		// form the plane now
		dvec3 planeNormal = glm::cross(secondTransf - firstTransf, thirdTransf - firstTransf);
		dvec4 plane = { planeNormal, -planeNormal[0] * firstTransf.x - planeNormal[1] * firstTransf.y - planeNormal[2] * firstTransf.z };
		projectionPlanes.push_back(plane);
	}
}

void Model::addTestingPoint(dvec4 toTest)
{
	this->testingPoints.push_back(toTest);
}