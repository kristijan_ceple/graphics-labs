#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <fmt\format.h>
#include <boost/algorithm/string.hpp>
#include "Model.h"
#include "BezierHolder.h"
using namespace std; using namespace glm;

 // #################################################################################################################
 // ##########################################		CONSTANTS		################################################
 // #################################################################################################################

GLuint window;
GLuint width = 640, height = 480;
GLdouble aspectRatio = width / height;
GLdouble move_z = 0, move_x = 0, rotate_x = 0, rotate_y = 0;
GLuint mouse_x, mouse_y;
GLbyte drawingColour = 'w';
unique_ptr<Model> customObj = nullptr;
const GLdouble EPSILON = 1E-6;
const GLdouble RADIUS = 0.025;
const GLdouble RADIUS_BEZIER = 0.075;
const GLdouble MOVE_CAMERA_INTERVAL = 1000. / 60.;			// Default - 1 second	
// const GLdouble MOVE_CAMERA_INTERVAL = 0.01;
unique_ptr<BezierHolder> bezierCurve = make_unique<BezierHolder>();
bool dummyCube = true;

fvec3 eye{5, 5, 5};
fvec3 center{0, 0, 0};

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void setColour(char drawingColour);
void specialKeys(int key, int x, int y);
void exampleCube();
void glObj();
int queryPoints(dvec4 toCheck);
dvec4 antiTransformVex(dvec4);
void transformModel();
void keyP();
void glTestingPoints();
void bezierInput();
void glBezierControlPolygon();
void drawBezier();
void animateCamera();
void moveCamera(int);
void hardcodeBezier();
bool cull = true;
bool animating = false;
inline void updateTransfPlanes();

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	// First load data
	string toLoad;
	cout << "Filename to load << ";
	cin >> toLoad;
	customObj = make_unique<Model>(toLoad);
	cout << "Loading object " << toLoad << endl;
	customObj->loadData();
	cout << customObj->dataStr() << endl;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("Bezier curves!");
	glutDisplayFunc(myDisplay);
	glutPassiveMotionFunc(mouseMoved);
	glutSpecialFunc(specialKeys);
	glutReshapeFunc(myReshape);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glEnable(GL_DEPTH_TEST);

	cout << "Object rendering program!" << endl;
	cout << "Right click clears the polygon and enables inputting another object to render!" << endl;
	cout << "Press p to input a point that will be tested whether it is inside, outside, or on the mantles of the convex polygon!" << endl;
	
	cout << "Press e to input a point that will be used for Bezier curve definition!" << endl;
	cout << "Press h for a hardcoded bezier example!" << endl;
	cout << "Press f to draw the bezier curve!" << endl;
	cout << "Press a to animate the camera by following the bezier curve!" << endl;
	
	cout << "Press r, g, b, y, k, or w to set color!" << endl;
	cout << "Press d to render the dummy cube!" << endl;
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	//cout << "rotate_x >> " << rotate_x << endl;
	//cout << "rotate_y >> " << rotate_y << endl;
	// glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
	glLoadIdentity();
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		0, 1, 0
	);
	updateTransfPlanes();
	renderScene();			// First render the scene!
	glutSwapBuffers();		// Render complete -- swap buffers!
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	glMatrixMode(GL_PROJECTION);		// Projection matrix
	glLoadIdentity();					// Reset to identity matrix
	//glFrustum(-10.0, 10, -10.0, 10.0, 50, 150.0);
	gluPerspective(60, aspectRatio, 0.1, 100);
	//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	//gluOrtho2D(-2, 2, -2, 2);
	glMatrixMode(GL_MODELVIEW);			// Model view matrix
	glLoadIdentity();					// -||-
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		0, 1, 0
	);
	updateTransfPlanes();
}

inline void updateTransfPlanes() {
	// Calculate object post-transform planes
	if (customObj->isDataLoaded()) {
		double transMat[16];
		glGetDoublev(GL_MODELVIEW_MATRIX, transMat);
		dmat4 projectionMat = {
			{transMat[0], transMat[1], transMat[2], transMat[3]},
			{transMat[4], transMat[5], transMat[6], transMat[7]},
			{transMat[8], transMat[9], transMat[10], transMat[11]},
			{transMat[12], transMat[13], transMat[14], transMat[15]}
		};
		customObj->postTransformPlanes(projectionMat);
	}
}

void glObj()
{
	glPushMatrix();
	//glRotatef(rotate_x, 1.0, 0.0, 0.0);
	//glRotatef(rotate_y, 0.0, 1.0, 0.0);
	transformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> transformPlanes = customObj->getProjectionPlanes();
	vector<dvec4> vertices = customObj->getVertices();

	for (int i = 0; i < pols.size(); i++) {
		ivec3 pol = pols[i];
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		if (cull) {
			// Get this polygon's plane and check the mul value!
			/*
			Something's wrong with my former culling. Need to implement
			post-transform culling.
			*/
			//dvec4 plane = transformPlanes[i];
			//GLdouble res = dot(plane, dvec4{ eye, 1 });
			//if (res < 0 || glm::abs(res) <= 1E-6) {
			//	// not visible
			//	continue;
			//}

			// Another try
			double modelViewArray[16];
			glGetDoublev(GL_MODELVIEW_MATRIX, modelViewArray);
			dmat4 modelViewMat = {
				{modelViewArray[0], modelViewArray[1], modelViewArray[2], modelViewArray[3]},
				{modelViewArray[4], modelViewArray[5], modelViewArray[6], modelViewArray[7]},
				{modelViewArray[8], modelViewArray[9], modelViewArray[10], modelViewArray[11]},
				{modelViewArray[12], modelViewArray[13], modelViewArray[14], modelViewArray[15]}
			};

			double projectionArray[16];
			glGetDoublev(GL_PROJECTION_MATRIX, projectionArray);
			dmat4 projectionMat = {
				{projectionArray[0], projectionArray[1], projectionArray[2], projectionArray[3]},
				{projectionArray[4], projectionArray[5], projectionArray[6], projectionArray[7]},
				{projectionArray[8], projectionArray[9], projectionArray[10], projectionArray[11]},
				{projectionArray[12], projectionArray[13], projectionArray[14], projectionArray[15]}
			};

			dmat4 totalMat = projectionMat * modelViewMat;
			dvec3 firstTransf = totalMat * first;
			dvec3 secondTransf = totalMat * second;
			dvec3 thirdTransf = totalMat * third;

			GLdouble x0 = firstTransf.x;
			GLdouble y0 = firstTransf.y;
			GLdouble z0 = firstTransf.z;

			GLdouble x1 = secondTransf.x;
			GLdouble y1 = secondTransf.y;
			GLdouble z1 = secondTransf.z;

			GLdouble x2 = thirdTransf.x;
			GLdouble y2 = thirdTransf.y;
			GLdouble z2 = thirdTransf.z;

			GLdouble area = (x0*y1 - x1*y0) + (x1*y2 - x2*y1) + (x2*y0 - x0*y2);

			if (area <= 0) {
				continue;
			}
		}

		glBegin(GL_LINE_LOOP);
		glVertex3d(first.x, first.y, first.z);
		glVertex3d(second.x, second.y, second.z);
		glVertex3d(third.x, third.y, third.z);
		glEnd();
	}
	glPopMatrix();
}

void specialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		move_x += 0.1;
		
		rotate_y -= 5;
	}
	else if (key == GLUT_KEY_LEFT) {
		move_x -= 0.1;
		
		rotate_y += 5;
	}
	else if (key == GLUT_KEY_UP) {
		move_z -= 0.1;
		
		rotate_x += 5;
	}
	else if (key == GLUT_KEY_DOWN) {
		move_z += 0.1;
		
		rotate_x -= 5;
	}

	glutPostRedisplay();
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void setColour(char drawingColour) {
	switch (drawingColour) {
	case 'k':
		glColor3f(0, 0, 0);
		break;
	case 'w':
		glColor3f(1, 1, 1);
		break;
	case 'r':
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case 'g':
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 'b':
		glColor3f(0, 0, 1);
		break;
	case 'y':
		glColor3f(1, 1, 0);
		break;
	}
}

void renderScene()
{
	glRotatef(rotate_x, 1.0, 0.0, 0.0);
	glRotatef(rotate_y, 0.0, 1.0, 0.0);

	//cout << "CustomObj = " << customObj << endl;
	//cout << "customObj->isDataLoaded() = " << fmt::to_string(customObj->isDataLoaded()) << endl;

	//glPushMatrix();
	//glMatrixMode(GL_MODELVIEW);			// Model view matrix
	//glLoadIdentity();					// -||-
	//gluLookAt(
	//	move_x, 0, 2.5 + move_z,
	//	0, 0, 0,
	//	0, 1, 0
	//);
	//glPopMatrix();

	if (customObj && customObj->isDataLoaded()) {
		glTestingPoints();
		setColour(drawingColour);
		glObj();
	}
	else {
		if (dummyCube) {
			exampleCube();
		}
		// GLUquadricObj* quadro = gluNewQuadric();
		// gluSphere(quadro, 1, 48, 48);
	}

	glBezierControlPolygon();
	drawBezier();
}

void glTestingPoints()
{
	// Now load the testing poitns
	glColor3f(1, 0, 0);
	vector<dvec4> testingPoints = customObj->getTestingPoints();
	
	//glLoadIdentity();
	for (dvec4 testingPoint : testingPoints) {
		//cout << "Testing point: " << fmt::format("{} {} {}\n", testingPoint[0], testingPoint[1], testingPoint[2]);
		glPushMatrix();
		glTranslatef(testingPoint[0], testingPoint[1], testingPoint[2]);
		//glLoadIdentity();
		glutSolidSphere(RADIUS, 48, 48);
		glPopMatrix();
	}	
}

void bezierInput()
{
	cout << "Please input a bezier control polygon point!" << endl;
	cout << "Format: x y z" << endl;
	fvec3 point;
	
	int toCheck = scanf_s("%f %f %f", &point[0], &point[1], &point[2]);
	if (toCheck != 3) {
		throw invalid_argument("Wrong format!");
	}

	bezierCurve->addControlPoint(point);
}

void glBezierControlPolygon()
{
	glColor3f(0, 0, 1);
	for (fvec3 currPoint : bezierCurve->getControlPolygon()) {
		glPushMatrix();
		// Don't forget to translate the sphere!
		glTranslatef(currPoint[0], currPoint[1], currPoint[2]);
		glutSolidSphere(RADIUS_BEZIER, 48, 48);
		glPopMatrix();
	}
}

void drawBezier()
{
	glBegin(GL_LINE_STRIP);
	for (fvec3 point : bezierCurve->getBezierCurve()) {
		glVertex3f(point[0], point[1], point[2]);
	}
	glEnd();
}

void animateCamera()
{
	// Calculate the interval
	/*
	We want to move the camera over n points, and we wanna do that in an interval
	of hmm gonna see about this later.
	*/
	// MOVE_CAMERA_INTERVAL = bezierCurve->getBezierCurve().size() / 
	if (animating) {
		return;
	}

	// Start moving the camera!
	animating = true;
	moveCamera(0);
}

void moveCamera(int index)
{
	// Update the camera, but first check if index is out of bounds
	vector<fvec3> bezierCurvePoints = bezierCurve->getBezierCurve();

	if (index >= bezierCurvePoints.size()) {
		// We've reached the end, that's all folks!
		animating = false;
		return;
	}

	// Else go on!
	eye = bezierCurve->getBezierCurve()[index];
	
	// Moves the camera
	gluLookAt(
		eye.x, eye.y, eye.z,
		center.x, center.y, center.z,
		0, 1, 0
	);
	glutPostRedisplay();

	// Set up the timer here which will move the gluLookAt()
	glutTimerFunc(MOVE_CAMERA_INTERVAL, moveCamera, index + 1);
}

void hardcodeBezier()
{
	bezierCurve->addControlPoint(fvec3{ 4, 0, 0 });
	bezierCurve->addControlPoint(fvec3{ 0, -5.1, 0 });
	bezierCurve->addControlPoint(fvec3{ -3.02, -0.01, 0 });
	bezierCurve->addControlPoint(fvec3{ -6.55, -5.29, 0 });
	// bezierCurve->addControlPoint(fvec3{ 4, 0, 0 });
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clear everything
		customObj.reset();
		cout << "Object reset -> enter a new object to render: << ";
		string toLoad;
		cin >> toLoad;
		cout << "Loading object " << toLoad << endl;
		customObj = make_unique<Model>(toLoad);

		// Okay got it now
		customObj->loadData();
		updateTransfPlanes();
		cout << customObj->dataStr();
		glutPostRedisplay();
	}
	else if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) {
		// Clear the bezier lines
		cout << "Bezier curve and rotation reset!" << endl;
		rotate_x = rotate_y = 0;
		bezierCurve.reset();
		bezierCurve = make_unique<BezierHolder>();
		glutPostRedisplay();
	}
}

void keyP()
{
	if (!customObj || !customObj->isDataLoaded()) {
		cout << "Object data not loaded!" << endl;
		return;
	}

	string buffer;
	dvec4 pointToCheck;
	cout << "Please enter the point to be queried << ";

	cin >> buffer;
	pointToCheck[0] = stod(buffer);

	cin >> buffer;
	pointToCheck[1] = stod(buffer);

	cin >> buffer;
	pointToCheck[2] = stod(buffer);

	// Set the homo var
	pointToCheck[3] = 1;

	/*vector<string> splitArr(3);
	boost::split(splitArr, buffer, boost::is_any_of(" "));
	dvec4 pointToCheck{ stod(splitArr[0]), stod(splitArr[1]), stod(splitArr[2]), 1 };
	*/

	// Add it to testing points here, request re-drawing
	customObj->addTestingPoint(pointToCheck);
	glutPostRedisplay();

	pointToCheck = antiTransformVex(pointToCheck);
	int statusCode = queryPoints(pointToCheck);
	if (statusCode == -1) {
		cout << "The point is located inside the polygon!" << endl;
	}
	else if (statusCode == 0) {
		cout << "The point is located on the polygon mantle!" << endl;
	}
	else {
		cout << "The point is located outside the polygon!" << endl;
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case 'h':
		hardcodeBezier();
		glutPostRedisplay();
		break;
	case 'a':
		animateCamera();
		break;
	case 'p':
		keyP();
		break;
	case 'e':
		bezierInput();
		glutPostRedisplay();
		break;
	case 'f':
		bezierCurve->calculateBezierCurve();
		glutPostRedisplay();
		cout << "Bezier curve calculated!" << endl;;
		break;
	case 'd':
		dummyCube ^= 0b1;
		glutPostRedisplay();
		break;
	case 'r':
	case 'w':
	case 'g':
	case 'b':
	case 'y':
	case 'k':
		drawingColour = theKey;
		glutPostRedisplay();
		break;
	case 'l':
		string line;
		vector<string> parts;
		while(true) {
			getline(cin, line);
			if (line == "") {
				continue;
			}
			else if (line == "quit") {
				break;
			}

			boost::split(parts, line, boost::is_any_of(" "));
			dvec4 pointToCheck{stod(parts[0]), stod(parts[1]), stod(parts[2]), 1};
			customObj->addTestingPoint(pointToCheck);
			glutPostRedisplay();

			pointToCheck = antiTransformVex(pointToCheck);
			int statusCode = queryPoints(pointToCheck);
			if (statusCode == -1) {
				cout << "The point is located inside the polygon!" << endl;
			}
			else if (statusCode == 0) {
				cout << "The point is located on the polygon mantle!" << endl;
			}
			else {
				cout << "The point is located outside the polygon!" << endl;
			}
		}
		break;
	//default:
	//	cout << "Specified colour(" << theKey << ") is not supported! :(" << endl;
	}
}

inline void transformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// glTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	glScalef(scaleFactor, scaleFactor, scaleFactor);
	glTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline dvec4 antiTransformVex(dvec4 toTransform)
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	GLdouble scaleFactor = customObj->getScaleFactor();

	dmat4 translateMat = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{bodyCentre[0], bodyCentre[1], bodyCentre[2], 1},
	};

	dmat4 scaleMat = {
		{1. / scaleFactor, 0, 0, 0},
		{0, 1. / scaleFactor, 0, 0},
		{0, 0, 1. / scaleFactor, 0},
		{0, 0, 0, 1},
	};

	// First scale then translate
	// In code: translateMat * scaleMat * vex
	dvec4 resVex = translateMat * scaleMat * toTransform;
	return resVex;
}

int queryPoints(dvec4 toCheck)
{
	// Check if inside
	bool inside = true;
	bool onMantle = false;
	vector<dvec4> planes = customObj->getPlanes();
	for (dvec4 plane : planes) {
		GLdouble res = glm::dot(toCheck, plane);
		if (glm::abs(res) <= EPSILON) {
			onMantle = true;
			break;
		}
		else if (res > 0) {
			/// Means it's not inside
			inside = false;
			break;
		}
	}

	// Print res
	if (onMantle) {
		return 0;
	}
	else if (inside) {
		return -1;
	}
	else {
		return 1;
	}
}

void exampleCube()
{
	// FRONT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0); glVertex3f(-0.5, -0.5, -0.5);
	glColor3f(0.0, 1.0, 0.0); glVertex3f(-0.5, 0.5, -0.5);
	glColor3f(0.0, 0.0, 1.0); glVertex3f(0.5, 0.5, -0.5);
	glColor3f(1.0, 0.0, 1.0); glVertex3f(0.5, -0.5, -0.5);
	glEnd();

	// BACK
	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glEnd();

	// Purple side - RIGHT
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glEnd();

	// Green side - LEFT
	glBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// Blue side - TOP
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glEnd();

	// Red side - BOTTOM
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();
}