#include "myGL.h"
#include <stdexcept>
#include <glm/gtc/constants.hpp>
using namespace std; using namespace glm;

void myGL::myglLoadIdentity()
{
	// Need to reset the matrix to the identity matrix
	// First check which matrix to reset!

	if (myGL::currState == GL_MODELVIEW) {
		myGL::modelviewMatrix = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
		};
	}
	else if (myGL::currState == GL_PROJECTION) {
		myGL::projectionMatrix = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
		};
	}

	// Reset OGL's matrix as well!
	glLoadIdentity();
}

void myGL::myglLoadIdentityExclusive()
{
	if (myGL::currState == GL_MODELVIEW) {
		myGL::modelviewMatrix = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
		};
	}
	else if (myGL::currState == GL_PROJECTION) {
		myGL::projectionMatrix = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
		};
	}
}

void myGL::myglPushMatrix()
{
	if (myGL::currState == GL_MODELVIEW) {
		matricesStack.push(myGL::modelviewMatrix);
	}
	else if (myGL::currState == GL_PROJECTION) {
		matricesStack.push(myGL::projectionMatrix);
	}
}

void myGL::myglPopMatrix()
{
	if (myGL::currState == GL_MODELVIEW) {
		myGL::modelviewMatrix = myGL::matricesStack.top();
	}
	else if (myGL::currState == GL_PROJECTION) {
		myGL::projectionMatrix = myGL::matricesStack.top();
	}
	myGL::matricesStack.pop();
}

void myGL::myglMatrixMode(int toSetState)
{
	// First check whether everything is alright!
	if (toSetState != GL_PROJECTION && toSetState != GL_MODELVIEW) {
		throw invalid_argument("Invalid matrix mode passed!");
	}

	myGL::currState = toSetState;

	// Now set OGL's mode as well!
	glMatrixMode(toSetState);
}

void myGL::mygluLookAt(
	GLfloat eyeX, GLfloat eyeY, GLfloat eyeZ,
	GLfloat centerX, GLfloat centerY, GLfloat centerZ,
	GLfloat upX, GLfloat upY, GLfloat upZ
)
{
	myGL::eye = {eyeX, eyeY, eyeZ};
	myGL::center = {centerX, centerY, centerZ};
	myGL::ECSet = true;

	// At the end inverse the z axis!
	myGL::myglScalef(1, 1, -1);

	// Now need to discover the axes!
	fvec3 zAxis = glm::normalize(myGL::center - myGL::eye);
	fvec3 vup = glm::normalize(fvec3{ upX, upY, upZ });
	fvec3 xAxis = glm::cross(zAxis, vup);
	fvec3 yAxis = glm::cross(xAxis, zAxis);
	
	// Got 'em, make the matrix now
	fmat4 rotMat = {
		{ xAxis, 0},
		{ yAxis, 0},
		{ zAxis, 0 },
		{ 0, 0, 0, 1}
	};
	myGL::modelviewMatrix = myGL::modelviewMatrix * transpose(rotMat);

	// Well, let's start by translating the camera
	myGL::myglTranslatef(-eyeX, -eyeY, -eyeZ);
}

void myGL::myglTranslatef(GLfloat dx, GLfloat dy, GLfloat dz)
{
	// Make a translation matrix
	fmat4 toTranslateWith = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{dx, dy, dz, 1}
	};

	// New matrix goes as the left operand into matrix multiplication operation
	if (myGL::currState == GL_MODELVIEW) {
		myGL::modelviewMatrix =  myGL::modelviewMatrix * toTranslateWith;
	}
	else if (myGL::currState == GL_PROJECTION) {
		myGL::projectionMatrix = myGL::projectionMatrix * toTranslateWith;
	}
}

void myGL::myglRotatef(GLfloat angle, GLfloat x, GLfloat y, GLfloat z)
{
	GLfloat radAngle = angle * glm::pi<GLfloat>()/180;
	GLfloat sinA = glm::sin(radAngle);
	GLfloat cosA = glm::cos(radAngle);

	// First let's construct the vector, and normalise it!
	fvec3 rotVex = glm::normalize(fvec3{ x, y, z });
	GLfloat rx = rotVex[0];
	GLfloat ry = rotVex[1];
	GLfloat rz = rotVex[2];

	// Make a rotation matrix
	fmat4 toRotateWith = {
		{cosA + rx*rx*(1 - cosA), ry*rx*(1 - cosA) + rz*sinA, rz * rx * (1 - cosA) - ry * sinA, 0},
		{ry * rx * (1 - cosA) - rz * sinA, cosA + ry * ry * (1 - cosA), ry * rz * (1 - cosA) + rx * sinA, 0},
		{rz * rx * (1 - cosA) + ry * sinA, ry * rz * (1 - cosA) - rx * sinA, cosA + rz * rz * (1 - cosA), 0},
		{0, 0, 0, 1}
	};

	// New matrix goes as the left operand into matrix multiplication operation
	// New matrix goes as the left operand into matrix multiplication operation
	if (myGL::currState == GL_MODELVIEW) {
		myGL::modelviewMatrix =  myGL::modelviewMatrix * toRotateWith;
	}
	else if (myGL::currState == GL_PROJECTION) {
		myGL::projectionMatrix = myGL::projectionMatrix * toRotateWith;
	}
}

void myGL::myglScalef(GLfloat sx, GLfloat sy, GLfloat sz)
{
	// Make a translation matrix
	fmat4 toScaleWith = {
		{sx, 0, 0, 0},
		{0, sy, 0, 0},
		{0, 0, sz, 0},
		{0, 0, 0, 1}
	};

	// New matrix goes as the left operand into matrix multiplication operation
	if (myGL::currState == GL_MODELVIEW) {
		myGL::modelviewMatrix = myGL::modelviewMatrix * toScaleWith;
	}
	else if (myGL::currState == GL_PROJECTION) {
		myGL::projectionMatrix = myGL::projectionMatrix * toScaleWith;
	}
}

void myGL::myglPerspective()
{
	// This function will set up the projection matrix
	// First check args
	if (!ECSet) {
		throw runtime_error("Please set the Eye coordinates, Center coordinates, and the ViewUp vector first!");
	}
	
	GLfloat H = glm::length(myGL::center - myGL::eye);
	fmat4 toMultWith = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 0, 1.f / H},
		{0, 0, 0, 0}
	};

	myGL::projectionMatrix = myGL::projectionMatrix * toMultWith;
}
