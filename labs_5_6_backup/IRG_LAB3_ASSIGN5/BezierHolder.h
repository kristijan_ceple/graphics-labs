#pragma once
#include <vector>
#include <glm\fwd.hpp>
using namespace std; using namespace glm;
#include <gl\GL.h>

inline GLdouble STEP = 0.01;

inline fmat2 B1 = {
	{ -1, 1 },
	{ 1, 0 }
};

inline fmat3 B2 = {
	{1, -2, 1},
	{-2, 2, 0},
	{1, 0, 0}
};

inline fmat4 B3 = {
	{-1, 3, -3, 1},
	{3, -6, 3, 0},
	{-3, 3, 0, 0},
	{1, 0, 0, 0}
};

class BezierHolder
{
private:
	int n;
	
	vector<fvec3> controlPolygon;
	vector<fvec3> bezierCurve;
	// vector<GLdouble> bernsteinPolynom;
	
	vector<GLdouble> bcoeffs;
	bool coeffsCalculated = false;
public:
	BezierHolder(vector<fvec3> controlPolygon);
	void calculateBinCoeffs();
	// GLdouble bernsteinPolynom(GLdouble t);			// Too big overhead
	vector<fvec3> calculateBezierCurve(const GLdouble step);
	vector<fvec3> getBezierCurve();
	bool areCoeffsCalculated();
};

