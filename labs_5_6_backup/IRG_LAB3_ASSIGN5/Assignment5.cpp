#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include "Model.h"
#include <fmt\format.h>
#include <boost/algorithm/string.hpp>
#include "../IRG_LAB3_ASSIGN5/myGL.h"
using namespace std; using namespace glm;

 // #################################################################################################################
 // ##########################################		CONSTANTS		################################################
 // #################################################################################################################

GLuint window;
GLuint width = 640, height = 480;
GLdouble aspectRatio = width / height;
GLdouble move_z = 0, move_x = 0, rotate_x = 0, rotate_y = 0;
GLuint mouse_x, mouse_y;
GLbyte drawingColour = 'w';
unique_ptr<Model> customObj = nullptr;
const GLdouble EPSILON = 1E-6;
const GLdouble RADIUS = 0.025;

fvec3 eye = { 1, 2, 3 };
fvec3 center = { 0, 0, 0 };

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void mouseMoved(int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void renderScene();
void setColour(char drawingColour);
void specialKeys(int key, int x, int y);
void exampleCube();
void glObj();
int queryPoints(dvec4 toCheck);
dvec4 antiTransformVex(dvec4);
void transformModel();
void keyP();
void glTestingPoints();
void updateEye();
void updateCenter();

// #################################################################################################################
// ##########################################		CONSTANTS		################################################
// #################################################################################################################

int main(int argc, char** argv)
{
	// First load data
	string toLoad;
	cout << "Filename to load << ";
	cin >> toLoad;

	customObj = make_unique<Model>(toLoad);
	cout << "Loading object " << toLoad << endl;
	customObj->loadData();
	cout << customObj->dataStr() << endl;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("Graphics Matrices!!!");
	glutDisplayFunc(myDisplay);
	glutPassiveMotionFunc(mouseMoved);
	glutSpecialFunc(specialKeys);
	glutReshapeFunc(myReshape);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glEnable(GL_DEPTH_TEST);

	cout << "Object rendering program!" << endl;
	cout << "Right click clears the polygon and enables inputting another object to render!" << endl;
	cout << "Press p to input a point that will be tested whether it is inside, outside, or on the mantles of the convex polygon!" << endl;
	cout << "Press r, g, b, y, k, or w to set color!" << endl;
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	//cout << "rotate_x >> " << rotate_x << endl;
	//cout << "rotate_y >> " << rotate_y << endl;
	// glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	myGL::myglMatrixMode(GL_PROJECTION);
	myGL::myglLoadIdentityExclusive();
	myGL::myglMatrixMode(GL_MODELVIEW);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// Clear both colour and depth
	myGL::myglLoadIdentity();
	myGL::mygluLookAt(
		eye[0], eye[1], eye[2],
		center[0], center[1], center[2],
		0, 1, 0
	);
	myGL::myglPerspective();
	renderScene();			// First render the scene!
	glutSwapBuffers();		// Render complete -- swap buffers!
}

void myReshape(int w, int h)
{
	width = w, height = h;
	glViewport(0, 0, width, height);	// Open in window
	myGL::myglMatrixMode(GL_PROJECTION);		// Projection matrix
	myGL::myglLoadIdentity();					// Reset to identity matrix
	// Later will set the projection matrix!

	//gluOrtho2D(0, width, 0, height);	// Perpendicular projection(???)
	gluOrtho2D(-1, 1, -1, 1);			// Make it nice and in bounds!
	//gluPerspective(45, aspectRatio, 0.1, 100);
	//gluLookAt(
	//	0, 0, 2.5,
	//	0, 0, 0,
	//	0, 1, 0
	//);
	myGL::myglMatrixMode(GL_MODELVIEW);			// Model view matrix
	myGL::myglLoadIdentity();					// -||-
	myGL::mygluLookAt(
		eye[0], eye[1], eye[2],
		center[0], center[1], center[2],
		0, 1, 0
	);
	myGL::myglPerspective();
}

void glObj()
{
	myGL::myglPushMatrix();
	myGL::myglRotatef(rotate_x, 1.0, 0.0, 0.0);
	myGL::myglRotatef(rotate_y, 0.0, 1.0, 0.0);
	transformModel();

	// Render each polygon
	vector<ivec3> pols = customObj->getPolygons();
	vector<dvec4> vertices = customObj->getVertices();

	for (ivec3 pol : pols) {
		dvec4 first = vertices[pol[0]];
		dvec4 second = vertices[pol[1]];
		dvec4 third = vertices[pol[2]];

		myGL::myglBegin(GL_LINE_LOOP);
		myGL::myglVertex3f(first.x, first.y, first.z);
		myGL::myglVertex3f(second.x, second.y, second.z);
		myGL::myglVertex3f(third.x, third.y, third.z);
		myGL::myglEnd();
	}
	myGL::myglPopMatrix();
}

void specialKeys(int key, int x, int y)
{
	if (key == GLUT_KEY_RIGHT) {
		move_x += 0.1;
		rotate_y -= 5;
	}
	else if (key == GLUT_KEY_LEFT) {
		move_x -= 0.1;
		rotate_y += 5;
	}
	else if (key == GLUT_KEY_UP) {
		fvec3 delta = normalize(center - eye);
		center = center + delta;
		eye = eye + delta;
		//move_z -= 0.1;
		//rotate_x += 5;
	}
	else if (key == GLUT_KEY_DOWN) {
		fvec3 delta = normalize(center - eye);
		center = center - delta;
		eye = eye - delta;
		//move_z += 0.1;
		//rotate_x -= 5;
	}

	glutPostRedisplay();
}

void mouseMoved(int x, int y)
{
	mouse_x = x;
	mouse_y = y;
	glutPostRedisplay();
}

void setColour(char drawingColour) {
	switch (drawingColour) {
	case 'k':
		glColor3f(0, 0, 0);
		break;
	case 'w':
		glColor3f(1, 1, 1);
		break;
	case 'r':
		glColor3f(1.0f, 0.0f, 0.0f);
		break;
	case 'g':
		glColor3f(0.0f, 1.0f, 0.0f);
		break;
	case 'b':
		glColor3f(0, 0, 1);
		break;
	case 'y':
		glColor3f(1, 1, 0);
		break;
	}
}

void renderScene()
{
	// myGL::myglRotatef(rotate_x, 1.0, 0.0, 0.0);
	// myGL::myglRotatef(rotate_y, 0.0, 1.0, 0.0);

	//cout << "CustomObj = " << customObj << endl;
	//cout << "customObj->isDataLoaded() = " << fmt::to_string(customObj->isDataLoaded()) << endl;

	//myGL::myglPushMatrix();
	//glMatrixMode(GL_MODELVIEW);			// Model view matrix
	//glLoadIdentity();					// -||-
	//gluLookAt(
	//	move_x, 0, 2.5 + move_z,
	//	0, 0, 0,
	//	0, 1, 0
	//);
	//myGL::myglPopMatrix();

	if (customObj && customObj->isDataLoaded()) {
		glTestingPoints();
		setColour(drawingColour);
		glObj();
	}
	else {
		exampleCube();
		// GLUquadricObj* quadro = gluNewQuadric();
		// gluSphere(quadro, 1, 48, 48);
	}
}

void glTestingPoints()
{
	// Now load the testing points
	glColor3f(1, 0, 0);
	vector<dvec4> testingPoints = customObj->getTestingPoints();
	
	//glLoadIdentity();
	for (dvec4 testingPoint : testingPoints) {
		//cout << "Testing point: " << fmt::format("{} {} {}\n", testingPoint[0], testingPoint[1], testingPoint[2]);
		glPushMatrix();
		glTranslatef(testingPoint[0], testingPoint[1], testingPoint[2]);
		//glLoadIdentity();
		glutSolidSphere(RADIUS, 48, 48);
		glPopMatrix();
	}	
}

void updateEye()
{
	cout << "Enter eye coordinates x, y, z << ";
	scanf_s("%f, %f, %f", &eye[0], &eye[1], &eye[2]);
	cout << endl;
	glutPostRedisplay();
}

void updateCenter()
{
	cout << "Enter center coordinates x, y, z << ";
	scanf_s("%f, %f, %f", &center[0], &center[1], &center[2]);
	cout << endl;
	glutPostRedisplay();
}

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		// Clear everything
		customObj = nullptr;
		cout << "Object reset -> enter a new object to render: << ";
		string toLoad;
		cin >> toLoad;
		cout << "Loading object " << toLoad << endl;
		customObj = make_unique<Model>(toLoad);

		// Okay got it now
		customObj->loadData();
		cout << customObj->dataStr();
		glutPostRedisplay();
	}
}

void keyP()
{
	if (!customObj || !customObj->isDataLoaded()) {
		cout << "Object data not loaded!" << endl;
		return;
	}

	string buffer;
	dvec4 pointToCheck;
	cout << "Please enter the point to be queried << ";

	cin >> buffer;
	pointToCheck[0] = stod(buffer);

	cin >> buffer;
	pointToCheck[1] = stod(buffer);

	cin >> buffer;
	pointToCheck[2] = stod(buffer);

	// Set the homo var
	pointToCheck[3] = 1;

	/*vector<string> splitArr(3);
	boost::split(splitArr, buffer, boost::is_any_of(" "));
	dvec4 pointToCheck{ stod(splitArr[0]), stod(splitArr[1]), stod(splitArr[2]), 1 };
	*/

	// Add it to testing points here, request re-drawing
	customObj->addTestingPoint(pointToCheck);
	glutPostRedisplay();

	pointToCheck = antiTransformVex(pointToCheck);
	int statusCode = queryPoints(pointToCheck);
	if (statusCode == -1) {
		cout << "The point is located inside the polygon!" << endl;
	}
	else if (statusCode == 0) {
		cout << "The point is located on the polygon mantle!" << endl;
	}
	else {
		cout << "The point is located outside the polygon!" << endl;
	}
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	cout << "Pressed key: " << theKey << endl;

	switch (theKey) {
	case 'e':
		updateEye();
		break;
	case 'c':
		updateCenter();
		break;
	case 'p':
		keyP();
		break;
	case 'r':
	case 'w':
	case 'g':
	case 'b':
	case 'y':
	case 'k':
		drawingColour = theKey;
		glutPostRedisplay();
		break;
	case 'l':
		string line;
		vector<string> parts;
		while(true) {
			getline(cin, line);
			if (line == "") {
				continue;
			}
			else if (line == "quit") {
				break;
			}

			boost::split(parts, line, boost::is_any_of(" "));
			dvec4 pointToCheck{stod(parts[0]), stod(parts[1]), stod(parts[2]), 1};
			customObj->addTestingPoint(pointToCheck);
			glutPostRedisplay();

			pointToCheck = antiTransformVex(pointToCheck);
			int statusCode = queryPoints(pointToCheck);
			if (statusCode == -1) {
				cout << "The point is located inside the polygon!" << endl;
			}
			else if (statusCode == 0) {
				cout << "The point is located on the polygon mantle!" << endl;
			}
			else {
				cout << "The point is located outside the polygon!" << endl;
			}
		}
		break;
	//default:
	//	cout << "Specified colour(" << theKey << ") is not supported! :(" << endl;
	}
}

inline void transformModel()
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	//dvec3 scaleFactors = customObj->getScaleFactor();
	GLdouble scaleFactor = customObj->getScaleFactor();

	// myGL::myglTranslatef(bodyCentre[0], bodyCentre[1], bodyCentre[2]);
	myGL::myglScalef(scaleFactor, scaleFactor, scaleFactor);
	myGL::myglTranslatef(-bodyCentre[0], -bodyCentre[1], -bodyCentre[2]);
}

inline dvec4 antiTransformVex(dvec4 toTransform)
{
	// Do the model transformations here
	dvec4 bodyCentre = customObj->getBodyCentre();
	GLdouble scaleFactor = customObj->getScaleFactor();

	dmat4 translateMat = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{bodyCentre[0], bodyCentre[1], bodyCentre[2], 1},
	};

	dmat4 scaleMat = {
		{1. / scaleFactor, 0, 0, 0},
		{0, 1. / scaleFactor, 0, 0},
		{0, 0, 1. / scaleFactor, 0},
		{0, 0, 0, 1},
	};

	// First scale then translate
	// In code: translateMat * scaleMat * vex
	dvec4 resVex = translateMat * scaleMat * toTransform;
	return resVex;
}

int queryPoints(dvec4 toCheck)
{
	// Check if inside
	bool inside = true;
	bool onMantle = false;
	vector<dvec4> planes = customObj->getPlanes();
	for (dvec4 plane : planes) {
		GLdouble res = glm::dot(toCheck, plane);
		if (glm::abs(res) <= EPSILON) {
			onMantle = true;
			break;
		}
		else if (res > 0) {
			/// Means it's not inside
			inside = false;
			break;
		}
	}

	// Print res
	if (onMantle) {
		return 0;
	}
	else if (inside) {
		return -1;
	}
	else {
		return 1;
	}
}

void exampleCube()
{
	myGL::myglPushMatrix();
	myGL::myglRotatef(rotate_x, 1.0, 0.0, 0.0);
	myGL::myglRotatef(rotate_y, 0.0, 1.0, 0.0);

	// FRONT
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0); myGL::myglVertex3f(-0.5, -0.5, -0.5);
	glColor3f(0.0, 1.0, 0.0); myGL::myglVertex3f(-0.5, 0.5, -0.5);
	glColor3f(0.0, 0.0, 1.0); myGL::myglVertex3f(0.5, 0.5, -0.5);
	glColor3f(1.0, 0.0, 1.0); myGL::myglVertex3f(0.5, -0.5, -0.5);
	myGL::myglEnd();

	// BACK
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	myGL::myglVertex3f(0.5, -0.5, 0.5);
	myGL::myglVertex3f(0.5, 0.5, 0.5);
	myGL::myglVertex3f(-0.5, 0.5, 0.5);
	myGL::myglVertex3f(-0.5, -0.5, 0.5);
	myGL::myglEnd();

	// Purple side - RIGHT
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	myGL::myglVertex3f(0.5, -0.5, -0.5);
	myGL::myglVertex3f(0.5, 0.5, -0.5);
	myGL::myglVertex3f(0.5, 0.5, 0.5);
	myGL::myglVertex3f(0.5, -0.5, 0.5);
	myGL::myglEnd();

	// Green side - LEFT
	myGL::myglBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	myGL::myglVertex3f(-0.5, -0.5, 0.5);
	myGL::myglVertex3f(-0.5, 0.5, 0.5);
	myGL::myglVertex3f(-0.5, 0.5, -0.5);
	myGL::myglVertex3f(-0.5, -0.5, -0.5);
	myGL::myglEnd();

	// Blue side - TOP
	myGL::myglBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	myGL::myglVertex3f(0.5, 0.5, 0.5);
	myGL::myglVertex3f(0.5, 0.5, -0.5);
	myGL::myglVertex3f(-0.5, 0.5, -0.5);
	myGL::myglVertex3f(-0.5, 0.5, 0.5);
	myGL::myglEnd();

	// Red side - BOTTOM
	myGL::myglBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 0.0);
	myGL::myglVertex3f(0.5, -0.5, -0.5);
	myGL::myglVertex3f(0.5, -0.5, 0.5);
	myGL::myglVertex3f(-0.5, -0.5, 0.5);
	myGL::myglVertex3f(-0.5, -0.5, -0.5);
	myGL::myglEnd();
	myGL::myglPopMatrix();
}