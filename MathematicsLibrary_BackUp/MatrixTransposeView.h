#include "AbstractMatrix.h"

#pragma once
#ifndef MATRIXTRANSPOSEVIEW_H
#define MATRIXTRANSPOSEVIEW_H

class MatrixTransposeView : public AbstractMatrix
{
private:
	IMatrix* _matrix;

public:
	MatrixTransposeView(IMatrix& toTranspose);
	int getRowsCount();
	int getColsCount();
	double get(int, int);
	IMatrix* set(int, int, double);
	IMatrix* copy();
	IMatrix* newInstance();
	double** toArray();
};

#endif
