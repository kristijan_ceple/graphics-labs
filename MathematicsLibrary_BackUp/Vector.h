#pragma once
#include "AbstractVector.h"

#ifndef VECTOR_H
#define VECTOR_H

class Vector :
	public AbstractVector
{
private:
	double*_elements;
	size_t _dimension;
	bool _immutable = false;

	// Empty constructor for copy
	Vector();
public:
	Vector(int, ...);
	Vector(size_t);
	Vector(double _elements[], size_t dimension);
	Vector(bool _immutable, bool canTakeArgumentArray, double* array, size_t dimension);
	double get(size_t toGet);
	IVector* set(size_t pos, double toSetVal);
	size_t getDimension();
	IVector* copy();
	IVector* newInstance(size_t n);
	~Vector();
	static Vector* parseSimple(std::string toParse);
};

#endif