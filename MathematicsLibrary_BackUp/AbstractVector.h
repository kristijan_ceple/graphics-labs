#include "IVector.h"
#include "Vector.h"
#include <iostream>
#include <stdexcept>
#include <cmath>

#pragma once

#ifndef ABSTSTRACTVECTOR_H
#define ABSTSTRACTVECTOR_H

class AbstractVector : public IVector
{
public:
	IVector* add(IVector& toAdd);
	IVector* nAdd(IVector& toAdd);
	IVector* sub(IVector& toSub);
	IVector* nSub(IVector& toSub);
	IVector* scalarMultiply(double scalar);
	IVector* nScalarMultiply(double scalar);
	double norm();
	IVector* normalize();
	IVector* nNormalize();
	double cosine(IVector& otherVector);
	double scalarProduct(IVector& otherVector);
	IVector* nVectorProduct(IVector& otherVector);
	double* toArray();
	IVector* copyPart(size_t toCopy);
	// virtual IMatrix* toRowMatrix(bool) = 0;
	// virtual IMatrix* toColumnMatrix(bool) = 0;
	std::string toString(char precision = 3);
	virtual ~AbstractVector();
};

#endif