#include <iostream>
#include "AbstractVector.h"
#include "Vector.h"
#include "IVector.h"
using namespace std;

void vectorsTest1()
{
	double values[]{3, 4, 5};
	Vector vect1{ values, 3 };
	cout << vect1.toString() << endl;

	Vector vect2{3, 5., 4., 3.};
	cout << vect2.toString() << endl;

	// Let's add them together!
	cout << vect1.add(vect2)->toString() << endl;
	cout << vect1.toString() << endl;

	cout << vect2.toString() << endl;
	cout << vect1.nSub(vect2)->toString() << endl;
}