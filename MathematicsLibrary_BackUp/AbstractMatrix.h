#include "IMatrix.h"

#pragma once
#ifndef ABSTRACTMATRIX_H
#define ABSTRACTMATRIX_H

class AbstractMatrix : public IMatrix
{
public:
	IMatrix* nTranspose(bool);
	IMatrix* add(IMatrix&);
	IMatrix* nAdd(IMatrix&);
	IMatrix* sub(IMatrix&);
	IMatrix* nSub(IMatrix&);
	IMatrix* nMultiply(IMatrix&);
	double determinant();
	IMatrix* subMatrix(int, int, bool);
	IMatrix* nInvert();
	double* toArray();
	std::string toString();
	std::string toString(int);
	IVector* toVector(bool);
};

#endif

