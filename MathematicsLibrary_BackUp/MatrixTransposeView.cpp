#include "MatrixTransposeView.h"

MatrixTransposeView::MatrixTransposeView(IMatrix& toTranspose)
{
	this->_matrix = &toTranspose;
}

int MatrixTransposeView::getRowsCount()
{
	return this->_matrix->getColCount();
}

int MatrixTransposeView::getColsCount()
{
	return this->_matrix->getRowsCount();
}

double MatrixTransposeView::get(int row, int col)
{
	return this->_matrix->get(col, row);
}

IMatrix* MatrixTransposeView::set(int row, int col, double toSet)
{
	return this->_matrix->set(col, row, toSet);
}

/*IMatrix* copy()
{
	IMatrix toRet = new Matrix
}*/

/*IMatrix* newInstance(int rows, int columns)
{
	I
}*/

double** MatrixTransposeView::toArray()
{
	// 2D array -> this is where the fun begins
	double** origArray = this->_matrix->toArray();

	size_t origRows = _matrix->getRowsCount();
	size_t origCols = _matrix->getColCount();

	// Array of arrays
	double** newArray = new double*[origCols];
	for (int i = 0; i < origCols; i++) {
		newArray[i] = new double[origRows];
	}

	// Just iterate inversely
	for (int j = 0; j < origCols; j++) {
		for (int i = 0; i < origRows; i++) {
			newArray[j][i] = origArray[i][j];
		}
	}

	return newArray;
}