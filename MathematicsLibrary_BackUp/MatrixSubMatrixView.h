#pragma once
#ifndef MATRIXSUBMATRIXVIEW_H
#define MATRIXSUBMATRIXVIEW_H

#include "AbstractMatrix.h"
class MatrixSubMatrixView :
	public AbstractMatrix
{
private:
//	int rowIndexes[];
	int* colIndice;
	int* rowIndice;
	MatrixSubMatrixView(IMatrix&, int, int);
public:
	MatrixSubMatrixView(IMatrix& origMat, int, int);
	size_t getRowsCount();
	size_t getColsCount();
	double get(int, int);
	IMatrix* set(int, int, double);
	IMatrix* copy();
	IMatrix* newInstance(int, int);
	IMatrix* subMatrix(int, int, bool);
};
#endif
